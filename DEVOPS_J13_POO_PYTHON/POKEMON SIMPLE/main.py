from f_type import Ftype
from combat import Combat
from w_type import Wtype
from f_type import Ftype
from trainer import Dresseur
from trainer import get_data
from combat import Combat

# #test combat.py
# pokemon1 = Ftype("Dracaucul", "250", "20")
# pokemon2 = Wtype("Tortufion", "300", "10")

# combat1 = Combat(pokemon1, pokemon2)
# combat1.baston()
# print(combat1.baston_display())

# def main():
#     nom, pokemons = get_data()
#     nom2, pokemons2 = get_data()
#     dresseur1 = Dresseur(nom, pokemons)
#     dresseur2 = Dresseur(nom2, pokemons2)
#     dresseur1.display_status()
#     dresseur2.display_status()

#     for i in range(len(pokemons)):
#         for k in range(len(pokemons)):
#             combat1 = Combat(dresseur1._pokemons[i], dresseur2._pokemons[k])

# main()

class Lancer():
    def __init__(self):
        self.algo()
        self.dual()

    def algo (self):
        self._nom, self._pokemons = get_data()
        self._nom2, self._pokemons2 = get_data()
        self._dresseur1 = Dresseur(self._nom, self._pokemons)
        self._dresseur2 = Dresseur(self._nom2, self._pokemons2)
        self._dresseur1.display_status()
        self._dresseur2.display_status()
        return self._dresseur1, self._dresseur2

    def dual(self):
        for i in range(len(self._pokemons)):
            for k in range(len(self._pokemons)):
                combat1 = Combat(self._dresseur1._pokemons[i], self._dresseur2._pokemons[k])

Lancer()