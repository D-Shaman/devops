'''
time pour le sleep
random pour crit et miss
'''
import time
import random


class Combat:
    '''
    Deff de la classe combat
    '''
    def __init__(self, pokemon1, pokemon2):
        self._pokemon1 = pokemon1
        self._pokemon2 = pokemon2
        self.type_avantage()
        self.baston()
        self.baston_display()

    def type_avantage(self):
        '''
        Définition de l'avantage de type
        '''
        self._a =0
        if self._pokemon1.get_type() == "Feu" and self._pokemon2.get_type() == "Plante":
            self._a = 2
        elif self._pokemon1.get_type() == "Eau" and self._pokemon2.get_type() == "Feu":
            self._a = 2
        elif self._pokemon1.get_type() == "Plante" and self._pokemon2.get_type() == "Eau":
            self._a = 2
        elif self._pokemon1.get_type() == "Feu" and self._pokemon2.get_type() == "Eau":
            self._a = 0.5
        elif self._pokemon1.get_type() == "Eau" and self._pokemon2.get_type() == "Plante":
            self._a = 0.5
        elif self._pokemon1.get_type() == "Plante" and self._pokemon2.get_type() == "Feu":
            self._a = 0.5
        else :
            self._a = 1
        return self._a

    def avantage_message(self):
        '''
        Définition du message selon l'avantage de type
        '''
        if self._a == 2 :
            print("C'est très efficace !")
        if self._a == 0.5:
            print("Ce n'est pas très efficace")

    def switch_sides(self):
        '''
        fonction pour switch les deux pokemon
        '''
        self._pokemon1, self._pokemon2 = self._pokemon2, self._pokemon1

    def baston(self):
        '''
        Fonction principale du combat
        '''
        while self._pokemon1.isKO() is False and self._pokemon2.isKO() is False:

            if self.crit() is True:
                self.messagecrit()
                self._pokemon2.set_hp(self._pokemon2.get_hp()-\
                ((self._pokemon1.get_atk()*self.type_avantage())*2))
                self.avantage_message()
                self.message_hp_pokemon()
                time.sleep(2)

            if self.miss() is True:
                self.newturnmiss()
                self._pokemon2.set_hp(self._pokemon2.get_hp()-\
                (self._pokemon1.get_atk()*self.type_avantage())*0)
                self.avantage_message()
                self.message_hp_pokemon()
                time.sleep(2)

            else:
                self.newturn()
                self._pokemon2.set_hp(self._pokemon2.get_hp() -\
                self._pokemon1.get_atk()*self.type_avantage())
                self.avantage_message()
                self.message_hp_pokemon()
                time.sleep(2)
            self.switch_sides()

    def messagecrit(self):
        '''
        Définition du message en cas de coup critique
        '''
        print("Nouveau tour: \nC'est un coup critique !!")
        print(f"{self._pokemon1.get_name()} attaque {self._pokemon2.get_name()} pour une \
valeur de {((self._pokemon1.get_atk()*self.type_avantage())*2)}")

    def newturn(self):
        '''
        Message a chaque nouveau tour
        '''
        print(f"Nouveau tour: \n{self._pokemon1.get_name()} attaque {self._pokemon2.get_name()} \
pour une valeur de {self._pokemon1.get_atk()*self.type_avantage()}")

    def newturnmiss(self):
        '''
        Message en cas de miss
        '''
        print(f"Nouveau tour: \n{self._pokemon1.get_name()} rate son attaque !!")

    def message_hp_pokemon(self):
        '''
        Display principal de l'état des Pokemon
        '''
        print(f"Les HP de {self._pokemon2.get_name()} sont {self._pokemon2.get_hp()}")
        print(f"Les HP de {self._pokemon1.get_name()} sont {self._pokemon1.get_hp()}\n")

    def miss(self):
        '''
        Algo de randomisation pour coup critique
        '''
        coumiss = False
        j = random.randint(0,10)
        if j == 0:
            coumiss = True
        return coumiss

    def crit(self):
        '''
        Algo de randomisation pour le miss
        '''
        coucrit = False
        i = random.randint(0,10)
        if i == 0:
            coucrit = True
        return coucrit

    def baston_display(self):
        '''
        Affiche le résultat du combat
        '''
        return f"Ce round de combat finit avec {self._pokemon1.get_name()} à \
{self._pokemon1.get_hp()} HP et {self._pokemon2.get_name()} à {self._pokemon2.get_hp()} HP"
