from f_type import Ftype
from w_type import Wtype
from p_type import Ptype

class Dresseur:
    def __init__(self, nom, pokemons):
        self.set_nom(nom)
        self.set_pokemons(pokemons)

    def set_nom(self, nom):
        self._nom = nom
    def get_nom(self):
        return self._nom

    def set_pokemons(self, pokemons):
        self._pokemons = pokemons
    def get_pokemons(self):
        return self._pokemons

    def display_status(self):
        print(f"Mon nom est {self.get_nom()} et mon équipe est constituée de:\n \
{self._pokemons[0].get_name()}\n \
{self._pokemons[1].get_name()}\n \
{self._pokemons[2].get_name()}\n")

def get_data():
    i = 0
    pokemons = []
    nom = input("Quel est le nom du premier dresseur ?\n")
    while i<3:
        choice_type = input("Quel est le type du pokemon (FType, Wtype, Ptype)?\n")
        if choice_type == "Ftype":
            pokemon1 = Ftype(input("Quel est le nom\n"), input("Quelle est HP\n"), input("Quel est ATK\n"))
            pokemons.append(pokemon1)
        if choice_type == "Wtype":
            pokemon2 = Wtype(input("Quel est le nom\n"), input("Quelle est HP\n"), input("Quel est ATK\n"))
            pokemons.append(pokemon2)
        if choice_type == "Ptype":
            pokemon3 = Ftype(input("Quel est le nom\n"), input("Quelle est HP\n"), input("Quel est ATK\n"))
            pokemons.append(pokemon3)
        i+=1
    return nom, pokemons