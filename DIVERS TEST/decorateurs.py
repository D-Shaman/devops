# def shout(text):
#     return text.upper()

# print(shout("Hello"))
# yell = shout
# print(yell("Hello"))
# print(shout, yell)

'''
print(shout("hello")) print la chaine hello passée par la fonction shout.
yell = shout permet d'assigner la fonction shout a la variable yell. On crée donc un second objet qui pointe vers le même endroit.
print(yell("Hello")) print la chaine passée par la fonction assignée a yell, qui est shout.
En printant directement les objets shout et yell on peut voir qu'ils pointent le même endroit.
'''

#####################################################################

# def shout(text):
#     return text.upper()

# def whisper(text):
#     return text.lower()

# def greet(func):
#     greeting = func("Hi, I'm cearted by a function passed as an argument")
#     print(greeting)

# greet(shout)
# greet(whisper)

'''
greet(shout) call la fonction greet qui prend un argument (ici, l'argument est shout, donc une fonction).
greeting stocke le résultat de la chaîne passée par la fonction qui est passée en argument de greet (donc shout).
shout prend un argument, ici, la chaîne de func, et la retourn en MAJ.
Puis on print greeting. Donc on print shout("la chaine")
'''

################################################################

def create_adder(x):
    def adder(y):
        return x + y
    return adder

add_15 = create_adder(15)
print(add_15(10))

#print(adder)
print(create_adder)
'''
On crée la fonction adder, qui prend un argument (y) a l'intérieur de la fonction create_adder qui prend elle même un argument (x)
La fonction adder return le résultat souhaité x + y. La fonction create_adder appelle adder dans son return (donc indirectement x+y)
Avec le add_15 = create_adder(15) on assigne a add_15 la fonction create_adder avec 15 comme argument.
Donc quand on print(add_15(10)) on passer l'argument 10 a la fonction adder via la fonction create_adder.
On voie ici qu'on ne peut pas accéder a la fonction adder car elle est nestée dans la fonction create-adder.
'''

def hello_decorator(func):
    def inner1():
        print("Hello, this is before function execution")
        func()
        print("This is after function execution")
    return inner1

def function_to_be_used():
    print("This is inside the function !!")

function_to_be_used = hello_decorator(function_to_be_used)
function_to_be_used()
'''
On défini la fonction hello_decorator qui prend un argument (func).
Qui contient une fonction inner qui ne prend pas d'arguments. Elle contient un premier print.
Puis un appel a la fonction qui est passée en argument a hello_decorator
'''