#! /bin/bash

TYPE=$1
DOC=$2
UCOUNT=0
HCOUNT=1

if [[ $1 == "alert" ]]
then
    DATE=`cat ${DOC} | awk -F"[\[\]]" '$4 == "alerte" {print $2}'| head -n\
    ${HCOUNT}`
    DISPLAY=`cat ${DOC} | awk -F"[\[\]]" '$4 == "alert" {print $5}'| head -n\
    ${HCOUNT}`
    COUNT=`awk -F"[\[\]]" '$4 == "alert" {print $4}' ${DOC} | wc -l`
elif [[ $1 == "notice" ]]
then
    DATE=`cat ${DOC} | awk -F"[\[\]]" '$4 == "notice" {print $2}'| head -n\
    ${HCOUNT}`
    DISPLAY=`cat ${DOC} | awk -F"[\[\]]" '$4 == "notice" {print $5}'| head -n\
    ${HCOUNT}`
    COUNT=`awk -F"[\[\]]" '$4 == "notice" {print $4}' ${DOC} | wc -l`
fi
while [ $UCOUNT -lt $COUNT ]
do
    echo ''
    echo "Liste des message de type  : $1 "
    echo "DATE: $DATE"
    echo $DISPLAY
    ((UCOUNT++))
    ((HCOUNT++))
done
