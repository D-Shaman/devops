<?php

namespace Commun\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fiche
 *
 * @ORM\Table(name="fiche", indexes={@ORM\Index(name="fk_fic_tfi_id", columns={"fic_tfi_id"}), @ORM\Index(name="fk_fic_etf_id", columns={"fic_etf_id"}), @ORM\Index(name="idx_fic_numero", columns={"fic_numero"}), @ORM\Index(name="fk_fic_uti_id_eff_emetteur", columns={"fic_uti_id_eff_emetteur"}), @ORM\Index(name="fk_fic_uti_id_prev_valideur", columns={"fic_uti_id_prev_valideur"}), @ORM\Index(name="fk_fic_uti_id_eff_valideur", columns={"fic_uti_id_eff_valideur"}), @ORM\Index(name="fk_fic_uti_id_resp_fiche", columns={"fic_uti_id_resp_fiche"}), @ORM\Index(name="fk_fic_uti_id_eff_engageur", columns={"fic_uti_id_eff_engageur"}), @ORM\Index(name="fk_fic_uti_id_prev_verificateur", columns={"fic_uti_id_prev_verificateur"}), @ORM\Index(name="fk_fic_uti_id_eff_verificateur", columns={"fic_uti_id_eff_verificateur"}), @ORM\Index(name="fk_fic_uti_id_modifieur_bloc_emission", columns={"fic_uti_id_modifieur_bloc_emission"}), @ORM\Index(name="fk_fic_uti_id_modifieur_bloc_validation_emission", columns={"fic_uti_id_modifieur_bloc_validation_emission"}), @ORM\Index(name="fk_fic_uti_id_modifieur_bloc_engagement", columns={"fic_uti_id_modifieur_bloc_engagement"}), @ORM\Index(name="fk_fic_uti_id_modifieur_bloc_verification", columns={"fic_uti_id_modifieur_bloc_verification"}), @ORM\Index(name="fk_fic_id_fiche_parent", columns={"fic_id_fiche_parent"}), @ORM\Index(name="fk_fic_uti_id_createur", columns={"fic_uti_id_createur"}), @ORM\Index(name="fk_fic_uti_id_modifieur", columns={"fic_uti_id_modifieur"})})
 * @ORM\Entity
 */
class Fiche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fic_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ficId;

    /**
     * @var string
     *
     * @ORM\Column(name="fic_description_faits_consequences", type="text", nullable=false)
     */
    private $ficDescriptionFaitsConsequences;

    /**
     * @var string
     *
     * @ORM\Column(name="fic_action_immediate", type="text", nullable=true)
     */
    private $ficActionImmediate;

    /**
     * @var string
     *
     * @ORM\Column(name="fic_numero", type="string", length=128, nullable=true)
     */
    private $ficNumero;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fic_is_analyse_necessaire", type="boolean", nullable=true)
     */
    private $ficIsAnalyseNecessaire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fic_annulee", type="boolean", nullable=false)
     */
    private $ficAnnulee = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_emission", type="datetime", nullable=true)
     */
    private $ficDateEmission;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_validation", type="datetime", nullable=true)
     */
    private $ficDateValidation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_engagement", type="datetime", nullable=true)
     */
    private $ficDateEngagement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_cloture_actions", type="datetime", nullable=true)
     */
    private $ficDateClotureActions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_echeance_verification", type="datetime", nullable=true)
     */
    private $ficDateEcheanceVerification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_cloture_fiche", type="datetime", nullable=true)
     */
    private $ficDateClotureFiche;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modif_bloc_emission", type="datetime", nullable=true)
     */
    private $ficDateModifBlocEmission;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modif_bloc_validation", type="datetime", nullable=true)
     */
    private $ficDateModifBlocValidation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modif_bloc_engagement", type="datetime", nullable=true)
     */
    private $ficDateModifBlocEngagement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modif_bloc_declencher_verification", type="datetime", nullable=true)
     */
    private $ficDateModifBlocDeclencherVerification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modif_bloc_verification", type="datetime", nullable=true)
     */
    private $ficDateModifBlocVerification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_creation", type="datetime", nullable=true)
     */
    private $ficDateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fic_date_modification", type="datetime", nullable=false)
     */
    private $ficDateModification = 'CURRENT_TIMESTAMP';

    /**
     * @var \Commun\Model\Entity\TypeFiche
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\TypeFiche")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_tfi_id", referencedColumnName="tfi_id")
     * })
     */
    private $ficTfi;

    /**
     * @var \Commun\Model\Entity\EtatFiche
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\EtatFiche")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_etf_id", referencedColumnName="etf_id")
     * })
     */
    private $ficEtf;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_eff_emetteur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiEffEmetteur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_prev_valideur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiPrevValideur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_eff_valideur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiEffValideur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_resp_fiche", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiRespFiche;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_eff_engageur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiEffEngageur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_prev_verificateur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiPrevVerificateur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_eff_verificateur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiEffVerificateur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_modifieur_bloc_emission", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiModifieurBlocEmission;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_modifieur_bloc_validation_emission", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiModifieurBlocValidationEmission;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_modifieur_bloc_engagement", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiModifieurBlocEngagement;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_modifieur_bloc_verification", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiModifieurBlocVerification;

    /**
     * @var \Commun\Model\Entity\Fiche
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Fiche")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_id_fiche_parent", referencedColumnName="fic_id")
     * })
     */
    private $ficFicheParent;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_createur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiCreateur;

    /**
     * @var \Commun\Model\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Commun\Model\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fic_uti_id_modifieur", referencedColumnName="uti_id")
     * })
     */
    private $ficUtiModifieur;


}
