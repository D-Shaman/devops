"""
Ce programme permet la gestion des voyages
"""

from flask import Flask, request
import requests as http
from os import environ

VOYAGE_HOST = environ.get("VOYAGE_HOST", "localhost")
VOYAGE_PORT = int(environ.get("VOYAGE_PORT", 8080))
SERVER_PORT = int(environ.get("SERVER_PORT", 8080))
app = Flask(__name__)

@app.route("/tickets", methods=["GET"])
def ticket_avec_tous_voyages():
    http_response = http.get(f"http://{VOYAGE_HOST}:{SERVER_PORT}/voyages")
    voyages = http_response.json()["results"]
    ticket = ""
    for voyage in voyages:
        ticket += f"""
    Voyage {voyage["titre"]}
    Description {voyage["description"]}
    *************************************************
    """
    return ticket

if __name__ == "__main__":
    app.run(port=SERVER_PORT, host="0.0.0.0")