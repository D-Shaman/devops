# Les requètes : communication avec la base de donnée

## Le CRUD : create, read, update & delete

Le création se fait avec le commande INSERT INTO. On précise ensuite la table, puis les colonnes. Enfin, la commande values, puis les données qui nous intéressent.

```sql
INSERT INTO user (nom, prenom, age) VALUES ("Lefebvre", "Diwan", 26), ("Bob", "Bobby", 41)
```

La partie read est la partie récupération d'informations. On récupère l'info pour la manipuler. Il se fait avec la commande SELECT.

```sql
SELECT * basededonnée.table ;
SELECT ville FROM basededonnée.table;
```

L'update permet de faire une mise a jouer des données liées a une colonne.

```sql
UPDATE table SET colonne = 'Diwan' WHERE id = 10;
```

Le delete permet de suprimer des infos de la DB, sans retours possible.

```sql
DELETE FROM table WHERE id = 11 or id = 12 or id = 13;
```

### Pratique

```sql
SELECT  * FROM auto.adress;
INSERT INTO auto.adress(num, rue, ville, cp) VALUES ("24", "Rue du cul", "Lille", 59000);
```

```sql
SELECT  * FROM auto.adress;
INSERT INTO auto.adress(num, rue, ville, cp) VALUES (24, "Rue du port", "Lille", 59000);

SELECT * FROM auto.student;
INSERT INTO auto.student(nom, prenom, naissance, numero, adress_idadress) VALUES ("Lefebvre", "Diwan", "1994-10-18", 354383, (SELECT idadress FROM adress WHERE rue = "Rue du port"));



SELECT * FROM cabinet.adresses;
INSERT INTO cabinet.adresses(num, rue, ville, cp) VALUES (24, "Rue Lallement", "Lille", 59000);

INSERT INTO cabinet.infirmieres(adeli, nom, prenom, portable, fixe, adresses_idadresses) VALUES 
(99, "Bouchard", "Narine", 0612547485, 0325654575, (SELECT idadresses FROM adresses WHERE rue = "Rue Lallement"));

SELECT * FROM cabinet.infirmieres;
```
