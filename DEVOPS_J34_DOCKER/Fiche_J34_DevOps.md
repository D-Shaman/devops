# Docker_compose

Le docker compose est un fichier permettant :

- De créer des conteneurs
- De sauvegarder des données grace aux volumes
- De créer des configurations
- Automatiser la configuration des conteneurs
- Permet de faire une installations automatique

Le dockerfile permet de créer les images. Une image est une application. Dans le dockerfile on récupère une anciène image (dans la ligne FROM), puis on rajoute une surcouche avec les informations restante du fichier. On a pas besoin de dockerfile pour créer un docker-compose.

