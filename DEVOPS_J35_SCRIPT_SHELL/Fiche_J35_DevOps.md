# Scripting shell

Les commandes externes sont des fichiers au format binaire exécutables, ou des fichiers au format texte représentant un script de commande (en shell, python, perl...).

Les commandes internes sont les cd, ls, etc...

## La substitution dans les noms de fichier

En utilisant [], on peut spécifer la liste de caractères que l'on attend a une position précise dans le nom. Il est également possible d'utiliser les notions d'intervalle et de négation.

```shell
# On liste les fichiers qui commencent par f ou o et se terminent par le caractère '.' suivi d'une lettre.
ls [fo]*.[az]
```

```shell
# Fichier dont le nom compoprte en deuxième caractère une majuscule ou un chiffre ou la lettre i. Les deux premiers caractères seront suivi d'une chaine quelconque.
ls ?[A-Z0-9i]*

#Fichier ne commençant pas par une minuscule
ls [!a-z]*
```

## Les pipe

Un pipe "|" permet a la commande de gauche d'envoyer son résultat dans la commande de droite.

La sortie d'erreur de la commande de gauche ne part pas dans le tube. Il est aussi important quand la commande de gauche envoie de l'information.
