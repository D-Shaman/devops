# Le Cloud

Les différences entre un serveur dédié et le cloud : 

Payement au mois pour le serveur dédié (environ 50 euros par mois).

L'entreprise gèrre le fonctionnement du CPU, de la RAM et des disques. Ce n'est pas forcement une machine dédiée, mais parfois une machine virtuelle. Ils mettent un hypervisuer de première génération (qui remplace donc l'OS - les hyperviseurs de seconde génération sont du type VirtualBox).

Rappel : l'os permet de donner des instruction a la machine. Il n'y a donc par de communication directe entre le système et l'utilisateur (app ou utilisateur physique), tout passe pas l'OS.

L'hypervisuer de première génération embarque un OS amélioré pour les machines virtuelles.

Certaines entreprises mettent l'OS de base, et permettent juste de mettre des logiciels préinstalés que l'on ne choisi pas : c'est le PaaS (Plateforme as a service). Dans certaines entreprises, on choisi le matériel, l'OS, et les logiciels, c'est l'IaaS (Infrastructure as a service).

Eb cloud, le choix est beaucoup plus granulaire, on peut choisir précisément le processeur / la ram / le nombre de coeurs, le stockage, etc... Il faut aussi prendre en compte le mode de payement : a l'utilisation (on paye pour le temps utilisé, heureu, minute ou seconde), ou au mois (en général, le serveur dédié est payé au mois).

Le cloud coute entre 30 et 50% plus cher pour une même machine en serveur dédiée, mais elle permet de mettre en ligne plus de puissance aux heures ou le service est beaucoup utilisé.

Le cloud utilise souvent les outils du big data. Ce sont des logiciels qui peuvent démarer avec des petits besoins mais qui sont **scalables** (dont le puissance peut être accrue). Donc capable de fonctionner sur une configuration minimale, mais aussi faire face a une nombre gigantesque d'utilisateurs.

    La scalbilité verticale : mon ordinateur manque de puissance, j'augmente mon nombre de CPU. C'est toujours un mauvais choix.

    Scalabilité horizontale : je multiplie le nombre d'ordinateurs et je les fait communiquer entre eux. Quand je manque de puissance sur une ordinateur, j'en demarre un deuxième, et ainsi de suite.

Dans un cas de scalabilité horizontale, si trois machines assurent 60 % du service, et que l'une casse, les deux restantes récupèrent la charge en augmentant leurs performances jusqu'a ce que le troisième ordinateur soit réparé. Dans le cas d'une scalabilité verticale, si l'ordinateur tombe, le système est down.

La logique est la même pour la construction de processeurs. Les constructeurs essayent dans une moindre mesuer d'augmenter la puissance, mais d'augmùenter le nombre de coeurs, pour réduire la chauffe du matériel.

Avec une base de donnée noSQL : 
    
    Rappel les 3 V : volume, variété, vélocité.

Par exemple Cassandra qui est une BDD aux grosses capacités d'écriture. Cette base de donnée peut écrire simultanément sur plusieurs ordinateurs pour améliorer la vitesse d'écriture. Elle permet aussi des systèmes de copies de données, pour la sécurité de l'information.

Avec le raid : au lieu d'avoir un seul disque dur, on a un ensemble de disque dur liés entre eux. Certains disques sont en HDFS, c'est une technologie qui permet de faire en sorte que les disques durs soient dilués sur plusieurs machines. Le point positifs est que les disques peuvent être éparpillés sur des kilomètres. Donc si une infrastructure prend feu, les backups existent encore. C'est une forte scalabilité horizontale.

## Qu'est-ce que le big data

On a une base de donnée appele un DataLake, elle peut prendre en compte une très grande quantité de données. Elles y sont entreposées. Ca peut aussi être un serveur de fichiers. Par exemple un OneDrive peut être considéré comme un Datalake s'il contient plusieurs terra de données.

    Par exemple Cassandra, GCP (google cloud platform), même MongoDB, S3 (serveur de fichier Amazon).

Il doit donc y avoir du volume, une certaines vélocité (écrire beaucoup de données en même temps) et de la variété (Json, XML, images, etc...), donc les 3V du NoSQL. C'est "un fourre-tout".

L'objectif du big data est de récupérer une masse considérable de données (via des scrappers et autre) depuis laquelle on va pourvoir extraire de l'information de la traiter, afin en pouvoir en faire émerger des tendances ou des effets de manière spécifique.

- Donc la première étape est de savoir créer de la plusvalue sur de la donnée.
- Ensuite il faut déterminer les données a rapatrier.
- Nettoyer les données et les filtrer, par exemple avec des pipelines en python. Cela est fait en script de quelques lignes.
  - Retrait des balises HTML
  - Passer tout le texte en minuscule
  - Agrégation regroupement de données et croisement
- Sauvegarde dans une base de donnée structurée / semi structurée (appellée un Warehouse). Ce sont les données métier.
- Les données sont structurées par métier dans des DataMart.
- Puis d'autres filtres et modifications peuvent être appliquées.
- Enfin, des statistiques sont faites. Puis, parfois, traitées via intelligence artificielle (machine learning).

Ici on peut ajouter des V, comme la véracité des données et la valeur de la donnée.

## Les technologies

Pour le traitement python, on a SPARK (filtre, agrégation, modifications). Cette bibliothèque est scalable horizontalement si besoin.

Beautiful soup, une bibliothèque de scrapper, qui va permettre de récupérer des éléments d'une pagère web. Il est aussi possible d'utiliser Sélénium ou Scrapy.

Dataviz (transformaer des données sous forme de graphique) : ELK. En python, Plotly, mathplotlib.

## GCP (Google Cloud Platform)

