## a quoi sert docker

installer, configurer, limiter les ressources

## telecharger une image

- docker pull 'nom du conteneur'

## lancer une image

- docker run 'nom du conteneur'

## exemple avec mysql

```
docker run --name test-mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:latest
```

- -e permet de définir des variables d'environnement

- -d permet de le lancer en détaché

- mysql: permet de définir la version de mysql à pull

## stopper un conteneur

- docker stop 'id du conteneur'

ou

- docker stop 'nom du conteneur'

## supprimer un conteneur

- docker rm 'id du conteneur'

ou

- docker rm 'nom du conteneur'

## entrer dans le conteneur

```
docker exec -ti 'nom du conteneur' bash
```

- -ti définit le terminal interactif connecté à notre conteneur (ici de type bash)
