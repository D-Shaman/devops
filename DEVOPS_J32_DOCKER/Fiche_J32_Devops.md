# Dockers

Au plus bas niveau on a le hardware (la RAM, le CPU, etc..). L'OS fait le lien entre le hardware et le software. Ces appli ont besoin de puissance de calcul.

Une VM est un logiciel qui permet d'émuler un ordinateur. La VM est composée d'un OS, mais ne possède pas de hardware. La VM communiquer donc avec l'OS de la machine pour fonctionner (par rapport aux ressources pour la faire tourner). C'est l'hyperviseur qui fait le lien entre la VM et l'OS.

Le problème c'est qu'une VM prend beaucoup de ressources. A l'époque on utilisait des VM pour le partage applicatif. A notre époque, on passe par Docker.

Docker ne possède pas d'OS, il utilise l'OS de la machine. Il va donc posséder l'applicatif. Cepedant Docker n'est pas directement lié a l'OS, mais est lié a une VM linux. Cette VM linux est ensuite connectée a l'OS. Sous Linux, Docker communique directement avec l'OS.

Les trois intérets de dockers sont :

- L'installation : Permet l'installation simple d'applications.
- La configuration : on configure ce que l'on désire.
- Les ressources : On détermine le nombre de CPU, la mémoire..

On passe en fait par la VM pour une question de sécurité. Car les conteneurs ne sont pas sécurisés.

## Les conteneurs et les images

Les images sont des applications. Par exemple MySQL et tout ses fichiers, python, etc...

Le conteneur sert a cloisoner l'ensemble des application. On communique avec le conteneur pour travailler avec les applications.

De base, dans le conteneur, les images vont pouvoir communiquer ensemble.

Docker peut s'utiliser en ligne de commande.

```shell
# Le -e crée les variables d'environnement
# -d détache la console
# Via -p on peut assi préciser un port

docker run --name mysql -e MYSQL_ROOT_PASSWORD=zizi -d mysql:tag

docker run --name mysql-5-7 -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7 -p 3310:3306
```

Voir les conteneurs actifs en ce moment:

```shell
docker ps
```

Pour supprimer un conteneur :

```shell
docker rm -f CONTAINER_ID
```

Pour rentrer dans le conteneur :

```shell
# -it pour le terminal interactif
docker exec -it michel bash
```

## Les volumes

Permettent de lier l'endroit ou l'on stock nos db, pour que tout ce qu'on fait soit sauvegardé. On peut le faire dans la VM qui contient le conteneur.

./ : on agit sur windows
/ on agit sur linux

```shell
docker run --name bernard  -v C:\DEV\DEVOPS_J32_DOCKERS\db_test:/var/lib/mysql -p 3310:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7

# Avec le -v on lui précise de créer un lien physique, ici dans windows
```

## L'utilisation

En générale on utilise des fichiers docker_composer pour plusieurs actions avec docker.

Le dockerfile crée les images, la docker-compose crée les les containers. On peut lancer plusieurs conteneurs ensemble qui vont travailler sur une même chose.

On renseigne la version de docker-compose. Certaines images ne sont pas compatibles avec docker compose, il faut donc regarder la doc.

Les services sont les containers. Il faut lui lier une image. Puis une ligne image. Puis un restart: unless-stopped (il se relance en cas d'erreur, ou le always qui relance qu'il y ait une erreur ou pas d'erreur). Puis le container name.

Dans environment on renseigne des variables d'environnement.

Plus bas sur la même indentation j'ai un autre container : adminer. On lance aussi une image, une commande restart, mais aussi un port.

Dans le dossier du docker compose :

docker compose up -d
docker compose down

Pour ajouter un volume :
