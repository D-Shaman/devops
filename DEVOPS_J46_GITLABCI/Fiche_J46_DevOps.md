# CICD

Quand on développe un logiciel, il y a plusieurs phases, par exemple : la phase de test qui contient les test unitaires, les tests fonctionnels, les tests de non régression, les tests de montée en charge, tests d'intégration...

Mais aussi des tests de code quality comme SonarQube (analyse de bugs, conventions de code). L'objectif est d'améliorer la qualité de code qui va être fournie.

Il existe aussi la phase de build (la phase de construction) : créer un package d'installation qui va installer l'application. Mais aussi la création de l'image Docker.

Ces deux phases peuvent être automatisées. A l'époque le développement se faisait en cycle en V : une analyse de besoin, découpe du projet, puis livraison de l'application final. Le client ne voyait donc que le produit final. Donc si l'application n'est pas en adéquation avec le besoin du client, le développement doit reprendre.

Dans le développement Agile, les sprints durent une a deux semaines, et les tests évoqués précedémement doivent être effectués a la fin de chaque cycle. Il est donc absolument nécessaire d'automatiser. C'est a ça que servent les outils comme Jenkins, GITLABCI, et GitHub Actions ou Travis. Ces outils permettent de mettre en place de l'intégration continue et du déploiment continu.

La CICD implique un versionning : tous les tests a effectuer doivent être déclencher a chaque push sur la branche develop, ou a chaque commit. Concrètement, un pipeline est crée, composé d'un ensemble de jobs, qui sont des scripts de vérifications. Ils peuvent être déployés que sur certaines branches, ou juste sur les commits ou sur les push.

## Révision docker

Dans Docker on a des images, qui sont unique. On peut en créer plusieurs conteneurs différents (ils ont leur propre cycle de vie, mais ils ont la même image). Le docker-compose est une vision macro qui permet de regrouper le lancement de plusieurs services et leur configuration (couple image/conteneur).

## Programme : architecture complete

**Première étape :** *Setup du GitLab avec docker compose*

    On créer un docker-compose constitué de :

    - La version de docker compose
    - Les services dont le nom est le nom de DNS de docker
    - Image (si elle existe) ou build (s'il faut  construire sa propre image)
    - Les ports
    - Le nom du conteneur
    - La méthode de restart
    - Le hostname
    - Les variables d'environnement (ici GITLAB_OMNIBIS_CONFIG)
    - volumes
    - network

    Pour avoir les logs du conteneur : docker-compose logs -f
    Pour voir les containers actifs (afficher les services) : docker_compose ps
    Arréter le service : docker-compose down
    inspecter un réseau : docker network ls
    downker network inspect <nom du réseau>

Pour l'intégration continue on a besoin de runner, qui sont des machines qui vont exécuter les scripts.

Il y a deux type : les shared et les specific (les shared sont ceux de gitlab) et les specific (qu'on va créer nous même). On peut le créer dans le docker compose

    Le runner se setup comme nimporte quelle image.
    Sauf qu'il faut l'enregistrer en rentrant dans le conteneur avec la commande :  docker exec -it <nom du container sur lequel on souhaite connecter le runner> gitlab-runner register, puis rentrer le token présent sur la page gitlab
    restart le runner : docker exec -it runner1 gitlab-runner restart
