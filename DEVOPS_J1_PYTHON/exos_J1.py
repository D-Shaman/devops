#déclarer une chaine de caractère, un entier, un boolean, un nombre décimal, une liste, un tuple

chaine_de_caracteres = 'Chaine de caractères'
entier = 1
is_boolean = True
decim = 6.25
list = ["elem1", "elem2", "elem3"]
tuple = (1, 2)
# ou tuple = 1, 2

# corriger l'erreur de déclaration de variable
ip = "127.0.0.1"

 # concaténer un nombre entier et une chaine de caractères
 # # le résultat doit être : son age est 15

first_part = "son âge est de "
second_part = 15

phrase = first_part + str(second_part)
print(phrase)

#On peut aussi faire :
#print(f'son âge est {second_part}')

# lire une variable
a = 10
print(a)

# afficher le contenu des variables avec un séparateur 
a, b, c = 10, 5, 47

print(f'{a}/{b}/{c}')
#On peut aussi faire 
#print(a,b,c, sep='/')