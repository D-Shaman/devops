#! /bin/bash

if [[ $# -gt 0 ]]
then

    for user in $@
    do
        if [[ -n $(cat /etc/passwd | grep $user) ]]    
        then
            echo "L'utilisateur "$user" fait tourner $(pgrep -u $user | wc -l)
            processus en ce moment."
        else
            echo "L'utilisateur n'existe pas"
        fi
    done

fi
