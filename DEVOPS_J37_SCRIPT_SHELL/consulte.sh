#! /bin/bash

#read -p "Quel est le dossier que vous voulez scanner ?" DIR 
QPATH=$1

echo $QPATH

if [[ -z $@ ]]
then
    echo "ok"
    QPATH=*
fi

for file in $(find $QPATH -maxdepth  1)
do
    if [[ -f $file && -r $file ]]
    then
        read -p "Voulez vous consulter le fichier $file ? [o/n/q]" choice
        if [[ $choice == "o" || $choice == "O"  ]]
        then
            less $file
        elif [[ $choice == "q" || $choice == "Q" ]]
        then
            exit
        fi 
    fi
done