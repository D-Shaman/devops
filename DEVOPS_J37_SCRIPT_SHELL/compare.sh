#! /bin/bash

a=`ls -l $1 | awk '{print $5}'`
b=`ls -l $2 | awk '{print $5}'`

if [[ $# = 2 ]] && [[ -f $1 && -f $2 ]]
then

    if [[ $a -gt $b ]]
    then
        echo "Le fichier $1 a la plus grande taille: $a octets"
    elif [[ $a -lt $b ]]
    then
        echo "Le fichier $2 a la plus grande taille: $b octets"
    fi

fi