#! /bin/bash

SEP=","
SEP2="[,-']"
echo "$SEP2"

echo "1 - Afficher uniquement les hommes"
echo "2 - Afficher uniquement les femmes"
echo "3 - Sélection par année de naissance"
echo "4 - Sélection par première lettre"
echo "5 - Afficher les hommes nés avant 1960"

read -p "Quel est votre choix " choice

case $choice in
    1)
        cat employees.txt | awk -F $SEP '$5 == "\x27M\x27" {print $3 $4 $5}'
        ;;
    2)
        cat employees.txt | awk -F $SEP '$5 == "\x27F\x27" {print $3 $4 $5}'
        ;;
    3)
        read -p "Quelle est l'année en question ? " YEAR
        cat employees.txt | awk -F $SEP2 '$3 < '$YEAR' {print $3 " - " $8 " " $11}'
        ;;
    4)
        read -p "Quelle est votre lettre ? " LETTER
        cat employees.txt | awk -F $SEP 'match($3,"'$LETTER'") {print $3 $4}' 
        ;;
    5)
        cat employees.txt | awk -F $SEP2 '($3 < 1960) && ($14 == "M")\
        {print $3 " " $8 " " $11 " " $14}'
        ;;
    *)
        echo "Ne mets pas tes testicules dans la porte du métro, tu vas te pincer très
        fort !"
esac