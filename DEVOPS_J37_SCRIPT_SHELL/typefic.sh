#! /bin/bash

i=1

for args in $@
do
    if [[ $args == *doc ]]
    then
        echo "$args est un fichier DOC"
    elif [[ $args == *pdf ]]
    then
        echo "$args est un fichier PDF"
    else
        echo "Ni DOC ni PDF"
    fi
((i++))
done
