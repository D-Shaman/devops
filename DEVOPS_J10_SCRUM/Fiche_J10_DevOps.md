# FrameWork SCRUM

Infos diverses :

- La sprint review contient la démo et le client est bien évidement présent. Juste après que le client soir parti, on commence la sprint rétrospective.
- Résumé (coloré) du scrum workflow.

![scrum-workflow](scrum_workflow.png)

- La longueur d'un sprint devait être assez courte, mais quand même assez longue pour permettre un travail significatif. La longueur traditionnelle est entre 2 semaines et 1 mois.
- Le SCRUM n'élimine pas la complexité du sujet, n'est pas une méthode (mais un framework). Si le framework est choisi, il faut le faire entièrement (on ne peut pas juste en choisir des élements). Chaque composante sert un but spécifique et complexe.
- Il n'y a pas de "manager du projet"
- Si le CEO veut ajouter un item a un sprint en cours, alors il faut en informer les autres membres de la team, pour que l'équipe puisse décider ce qu'il faut faire. Surtout par rapport au sprint goal.
- Une feature est ajoutée quand elle correspond a la définition of done.
- La DoD permet que comprendre quand une US est terminée.

![ile](ile.png)

## La certification

Il en existe plusieurs : 
[Scrum.org](https://scrum.org)
