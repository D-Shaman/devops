# LINUX

Outils ssh : MobaXterm, Putty.

## Les utilisateurs et les droits.

On utilise la commande useradd : 

```shell
# On créer l'utilisateur Michel (ainsi que son dossier) et set son password. -s ajouter un bash.
sudo useradd -m Michel -p 1234 -s /bin/bash
```

Pour se connecter comme un autre utilisateur :

```shell
su Michel
```

Pour supprimer un autre utilisateur

```shell
sudo userdel Michel
```

Pour les autorisations on peut utiliser :

```shell
#Je donne le droit d'execution de mon fichier a user
sudo chmod u+x monfichier

#Je donne le droit de lecture a tout le monde pour le dossier
sudo chmod a=w mondossier

#En octale (base 8) : lecture ecriture exec pour l'utilisateur, lecture et exécution pour les autres.
sudo chmod 775 mondossier
```

## Réseau

La plupart des communications sur le réseau se fait par le biais de câble RJ45, également appelés "cable ethernet" en référence aux normes IEE* 802.3. Le cable RJ45 est constitué de 4 paires de câbles en cuivre. La transmission d'information se fait grâce à la variation de la tension sur une paire.

Ressources Ben Eater (youtube).

Les données représentées par les niveaux de tension sont appelés symboles. Dans le cadre de la transmission d'information, la fréquence à laquelle les symboles sont envoyés est mesurer en baud (symboles par secondes). Dans la pratique le signal électrique n'est pas parfaitement carré.

Plusieurs méthodes sont possible pour déterminer l'intervalle de lecture (le moment ou la clock va lire le signal). La plus simple étant l'utilisation d'un signal horloge qui indique à la carte réseau quand lire l'information.

Pour la fibre, la transmission peut se faire via fibre optique.

L'autre moyen de transmettre un signal est le wifi. Un signal électrique sinusoidal a haute fréquence est envoyé dans une antenne. Les symboles sont différenciés par mosulation de phase : changement de sens de la sinusoïde. On utilise le wifi pour des utilisations non critique dans le cas ou le rj45 est impossible.

Même si les technologies du Wifi s'améliorent, le débit, la qualité du signal, et sa portée, en font une technologie consommateur. Pour augmenter le débit d'une transmission, il faut augmenter la fréquence, ce qui baisse la portée.

### Echelle du réseau

PAN : personal area network
LAN : local area network
MAN : metropolitan area network
WAN : world area network

Pour un routeur : les port jaune représentent le WAN (transmission vers l'extérieur) et bleu le LAN (vers l'intérieur du réseau).

## Configuration réseau

### L'adresse MAC

Pour communiquer sur un réseau, nos machines utilisent une carte réseau (carte wifi, RJ45, GBIC pour la fibre). Pour être identifiable sur le réseau, chaque carte est identifiée grâce a une adresse MAC (Media Access Control). Cette adresse, aussi appellée adresse physique car elle référence à la couche 1 du modèle OSI.

Cette adresse est constituée de 12 caractères hexadécimaux: les 6 premiers caractères est propre au constructeur. Il n'est pas recommandé de changer son adresse mac.

### L'adresse IP

Pour communiquer sur un réseau, notre cartr réseau a besoin d'informations. A commencer par son adresse IP. Elle est **statique** ou **dynamique**.

En général les IP statiques sont attribuées aux serveurs afin de la retrouver facilement sur le réseau. Elles sont configurées.

Il y a deux types d'IP : IpV4 et IpV6. Une IPV4 est composée de 4 séries de 8bits pour une longueur totale de 32 bits (4 paquets de chiffres en base 10 de 0 a 255).

L'IPV6 est constituée de 8 paquets de 2 octets, separés par des ":", soit 128 bits. Les paquets sont notés en hexadécimal.

L'adresse 127.0.0.0 est le loopback ( l'adresse ip du réseau virtuel de la machine).

### Les masques de sous-réseau

Permet de la liaison entre l'host et le réseau. La carte réseau connait l'IP, mais elle a besoin de savoir sur quemme réseau elle communique. Pour cela elle utilise un système de maque bianire pour différencier la partie réseau et la partie hote de l'adresse IP.

Le masque utilise le même format de l'adresse IP. Il sert a optimiser le nombre d'hôte au sein du réseau. Pour cela on attibut un certain nombre de bit, de gauche a droite, pour éliminer...

### Le DNS

Un serveur DNS est un serveur qui garde en mémoire l'adresse IP associée a un nom de domaine. Si il be l'a pas en mémoire, il dialogue avec d'autres serveurs DNS pour obtenir l'information.

### Les outils réseau




```shell
```
```shell
```
```shell
```
```shell
```

