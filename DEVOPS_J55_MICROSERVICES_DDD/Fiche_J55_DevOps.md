# Microservices et DDD

Le MicroService et le monolithique sont deux type d'architecture logiciel. Les architectures monolithiques sont des systèmes qui centralisent l'ensemble des fonctionnalités pour une application. Ces architectures sont en général des services, d'une couche d'accès aux données, et d'une couche MVC/HTML/JS.

Une application monolithique est un seul bloc, c'est un immense grate-ciel avec tout ce qu'il faut pour vivre dedans: le problème c'est que ces batiments peuvent être labirynthiques. Un autre problème est que retirer un bug ou une feature peut toucher a l'intégrité du système entier.

Un autre problème est la scalabilité (dimensionner) : c'est le fait de faire grandir son application, et augmenter sa puissance pour s'adapter au flux de donnée. La scalabilité verticale se rapporte a la puissance physique des serveurs. La scalabilité horizontale est le fait de copier son code vers un nouveau serveur. Le monolithique est difficile a rendre scalable.

Au niveau du microservice, on découpe le programme au maximum, c'est une méthode bien plus facile a scaller. C'est une conception qui est au final identique aux SOA. Cependant les SOA sont géénralement basées sur le protocole SOAP alors que les microservices se basent sur l'architecture REST (mettre en place des API REST, avec une communication HTTP).

Les microservices ont une encapsulation et une séparation des domaines beaucoup plus forte tout en gardant un découplage lâche. Les microservices définissent un domaine par service, il y a décomposition des applications.

Dans ce type d'architecture, un service n'a qu'une seule et unique fonction.

![microservices.png](microservices.png)

Cette méthode a pour inconvénient de nécessiter la mise en place de beaucoup de technologies.

## Le microservice

Les microservices communiquent avec une passerelle d'API (un gateway).