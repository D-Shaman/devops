import math

class Point:
    def __init__(self, x1, y1, x2, y2):
        self.set_x1(x1)
        self.set_y1(y1)
        self.set_x2(x2)
        self.set_y2(y2)
        self.calculation()
        self.display()
    
    def set_x1(self,x1):
        self._x1 = x1
    def get_x1(self):
        return self._x1
    
    def set_y1(self, y1):
        self._y1 = y1
    def get_y1(self):
        return self._y1

    def set_x2(self, x2):
        self._x2 = x2
    def get_x2(self):
        return self._x2

    def set_y2(self, y2):
        self._y2 = y2
    def get_y2(self):
        return self._y2

    def calculation(self):
        self._dist = math.sqrt((int(self.get_x2())-int(self.get_x1()))**2 + \
            (int(self.get_y2())-int(self.get_y1()))**2)
        return self._dist

    def display(self):
        print(f"La distance entre P1 et P2 est de {self.calculation()}")

# def get_data():
#     print("Premières coordonnées \n")
#     x1 = (input("Entrez x1 : \n"))
#     y1 = (input("Entrez y1 :\n"))
#     print("Secondes coordonnées\n")
#     x2 = (input("Entrez x2 : \n"))
#     y2 = (input("Entrez y2 :\n"))
#     return x1, y1, x2, y2

# x1, y1, x2, y2 = get_data()
# point1 = Point(x1, y1, x2, y2)