from pokemon import Pokemon

class Wtype(Pokemon):
    def __init__(self, name, hp, atk):
        super().__init__(name, hp, atk)
    
    def get_type(self):
        return "Eau"
    
    def roll_id(self):
        return str(super().display_id()) + str(f" Mon type est {self.get_type()}.")


# pokemon1 = Wtype("Carapute", "1000", "100")
# print(pokemon1.roll_id())