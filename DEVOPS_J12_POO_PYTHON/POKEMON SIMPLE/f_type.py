from pokemon import Pokemon

class Ftype(Pokemon):
    def __init__(self, name, hp, atk):
        super().__init__(name, hp, atk)
    
    def get_type(self):
        return "Feu"
    
    def roll_id(self):
        return str(super().display_id()) + str(f" Mon type est {self.get_type()}.")


# pokemon1 = Ftype("Dracofeu", "1000", "100")
# print(pokemon1.roll_id())