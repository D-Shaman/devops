from f_type import Ftype
from w_type import Wtype
from p_type import Ptype
import time
import random

class Combat:
    def __init__(self, pokemon1, pokemon2):
        self._pokemon1 = pokemon1
        self._pokemon2 = pokemon2
        self.type_avantage()
        self.baston()
        self.baston_display()

    def type_avantage(self):
        self._a =0
        if self._pokemon1.get_type() == "Feu" and self._pokemon2.get_type() == "Plante":
            self._a = 2
        elif self._pokemon1.get_type() == "Eau" and self._pokemon2.get_type() == "Feu":
            self._a =2
        elif self._pokemon1.get_type() == "Plante" and self._pokemon2.get_type() == "Eau":
            self._a = 2
        else :
            self._a = 1
        return self._a

    def switch_sides(self):
        temp = self._pokemon1
        self._pokemon1 = self._pokemon2
        self._pokemon2 = temp

    def baston(self):
        while self._pokemon1.isKO() == False and self._pokemon2.isKO() == False:
            if self.crit() == True:
                print("Nouveau tour: \nC'est un coup critique !!")
                print(f"{self._pokemon1.get_name()} attaque {self._pokemon2.get_name()} pour une valeur de {((self._pokemon1.get_atk()*self.type_avantage())*2)}")
                self._pokemon2.set_hp(self._pokemon2.get_hp() - ((self._pokemon1.get_atk()*self.type_avantage())*2))
                print(f"Les HP de {self._pokemon2.get_name()} sont {self._pokemon2.get_hp()}")
                print(f"Les HP de {self._pokemon1.get_name()} sont {self._pokemon1.get_hp()}\n")
                self.switch_sides()
                time.sleep(2)
            if self.miss() == True:
                print(f"Nouveau tour: \n{self._pokemon1.get_name()} rate son attaque !!")
                self._pokemon2.set_hp(self._pokemon2.get_hp() - (self._pokemon1.get_atk()*self.type_avantage())*0)
                print(f"Les HP de {self._pokemon2.get_name()} sont {self._pokemon2.get_hp()}")
                print(f"Les HP de {self._pokemon1.get_name()} sont {self._pokemon1.get_hp()}\n")
                self.switch_sides()
                time.sleep(2)
            else:
                print(f"Nouveau tour: \n{self._pokemon1.get_name()} attaque {self._pokemon2.get_name()} pour une valeur de {self._pokemon1.get_atk()*self.type_avantage()}")
                self._pokemon2.set_hp(self._pokemon2.get_hp() - self._pokemon1.get_atk()*self.type_avantage())
                print(f"Les HP de {self._pokemon2.get_name()} sont {self._pokemon2.get_hp()}")
                print(f"Les HP de {self._pokemon1.get_name()} sont {self._pokemon1.get_hp()}\n")
                self.switch_sides()
                time.sleep(2)

    def crit(self):
        coucrit = False
        i = random.randint(0,10)
        if i == 0:
            coucrit = True
        return coucrit

    def miss(self):
        coumiss = False
        j = random.randint(0,10)
        if j == 0:
            coumiss = True
        return coumiss

    def baston_display(self):
        return (f"Ce round de combat finit avec {self._pokemon1.get_name()} à \
{self._pokemon1.get_hp()} HP et {self._pokemon2.get_name()} à {self._pokemon2.get_hp()} HP")