class Pokemon:
    def __init__(self, name, hp, atk):
        self.set_name(name)
        self.set_hp(hp)
        self.set_atk(atk)
        self.isKO()
        #self.fight()
        #self.display_id()
    
    def set_name(self, name):
        self._name = name
    def get_name(self):
        return self._name

    def set_hp(self, hp):
        self._hp = int(hp)
    def get_hp(self):
        return self._hp
    
    def set_atk(self, atk):
        self._atk = int(atk)
    def get_atk(self):
        return self._atk

    def isKO(self):
        isKO = False
        if int(self.get_hp()) <= 0:
            isKO = True
        return isKO
    
    def fight(self, pokemon2):
        pokemon2.set_hp(pokemon2.get_hp()- self.get_atk()) 

    def display_id(self):
        return (f"Je suis le Pokemon {self.get_name()}, je dispose de {self.get_hp()} points de vie, et j'ai une stat d'attaque a {self.get_atk()}.")
