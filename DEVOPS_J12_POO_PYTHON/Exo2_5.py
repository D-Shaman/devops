class Complex:
    def __init__(self, reel1, imag1, reel2, imag2):
        self.set_reel1(reel1)
        self.set_imag1(imag1)
        self.set_reel2(reel2)
        self.set_imag2(imag2)
        self.calcreel()
        self.calcimag()
        self.message()
    
    def set_reel1(self, reel1):
        self._reel1 = reel1
    def get_reel1(self):
        return self._reel1
    
    def set_imag1(self, imag1):
        self._imag1 = imag1
    def get_imag1(self):
        return self._imag1

    def set_reel2(self, reel2):
        self._reel2 = reel2
    def get_reel2(self):
        return self._reel2

    def set_imag2(self, imag2):
        self._imag2 = imag2
    def get_reel2(self):
        return self._imag2

    def calcreel(self):
        self._part_reel = int(self._reel1) + int(self._reel2)
        return self._part_reel
    
    def calcimag(self):
        self._part_imag = int(self._imag1) + int(self._imag2)
        return self._part_imag
    
    def message(self):
        print(f"La somme est {self.calcreel()} + {self.calcimag()}i")

# def get_data():
#     print("Premier nombre \n")
#     reel1 = (input("Entrez la partie réelle : \n"))
#     imag1 = (input("Entrez la partie imaginaire :\n"))
#     print("Second nombre\n")
#     reel2 = (input("Entrez la partie réelle : \n"))
#     imag2 = (input("Entrez la partie imaginaire :\n"))
#     return reel1, imag1, reel2, imag2

# reel1, imag1, reel2, imag2 = get_data()
# complex1 = Complex(reel1, imag1, reel2, imag2)