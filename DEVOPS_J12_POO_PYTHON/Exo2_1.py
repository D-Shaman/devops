class Rectangle:
    def __init__(self, longueur, largeur):
        self.set_longueur(int(longueur))
        self.set_largeur(int(largeur))
        self.surface()
        self.message()
    

    def set_longueur(self, longueur):
        self._longueur = longueur
    
    def get_longueur(self):
        return self._longueur
    
    def set_largeur(self, largeur):
        self._largeur = largeur

    def get_largeur(self):
        return self._largeur


    def surface(self):
        self._surface = self._largeur * self._longueur
        return self._surface

    def message(self):
        print(f"Avec une longueur de {self.get_longueur()}, et une largeur de {self.get_largeur()}, alors la surface est de {self.surface()}")
