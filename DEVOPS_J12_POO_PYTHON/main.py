from Exo2_1 import Rectangle
from Exo2_2 import Somme
from Exo2_3 import Student
from Exo2_4 import Student2
from Exo2_5 import Complex
from Exo2_6 import Point

#Exo2_1.py : Rectangle
rectangle1 = Rectangle(input("Longueur ? \n"), input("Largeur ? \n"))

#Exo2_2.py : Somme
calcul1 = Somme(input("Quel est vôtre premier nombre ?\n"), input("Quel est le second nombre ?\n"))

#Exo2_3 : Student
étudiant1 = Student(
    input("Quel est vôtre nom ?\n"),
    input("Quelle est vôtre première note ?\n"),
    input("Quelle est vôtre seconde note ?\n")
)

#Exo2_4 : Student2
def get_data():
    nom = input("Quel est vôtre nom ?\n")
    notes = []
    i = 0
    nbr_notes = input("Combien de notes souhaitez vous rentrer ?\n")
    while i < int(nbr_notes):
        new_note = int((input("Quelle est la note ?\n")))
        notes.append(new_note)
        i += 1
    return nom, notes

nom, notes = get_data()
student1 = Student2(nom, notes)

#Exo2_5 : Addition complexe
def get_data():
    print("Premier nombre \n")
    reel1 = (input("Entrez la partie réelle : \n"))
    imag1 = (input("Entrez la partie imaginaire :\n"))
    print("Second nombre\n")
    reel2 = (input("Entrez la partie réelle : \n"))
    imag2 = (input("Entrez la partie imaginaire :\n"))
    return reel1, imag1, reel2, imag2

reel1, imag1, reel2, imag2 = get_data()
complex1 = Complex(reel1, imag1, reel2, imag2)

#Exo2_6 : Points
def get_data():
    print("Premières coordonnées \n")
    x1 = (input("Entrez x1 : \n"))
    y1 = (input("Entrez y1 :\n"))
    print("Secondes coordonnées\n")
    x2 = (input("Entrez x2 : \n"))
    y2 = (input("Entrez y2 :\n"))
    return x1, y1, x2, y2

x1, y1, x2, y2 = get_data()
point1 = Point(x1, y1, x2, y2)

#Exo 2_7