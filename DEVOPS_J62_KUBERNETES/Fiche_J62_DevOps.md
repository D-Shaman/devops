# Kubernetes

Deux type : 
Soit on passe par un déploiement, soit on en fait pas.

Kubernetes permet de faire de l'orchestration de conteneurs : de les faire travailler ensemble, de gérer le load balancing, etc...

L'élement le pluis petit est le pod. On met un élément dedans... Comme une base de donnée, une API ou une APP. Dans un un POD on peut mettre plusieurs applications mais ce n'est pas recommandé. 
Dans un POD on peut mettre en place les réplica. Ils permettent de répliquer les conteneurs au cas ou il crash. Dans APP on va définir un certain nombre de replica. Si APP tombe, Kubernetes choisi l'un des replica qui va devenir le nouveau master. L'ancien APP qui est tombé est relancé est devient slave de nouveau APP.

Les pods peuvent communiquer entre eux dans le même namespace. Il n'est pas recommandé de faire un lien direct entre les pods, mais d'utiliser un service qui va permettre la communication des pods entre eux. Il y a un service par pod. Les services n'ont pas d'accès direct a l'extérieur, ils doivent passer par ingress (qui agit comme un middleware). L'ingress doit avoir un ingress controller qui va gerrer l'allocation d'adresse IP (c'est l'ingress controller). Il y a aussi un load balancer entre l'ingress et l'utilisateur.

Les fichiers nécessaire sont un yml. On se connecte a l'application en HTTP (localhost pour ce cours en particulier).

Au niveau des fichiers : 

- Définir les pods. Un fichier par pod.
- Définir les services.
- Définir le namespace (pour conpartimenter les pods). Un fichier juste pour définir le namespace.
- Definir le secret

Quand on supprimer un namespace, les services et les pods correspondant se suppriment automatiquement

lancer les fichiers de conf :

    kubectl apply -f .

    kubectl get namespaces

    kubectl get secrets

    kubectl get services --namespace <mon namespace>

    kubectl get pods --namespace <mon namespace>

    kubectl describe pod <nom du pod> --namespace mysql-namespace

    kubectl delete namespace <nom du namespace>

## Ingress

