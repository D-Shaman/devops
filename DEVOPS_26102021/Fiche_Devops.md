# Azure réseau virtue

Un réseau virtuel Azure est concu de 4 points : 

- Un espace d'adressage : les IP pour les personnes qui font parti du même network. 
- Le sous-réseau permet de segmenter l'espace virtuel, pour ne pas encombrer l'espace global.
- Les régions : un réseau s'étend a une zone, ou une région.
- Les abonements : Un réseau virtuel est limité a un abonnement mis il est possible d'implémenter plusieurs réseaux virtuels au sein d'un même abonnement.

Les meilleures pratiques :
- Bien séparer les espace d'addressage
- Ne pas tout alouer, laisser des espaces blancs.
- Il vaux mieux avoir un petit nombre de grand réseau virtuel que l'inverse.

Communication avec internet:
On peut utiliser une adresse IP publiquer, ou via un loadbalancer.

Communication entre les ressources Azure :
- Via le réseau virtuel : On met toutes les ressources sous le même réseau virtuel
- Peering : permet de connecter des réseaux dans le réseaux virtuel azure.
- Point de terminaison de service de réseau virtuel : Ils permettent aux adresses IP privés du réseau d'atteindre les services.

Communiquer avec les ressources locales :
- VPN de point de site : communication sécurisé avec un réseau dont l'ordinateur ne fait pas parti
- VPN de site a site : c'est une connection par passerelle
- ExpressRoute : créer une connection entre Azure et un partenaire expressroute, c'est une connection privée que le passe pas par internet.

Il est possible de filtrer le trafic via les groupes de sécurité ou par appliance virtuelle réseau (qui est une machine qui sert de firewall).

Router le trafic : Il est possible de modifier les tables de routage (avec nos propres itinéraires).

## Le NoSQL

Volume : de plus en plus de data a disposition.