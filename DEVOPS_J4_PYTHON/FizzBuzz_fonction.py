#énumère de 1 à 35
#Fizzbuzz quand multiple 3 et 5
#Fizz quand multiple 3
#Buzz quand multiple 5

# def FizzBuzz():
#     for i in range(35):
#         if i % 5 == 0 and i % 3 == 0:
#             print("FizzBuzz! ", i)
#         elif i % 5 == 0:
#             print("Buzz! ", i)
#         elif i % 3 == 0 :
#             print("Fizz !", i)
#         else :
#             print(i)

# FizzBuzz()


# def test_multiple ():
#     if i % 5 == 0 and i % 3 == 0:
#         return('FizzBuzz', i)
#     elif i % 5 == 0 :
#         return("Buzz", i)
#     elif i % 3 == 0:
#         return("Fizz", i)
#     else:
#         return(i)


# def test_mult_15(i):
#     if i % 5 == 0 and i % 3 == 0:
#         print("FizzBuzz !", i)
#     else :
#         test_mult_5(i)

# def test_mult_5(i):
#     if i % 5 == 0:
#         print("Buzz !", i)
#     else:
#         test_mult_3(i)

# def test_mult_3(i):
#     if i % 3 == 0:
#         print("Fizz !", i)
#     else:
#         print(i)

# for i in range(36):
#     test_mult_15(i)

def test_mult_15(i):
    print("FizzBuzz", i) if i % 15 == 0 else test_mult_5(i)

def test_mult_5(i):
    print("Buzz !", i) if i % 5 == 0 else test_mult_3(i)

def test_mult_3(i):
    print("Fizz !", i) if i % 3 == 0 else print(i)

for i in range(int(input("Quel nombre ? \n"))+1):
    test_mult_15(i)