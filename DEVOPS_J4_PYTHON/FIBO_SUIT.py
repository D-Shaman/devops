# Faire une suite de fibonacci.


nombre_désiré = int(input("Combien de chiffres voulez vous ?\n"))

def fibo (var_fin, var_1, var_2):
    for i in range(nombre_désiré):
        var_fin = var_1 + var_2
        print(var_fin)
        var_1 = var_2
        var_2 = var_fin
    return(var_fin)

(fibo(var_fin = 0, var_1 = 0, var_2 = 1))


# En récursif
def fibo(n, un=1, un1=1):
    print(un)
    return fibo(n-1, un1, un1+un) if n else un
fibo(10)