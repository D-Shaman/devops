# Devops J4

## Le random

Random est une librairie inclue dans python qui n'est pas chargée a l'ouverture du programme (dans un soucis d'optimisation). Quand on fait un import, l'intégralité du fichier importé est lu et mouliné. On peut utiliser :

````python
from random import random
````

Il faut donc appeller le module via import.

````python
# Pour importer un entier random entre 0 et 1000
import random
ma_variable = random.randint(1, 1001)
````

* On va souvent utiliser import os et import path *

### A propos du break

On conseille d'init une variable is_over a False, et de l'utiliser dans la boucle while pour finir le programme.

````python
is_won = False
while not is_won :
    instructions
is_won = True
````

## Les fonctions

Une fonction est un bloc de code qui s'exécute quand on l'appelle. Le mot clé "def" permet de définir une fonction, lui donner un nom. Quand on va appeller le fonction qu'on a créer, TOUT le bloc de code a l'intérieur de la fonction est exécuté. La principale utilité de la fonction est l'optimisation du code et sa factorisation.

````python
def dire_coucou():
    print("coucou")
dire_coucou ()
````

On peut aussi passer des arguments entre les prenthèses`. Paramètres et arguments ne sont pas des synonimes. Un paramètre est ce qui est attendu par la fonction. L'argument est ce qui est envoyé a la fonction.

````python
def dire_coucou(prenom):
    print("coucou", prenom)
dire_coucou ("Diwan")
````

### Le type de retours d'une fonction

Une fonction fait un traitement de données, et elle peut retourner quelquechose. Je peut print ce return.

````python
def multiplier (a, b):
    return(a*b)
    
print(multiplier(5, 7))
#Mais je peut aussi stocker le retours dans une variable :
c = multiplier (5,7)

#Voire même travailler dessus
print(multiplier(multiplier(2,2), multiplier(5,5)))
````

## Les variables globales

Une variable déclarée dans une instruction indépendante est accessible depuis nimporte où. Ces variables sont accessibles de la manière suivante :

````python
print(globals())
````

Une variable qui est déclaré dans une fonction est dite locale. Une variable déclarée dans un module est accessible dans nimporte quel endroit de ce module.
Les blocs conditionnels, itératifs ou d'exception ne modifient pas l'esapce de nommage. On utilise "vars()" pour les locals et global() pour les globales.

````python
if True:
    variable1 = 345

if variable "variable1" in global().keys()
````

Cetains blocs introduisent un nouvels espace de nommage tel que les fonctions ou les classes.
TOUTES LES VARIABLES DECLAREES DANS UNE FONCTION SONT REINITIALISEES A CHAQUE LANCEMENT DE CETTE DERNIERE.

Pour modifier une variable depuis une fonction il faut retourner la valeur. Il est par contre possible de modifier une variable de l'espace local par une variable d'un autre local par le biais des retours de fonction.

## Argument par défaut

Si on ne passe pas d'argument, la fonction prend un valeur par défaut, par exemple :

````python
def say_hello(prenom="world"):
    print("Hello", prenom)

say_hello()
say_hello("Thomas")

# ou même

def beta(a=1, b=38, c=8):
    return(a + b + c)

print(beta(c=39))
````

## Correction des exos

````python
# Corriger la fonction:
def multiplicateur_mot(motBonjour, mult=5):
    return(motBonjour * mult)
 
mot_multiplie = multiplicateur_mot(motBonjour="Bonjour")
print(mot_multiplie)

# Corriger la fonction:
def multiplicateur_mot(motBonjour, mult=5):
    return(motBonjour * mult)
 
mot_multiplie = multiplicateur_mot(motBonjour="Bonjour")
print(mot_multiplie)
````

## Correction FizzBuzz (en def)

````python
def test_mult_15(i):
    print("FizzBuzz", i) if i % 15 == 0 else test_mult_5(i)

def test_mult_5(i):
    print("Buzz !", i) if i % 5 == 0 else test_mult_3(i)

def test_mult_3(i):
    print("Fizz !", i) if i % 3 == 0 else print(i)

for i in range(int(input("Quel nombre ? \n"))+1):
    test_mult_15(i)
````

## Correction suite de Fibonnaci

````python
nombre_désiré = int(input("Combien de chiffres voulez vous ?\n"))

def fibo (var_fin, var_1, var_2):
    for i in range(nombre_désiré):
        var_fin = var_1 + var_2
        print(var_fin)
        var_1 = var_2
        var_2 = var_fin
    return(var_fin)

(fibo(var_fin = 0, var_1 = 0, var_2 = 1))

# En récursive, parceque c'est quand même plus la classe...

def fibo(n, un=1, un1=1):
    print(un)
    return fibo(n-1, un1, un1+un) if n else un
fibo(10)
````

## Correction du reverse mots

````python
# Pas la plus jolie solution
def splitting() :
    ma_string = input("quelle est votre string ? \n")
    spliced_string = ma_string.split(" ")
    return('spliced_string')

def organizing(spliced_string) :
    for word in spliced_string:
        print(word[len(word)//2:] + word[:len(word)//2], end=" ")
    return('')

print(organizing(splitting()))
````

## Exo de comptage des occurences

````python
def listing():
    ma_phrase = input("Quelle est votre phrase ? \n")
    liste_phrase = list(ma_phrase)
    return(truc_qui_fait_le_machin(liste_phrase))

def truc_qui_fait_le_machin(liste_phrase) :
    for i in set(liste_phrase):
        print(liste_phrase.count(i),i)

listing()
````

## Correction exo Zip

````python
liste1 = list(string.ascii_lowercase)
liste2 = list(range(1, len(liste1)+1))
print(liste2)

final = zip(liste1, liste2)
print(dict(final))
````
