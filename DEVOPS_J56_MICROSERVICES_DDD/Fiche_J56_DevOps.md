# Les microservices 

## Domain Driven Design

### Le langage omniprésent (ubiquitous langage)

La communication est l'élément principal d'un bon développement.

Couche métier :

Une application de transaction banquaire : son métier est de faire des transaction. Ce n'est pas la technique (son métier n'est pas d'enregistrer des client en bdd par exemple). Par exemple, le métier de facebook n'est pas de se loger mais de permettre de lier les utilisateurs et de display des informations sur un mur. Le métier d'une application est sa raison d'être.

La partie technique est plutot la base de donnée, la gestion des serveurs, etc...

L'UL est donc un langage technique et son vocabulaire doit éviter les incompréhensions dues aux polysémies, synonymie, homonymies. **Le thésaurus** est un document qui liste l'emsemble des termes d'un domaine et les normalise.

- Donne les définitions des mots
- Indique les synonymes ou les antonymes
- Hiérarchise les mots

L'ensemble des parties, l'expert technique, l'utilisateur final et l'expert technique se réunissent sous forme de micro-formation pour expliuqer l'ensemble du domaine par des scenarii (qui peuvent ensuite eêtre schématisés).
