# Les conditions

Un code s'exécute de la même manière : de la première instruction a la dernière, puis il s'arrète. Ce fonctionnement ne permet pas de prise de décision. C'est pour ça que les boucles sont importante, elles permettent de faire un choix logique selon la situation.

## Le if

Le if permet d'exécuter un ensemble d'instruction si une condition est vrai.
La logique est toujours la même, il y a des instructions avant (par exemple des initialisation de variable), puis on va tester une condition bool. Par exemple :

````python
a = 5

if a < 10:

#Donc ici le booléen renvoie True
````

On utilise l'indentation dans les instructions qui sont dans la boucle if. Les ':' servent a ouvrir le bloc de code (comme des accolades dans d'autres langages). La prochaine ligne qui n'a pas d'indentation ne fait pas parti de la boucle. Le 'if est donc fini. Si le bool est True, il execute les instruction du bloc, sinon il skip jusqu'a la prochaine ligne non indentée.

````python
k = 12
b = 18

if k>b :
    print('lol')
print('pas lol')
````

* La bool k>b est False donc c'est le deuxième print qui va être exécuté. Le 'pas lol' *

Si on a absolument besoin d'un bloc if vide on fera :

````python
k = 12
b = 18

if k>b :
    pass
````

### Le else

Le if...else permet de créer un embranchement.
Dans ce cas, si le if est vrai, on exécute le bloc de code sous le if. Si le if est False, alors le bloc de code sous le else sera exécuté. C'est TOUS LES AUTRES CAS.

````python
a = 10

if a < 10:
    print('lol')
else :
    print('pas toto')
````

* Ici, pas toto sera print *
  
### Les if impbriqué

C'est déconseillé, mais il est possible d'imbriquer les if.

### Le else if : elif

Le elif permet de tester une seconde condition dans une boucle, de la manière suivante :

````python
a = input(" quel est ton nombre ?)

if a > 100:
    print('le nombre est sup a 100')
elif a > 10:
    print('Le nombre est sup a 10')
else:
    print('Le nombre est inférieur a 10')
````

## Les boucles

### La boucle for

Il existe deux types de boucles : le premier est le 'for'. Ici on connait le nombre d'itérations. SI ON CONNAIS LA FIN, ON UTILISE TOUJOURS UNE BOUCLE FOR.

Pour chaque valeurs de i contenues dans un range de 4

````python
i = 0
for i in range(4):
    print('i a pour valeur', i)
````

Ici, 'i' prendre successivement des valeurs de 0 a 3.

### Le while

La boucle while ("tant que") répète l'opération tant que la condition est vrai. On ne connais pas le nombre d'itération.

````python
x = 1
while x < 10:
    print (x)
    x += 1
pass
````

On précise le mot while suivi d'une expression bool, on termine la ligne pas ':', le corps de la boucle est ajouté avec une indentation.

### Break et continue

Break et continue sont deux mots clé utilisables dans les boucles. Break "casse" l'exécution de la boucle et passe à l'instruction suivante. Continue force l'itération suivante sans exécuter le reste du code inclu dans le corps de la boucle.

Exemple de break :

````python
for i in range (10) :
    print("iteration", i)
    print("salut")
    if i == 2:
        break
    print("fin itération", i)
print("après la boucle")
````

Exemple de continue :

````python
for i in range (10) :
    print("iteration", i)
    print("salut")
    if i % 2 == 0:
        continue
    print("fin iteration boucle", i)
print("après la boucle")
````

### Le do while

Au cas ou la condition est fausse avant de rentrer dans le while. Il faut une boucle qui va être exécuté au moins une fois. Puis après une première boucle on voit ce que on fait. Le do while c'est FAIT LE, puis vérifie la condition. Une solution est le "while True"

````python
d = 0;
while True :
    n = int(input("donnez un entier > 50 : \n"))
    print("vous avez fourni", n)
    d += 1
    if n > 50:
        break
    if d > 10:
        break
print("réponse correcte")
````

````python
reponse = 0
while reponse < 50 :
    reponse = int(input("donnez un entier > 50 :"))
    print("vous avez fourni", reponse)
print("reponse correcte")
````

### else dans le for

Si une boucle for se termine par épuisement sans toucher le break, le else est executé.

````python
for i in range(20):
    for j in range (2, i):
        if i % j == 0 :
            print(f"{i} n'est pas un nombre premier car {i}/{j} = {i/j}")
            break
    else:
        print(f"{i} est un nombre premier")
````

### Retours vers les conversion

Le datetime permet de changer une chaine en date via la fonction datetime.strptime()

### Enumerate

Enumerate renvoie un index et une valeur.

````python
for i, val in enumerate(range(50, 101)):
    print(f"{i}, {val}")
````

## Les conditions ternaire

Permet de faire des conditions en une seule ligne. Cela a été" ajouté pour simplifier l'écriture.  Il est possible d'imbriquer un autre if mais c'est peu souhaitable.

````python
a = int(input("donnez un nombre\n"))
print("toto") if a < 5 else print("tata")

#On peut aussi utiliser else pass
````

* S'organise sous la forme cond_vrai if cond else cond_fausse *

## La variable d'environnement

 Une variable d'envieonnement sont stockées par le système et sont en général utile pour les settings. On peut nottament voir l'emplacement du dossier utilisateur 'HOME". Voir dans les exos du matin.

## Correction des exos du matin

````python
# créer un ternaire depuis cette expression
# if(a == 10):
#     print("ok")
# else:
#     print(" pas ok")

a = int(input("chiffre\n"))
print("ok") if a == 10 else print("pas ok")

# # Corriger une boucle infinie
i = 0
while i < 10:
    print("réussi!")
    i += 1

# Corriger la boucle for:
mot = "Semifir"
for i, v in enumerate(mot):
    print(i,v)

#Afficher la table de 7 (solution bizarre mais qui marche)
liste1 = list(range(11))
for i in (liste1):
    liste1[i] = i * 7
print(list(enumerate(liste1)))

# Afficher les table de multiplication
for i in range(0, 11):
    for j in range(0, 11):
        print(f'{i} multiplié par {j} font {i*j}')
        

# Afficher tous les éléments d'une liste
liste = ["Chat", "Chien", "Poulet"]
for i in liste:
    print(i)

# Récupérer tous les éléments pairs d'une liste
nombres = range(50)
for i in nombres:
    if i%2 == 0:
        print(i)

# Afficher la variable d'environnement HOME
from pathlib import Path
print((Path.home()))

import os
print(os.environ.get('USERPROFILE'))
````

## Correction triangle et pyramide

### Triangle

````python
base = int(input("Quelle est la taille de vôtre triangle\n"))
i = 0
if base <= 0:
    print("Le nombre doit être positif")
else:
    while i < base +1 :
        print('*' * i)
        i+= 1
print("Fin de la pyramide")

# Meilleure méthode 

user_number = int(input("Base ?"))
for i in range(user_number+1):
    print("*" * i)

````

### Pyramide

````python
base = int(input("Quelle est la taille de vôtre pyramide\n"))
i = 1
j = base+2
if base <= 0:
    print("Le nombre doit être positif")
else:
    while i < base :
        print(" " * j,'*' * 2*i)
        i+= 1
        j -= 1
print("Fin de la pyramide")

#Meilleure solution 

taille_pyramide = int(input('coucouille ?\n'))
for i in range(taille_pyramide +1):
    print(' ' * taille_pyramide,'* ' *i)
    taille_pyramide -= 1
````

## Correction FizzBuzz!

````python
for i in range(1, 36):
    if i % 3 == 0 and i % 5 == 0:
        print("FizzBuzz!", i)
    elif i % 3 == 0:
        print("Fizz!", i)
    elif  i % 5 == 0 :
        print("Buzz !", i)
    else:
        print(i)
````

## Correction juste prix

````python
import random
nombre_juste = random.randint(0, 1000)
#print(nombre_juste)
essais = 0
is_over = False

while is_over == False:
    nbr_concurent = int(input("Quelle est vôtre proposition ?"))
    if essais == 10:
        is_over = True
        print("T'as perdu !")
    elif nbr_concurent < nombre_juste :
        print("C'est plus !")
    elif nbr_concurent > nombre_juste :
        print("C'est moins !")
    else :
        print("C'EST GAGNE OUAIS OUAIS OUAIS OUAIS OUAIS...")
        is_over = True
    essais += 1
````
