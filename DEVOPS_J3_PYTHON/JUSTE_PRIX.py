# Générer nombre aléatoire et 1000
# L'utilisateur propose un nombre
# Si l'utilisateur est trop petit alors "C'est plus !"
# Si l'utilisateur est trop grand alors "C'est moins !"
# Si l'utilisateur a juste alors "C'est gagné !"
# 10 essais max -> C'est perdu.

# import random
# nombre_juste = random.randint(0, 1000)
# print(nombre_juste)

# essais = 0
# while essais <10:
#     nbr_concurent = int(input("Quelle est vôtre proposition ?"))
#     if nbr_concurent < nombre_juste :
#         print("C'est plus !")
#         essais += 1
#     elif nbr_concurent > nombre_juste :
#         print("C'est moins !")
#         essais += 1
#     else :
#         print("C'EST GAGNE OUAIS OUAIS OUAIS OUAIS OUAIS... Vous remportez un séjour a Bagdad avec Jean Castex !")
#         break

import random
nombre_juste = random.randint(0, 1000)
#print(nombre_juste)
essais = 0
is_over = False
while is_over == False:
    nbr_concurent = int(input("Quelle est vôtre proposition ?"))
    if essais == 10:
        is_over = True
        print("T'as perdu !")
    elif nbr_concurent < nombre_juste :
        print("C'est plus !")
    elif nbr_concurent > nombre_juste :
        print("C'est moins !")
    else :
        print("C'EST GAGNE OUAIS OUAIS OUAIS OUAIS OUAIS... Vous remportez un séjour a Bagdad avec Jean Castex !")
        is_over = True
    essais += 1