# créer un ternaire depuis cette expression
# if(a == 10):
#     print("ok")
# else:
#     print(" pas ok")

a = int(input("chiffre\n"))
print("ok") if a == 10 else print("pas ok")

# # Corriger une boucle infinie
i = 0
while i < 10:
    print("réussi!")
    i += 1

# Corriger la boucle for:
mot = "Semifir"
for i in range(len(mot)):
    print(i, mot[i])

for i, v in enumerate(mot):
    print(i,v)


# Afficher la table de multiplication
for i in range(0, 11):
    for j in range(0, 10):
        print(i * j)
        

# Afficher tous les éléments d'une liste
# liste = ["Chat", "Chien", "Poulet"]
# i = 0
# while i < len(liste):
#     print(liste[i])
#     i += 1
liste = ["Chat", "Chien", "Poulet"]
for i in liste:
    print(i)

# Récupérer tous les éléments pairs d'une liste
nombres = range(50)
for i in nombres:
    if i%2 == 0:
        print(i)

# Afficher la variable d'environnement HOME
from pathlib import Path
print((Path.home()))

import os
print(os.environ.get('USERPROFILE'))

# liste1 = list(range(11))
# for i in (liste1):
#     liste1[i] = i * 7
# print(list(enumerate(liste1)))
