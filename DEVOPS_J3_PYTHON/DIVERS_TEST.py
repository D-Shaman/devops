# var_a = int(input('Entrez un nombre\n'))

# #print(type(var_a))

# if var_a > 100:
#     print('le nombre est sup a 100')
# elif var_a > 10:
#     print('Le nombre est sup a 10')
# else:
#     print('Le nombre est inférieur a 10')

# i = 0
# for i in range(4):
#     print('i a pour valeur', i)

# x = 1
# while x < 10:
#     print (x)
#     x += 2
# pass 

# for i in range (10) :
#     print("iteration", i)
#     print("salut")
#     if i == 2:
#         break
#     print("fin itération", i)
# print("après la boucle")

# for i in range (10) :
#     print("iteration", i)
#     print("salut")
#     if i % 2 == 0:
#         continue
#     print("fin iteration boucle", i)
# print("après la boucle")

# while True :
#     n = int(input("donnez un entier > 50 : \n"))
#     print("vous avez fourni", n)
#     if n > 50:
#         break
# print("réponse correcte")

# reponse = 0
# while reponse < 50 :
#     reponse = int(input("donnez un entier > 50 :"))
#     print("vous avez fourni", reponse)
# print("reponse correcte")

# for i in range(20):
#     for j in range (2, i):
#         if i % j == 0 :
#             print(f"{i} n'est pas un nombre premier car {i}/{j} = {i/j}")
#             break
#     else:
#         print(f"{i} est un nombre premier")


# mot = "Semifir"
# for i, v in enumerate(mot):
#     print(i,v)



for i in range(0, 11):
    for j in range(0, 11):
        print(f'{i} multiplié par {j} font {i*j}')

        
liste1 = list(range(101))
for i in liste1:
    print(f"{i}, {liste1[i]}")

a = (2, 5, 6, 7)
b, c, d, = a
print(f"{b}, {c}, {d}")

for i, val in enumerate(range(50, 101)):
    print(f"{i}, {val}")


liste1 = list(range(11))
for i in (liste1):
    liste1[i] = i * 7
print(list(enumerate(liste1)))