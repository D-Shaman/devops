# Afficher les nombre de 1 à 35 (un par ligne) en remplaçant les multiples de 3 pas "Fizz!" et les multiples de 5 pas "Buzz!"
#Les multiples de 3 et 5 seront remplacés par "FizzBuzz!"*

for i in range(1, 36):
    if i % 3 == 0 and i % 5 == 0:
        print("FizzBuzz!", i)
    elif i % 3 == 0:
        print("Fizz!", i)
    elif  i % 5 == 0 :
        print("Buzz !", i)
    else:
        print(i)


# #BUG
# for i in range(1, 36):
#     if i % 3 == 0 and i % 5 == 0:
#         print("FizzBuzz!")
#     elif i % 3 == 0:
#         print("Fizz!")
#     print("Buzz!") if i % 5 == 0 else print(i)