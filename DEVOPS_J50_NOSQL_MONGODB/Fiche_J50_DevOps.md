# NoSQL -> MongoDB

## Rappel, le JSON

Le Json est une notation ou un objets se trouve entre acollades. C'est basé sur un système de clé-valeur.

## Les bases de données NoSQL

NoSQL : signifie NotOnlySQL.

Le NoSQL respecte les 3V :

- Volume
- Vitesse
- Variété (différent format de données)

Ses atouts sont :

- L'évolutivité
- La disponibilité
- La tolérance aux fautes

La base de donnée NoSQL est basée sur un système clé-valeur. La valeur peut-être une chaine de caractères, un blob, etc...
La donnée est opaque au système, il n'est pas possible d'y accéder sans passer par la clé. Il n'y a pas de typage, l'intelligence est placé dans l'applicatif avant la BDD. Cela permet un accès rapide aux informations, par exemple conserver la session d'un site web.

![clevaleur.PNG](clevaleur.PNG)

### Le type document

Il permet d'étendre le paradigme clé/valeur avec des documents. Les documents peuvent être vu comme des fichiers contenant des objets JSON ou XML. Chaque document est un objet, qui contient un ou plusieurs champs, et chaque champ contient une valeur typée (string, date, binaire, array). Cela permet de stocker des données semi-structurées. Ainsi, on peut récupérer via une seule clé, un ensemble d'informations structurées hiérarchiquement (cela diminue les jointures).

Exemple : MongoDB

En NoSQL on ne parle pas de tables mais de collection.
On en a plusieurs types :

- Documents
- Colonnes : évolution des BDD clé/valeur (ressemble au SQL)

### Commandes Mongo

    lancer un conteneur mongo :
    docker run -d -p 27017:27017 --name mongo mongo

    Rentrer dans le conteneur
    docker exec -it mongo bash

    show dbs :
    Afficher les bases

    let demo = connect("demo") / On crée la variable de connection
    
    db.users.i

    demo.user.find()
    db.users.findOne()
    db.users.find({"borough":"Bronx"}).count()
    db.users.find({ "borough": "Bronx","cuisine":"American"}).count()

    db.users.find({"restaurant_id":{$gt:"40368000"}}).count()

    db.users.find({"address.street":"Broadway","cuisine":"French"}).count()

    db.users.find({ "address.street": "Broadway", "cuisine": "French" }, { "name": 1, "address":1,"cuisine":1})

    db.users.find({ "address.street": "Broadway", "cuisine": "French" }, {"_id":0,"name": 1, "address":1,"cuisine":1})

    db.users.insert({"nom":"Kul", "prenom":"Jean", "adresse":{"rue":"Lallement", "Ville":"Lille"}, "actif":false})

### L'agrégation

1) 1576 : demo.volcano.find().count()
2) Location: { type: 'Point', coordinates: [ 139.18, 36.53 ] / demo.volcano.find({"Volcano Name":"Akagi"},{"Location":1})
3) 842 / demo.volcano.find({"Elevation":{$gte:1303}}).count()
4) demo.volcano.find({$and:[{"Status":"Radiocarbon"},{"Elevation":{$gt:1000}}]}).count() / 92
5) 'Adams' / demo.volcano.findOne({"Location.coordinates":[-121.49, 46.206]})
6) { Region: 'Arabia-S' } / demo.volcano.findOne({"Volcano Name":"Arhab, Harra of"}, {"Region":1, _id:0})