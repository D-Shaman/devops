INSERT INTO cabinet.adresses(num, rue, ville, cp) VALUES (25, "Rue Lqzdqdent", "Lille", 59000);
INSERT INTO cabinet.infirmieres(adeli, nom, prenom, portable, fixe, adresses_idadresses) VALUES 
(99, "Bouchard", "Narine", 0612547485, 0325654575, (SELECT idadresses FROM adresses WHERE rue = "Rue Lallement"));
INSERT INTO cabinet.deplacements(date_dep, cout) VALUES ("2021-07-08", 29.99);

SELECT * FROM cabinet.patients;
INSERT INTO cabinet.patients (nom, prenom, birth_date, sexe, num_cq, infirmieres_idinfirmieres) VALUES
("Lefebvre", "Diwan", "1994-10-18", "H", 99999, (SELECT idinfirmieres FROM infirmieres WHERE nom = "Bouchard"));

SELECT * FROM cabinet.infirmieres NATURAL JOIN cabinet.adresses INNER JOIN cabinet.patients;
SELECT * FROM cabinet.patients INNER JOIN cabinet.infirmieres INNER JOIN cabinet.adresses;
SELECT * FROM cabinet.infirmieres;
SELECT * FROM cabinet.adresses;
SELECT * FROM cabinet.deplacements;

UPDATE cabinet.infirmieres SET nom = "Cul" WHERE idinfirmieres = 1;
DELETE FROM cabinet.adresses WHERE idadresses = 2;