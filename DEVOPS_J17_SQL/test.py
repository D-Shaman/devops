class Addition():
    def __init__(self, a:int, b:int):
        self.set_a(a)
        self.set_b(b)
        self.calc()
    
    def set_a(self, a):
        self._a = a
    def get_a(self):
        return self._a
    
    def set_b(self, b):
        self._b = b
    def get_b(self):
        return self._b
    
    def calc(self):
        result = int(self._a) + int(self._b)
        return result

result1 = Addition(10, 37)
print(result1.calc())