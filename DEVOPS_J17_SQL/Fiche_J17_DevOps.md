# MYSQL

C'est la partie base de données. C'est un langage qui permet d'échanger des données stockées.

Un utilisateur va se connecter via le login et le mdp. On se connecte a un serveur. La communication se fait vers le serveur en HTTPS et depuis le serveur en Json.

Le serveur est en 3 parties : le controlleur, le model et le repo.
Le controlleur envoir les donnée au modèle, qui va les envoyer au repo, qui va communiquer avec le SGBD qui communique avec le serveur. La communication se fait en TCP.Le SGBD va communiquer avec la base de données.

En suite, la cascade se fait dans l'autre sens pour que l'utilisateur récupère l'info. La partie serveur se fait en Python et le SGBD se fait en MySQL (et la base de donnée en SQL). Le coté utilisateur se fait en HTML / CSS/ JS et des framework Vue.js, react, angular

![schéma](schéma.png)

## La LGBD (système de gestion de base de données)

Il en existe plusieurs : MySQL (le plus utilisé), MARIADB, oracle, SQL serveur, MongoDB...
Pour MySQL il faut un outil qui s'appelle Workbench qui permet d'avoir une interface graphique, ainsi qu'un serveur MySQL.

Le TCP est le protocole de communication entre l'ordi et le serveur via le port (une route particulière / 3306). Password mySQL : root

les connections sont des sitmulation pour voir comment l'utilisateur va se connecter au LGBD. Certains utilisateurs vont pouvoir faire récupéreation / ajout, etc... D'autres juste de la récupération. localhost (127.0.0.1).

## Comment ça fonctionne

Le MySQL a une partie applicative en une partie serveur MySQL, il va communiquer avec un serveur SQL. Le serveur SQL renvoie les données au serveur MySQL puis les renvoyer a l'applicatif.

![serveur](serveur.png)

## Présentation de l'outil

On commence dans la partie administration de workbench. Dans schéma, on a les bases de donnée (la système contient toutes les informations liées a MySQL, pas toucher). Le fichier SQL permet d'ouvrir une query, une requète. A côté on ouvre un fichier sur notre machine. La troisième créer une base de données en local. A coté on crée des tables qui permettent d'ordonner des données.

A partir de l'onglet database on peut se connecter a une base de donnée, mais aussi d'afficher visuellement une base de donnée. Dans serveur on a data export et data import.

## Le SQL

### Le relationnel (SQL)

Dans le SQL on parle de relations entre les données (tableau a 2 dim) avec des lignes et des collones. C'est du SQL. La donnée doit correspondre au tableau. Le SQL est représenté par des tables avec des collones avec un nom spécifique et des lignes avec des noms spécifique.

Il y a une ID différente pour chaque donnée, même si la donnée est identique. Donc par exemple la première ligne peut être composée d'un ID, un nom, un prénom... Toute la collone correspond au titre de la collone (tout la collone ID sera des ID).

Quand on fait une insertion on doit remplir CHACUNE des collones. C'est l'aspect relationnel. Si on rentre un utilisateur il a une ID unique (en général un int), un nom a renseigner, un prénom, âge... etc... Toutes ces données sont liées a un seul utilisateur. Il y a une relation entre chacune des données liées par l'utilisateur.

Si on rentre un second utilisateur (donc ID différente), même si toutes les infos sont les même, ILS SONT DIFFERENT car l'ID (la primary key, clé primaire) est différente. IL NE PEUT PAS Y AVOIR D'ID IDENTIQUE.

L'intéret : le SQL n'est bon nul part, mais il est moyen partout.

### Le NoSQL

Quelque soit l'utilisateur, je peut rentrer les informations que je veux, même si elles ne sont pas en relation (ce sont des objets). La notion d'ID n'existe pas pour ces utilisateurs. Ici les collones n'existent pas et on a pas besoin de rentrer toutes les infos.

C'est un ensemble de données sans doubles, avec uniquement les données qui nous intéressent. Il n'y a aucune relation entre les données.

L'intérêt : spécifique, parfois moins performant mais souvent plus performant, mais qui se développe de plus en plus.

### Exemples et cas particuliers

Si on fait un tableau SQL avec ID/Nom/Age/Bool .On fait une première insertion 1/DUPONT/18/True.

Encore une fois il ne peut pas y avoir de collones vides. TOUTES LES INFOS DOIVENT ETRE LA. On ne peut pas avoir de non relation de données.

Les types :
On peut définir une collones comme "non obligatoire" (mais ça ne veut pas drire qu'elle n'existe pas). C'est NULL qui va remplacer la donnée. NULL est un type (âge est de type NULL sur 2/DURANT/NULL/False). Il y a quand même une relation entre âge et le reste des données. 

Quand on fait un select* on récupère quand même la collone âge pour l'ID2, si il n'y avait rien on ne récupèrerait pas la collone. C'est l'ELEMENT VIDE qui n'est pas possible (on peut par exemple remplir avec des espaces).

Dans les tables MyISAM ne permet pas les relations entre tables. Par exemple entre une table utilisateur et une table adresse. InoDB les permet mais ils est plus complexe et donc plus lent au niveau des requettes.

### La primary key

La collone ID représente les primary key, elles sont unique par individu. En général elles sont auto-incrémentées. Elle permet la communication entre les tables (clé primaire / clé étrangères).

Quand on rentre les données dans la table, on est pas censé rentrer les ID, elles se remplissent toutes seules.

Pour le type de variable on utilise en général les int, doubles, float, smallint, tinyint, char...

- Int : jusqu'a 15 chiffres. Utile pour les bases de donnée classique. En général on utilise bigint pour stocker les ID sur les très grosse DB
- Décimal : précision la moins importante.
- Double : moins bonne précision que float.
- Float : précision la plus importante.
- Char : 127 caractères
- Varchar : pour nom et prénom (très courant)
- Text : plusieurs 100ène de milliers de caractères
- Date : date complète YYMMJJ
- Datetime : date + time HHMMSS
- Blob : référence aux objets en base de donnée
- Enum : fait référence a ce qu'il y a a l'intérieur (Homme/Femme/Autre)

NN : non nul -> La case doit être forcement remplie. Si elle n'est pas cochée
UQ : Index unique -> Pour les éléments qu'on ne veut qu'une fois dans la base de donnée (exemple : adresse mail, numéro de CQ...)
B : binary -> Utilisé pour le stockage d'image au format binaire.
UN : unasigned datatype -> force le nombre positif (exemple pour l'âge)
ZF : Utilisé en sciences -> 25 devient 00025
AI : auto-incrémentation de la collone : elle s'incrémente toute seule et ne revient jamais en arrière.
G : collones auto générées -> permet un traitement entre deux collones (comme une concaténation)
Expression par défaut : pour remplacer les NULL quand rien n'est précisé

## Relations entre les tables : clé primaire clé étrangère 

On passe entre la clé primaire et la clé étrangère. La clé étrangère est une relation avec la clé primaire d'une autre table. 

3 types de relation :

- 1 to many : un user ne peut avoir qu'une seule adresse mais une adresse peut correspondre a plusieurs personnes
- 1 to 1 (1-1): Un utilisateur ne peut avoir qu'une adresse, et une adresse ne peut avoir qu'un utilisateur.
- many to many (n-m) : un user peut avoir plusieurs adresses et une adresse peut avoir pluseurs users. Il faut donc une table intermédiaire. Il y a deux clé étrangères a l'intérieur.

![mtm](mtm.png)

Le petit losange rouge correspond a une clé étrangère, la clé jaune pour une clé primaire, le losange bleu pour un champs essentiel.

exo :

- repérer les diff table : matériel, catégorie, client, adresse, fiche de location
- reprérer les liaisons : matériel catégorie (one to many), client fiche de location (one to many), matériel fiche de location (many to many), client adresse (one to many)
- extraire les collones : code, libélé de catégorie, matériel indus, outillage indus, matériel bureau, référence interne, désignation, date d'achat, prix par jour, numero, rue, ville, cp, type, tel, durée de loc, numero loc, date loc.
