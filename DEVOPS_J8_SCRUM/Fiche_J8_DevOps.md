# Fiche DEVOPS Jours 8 : SCRUM, AGILE

Objectif : préparation pour la certificcation SCRUM Master. La certif est composée de 80 questions en 1 heure (sous forme de QCM, dans l'esprit du code de la route). Cet exam / certif est en anglais. En Français la certification PSC existe, mais elle est moins reconnue.

## Méthodologie classique : le waterfall

Les phases du projet se passent dans un ordre précis sans revenir en arrière. On fait d'abord une analyse du projet, puis on écoute les spécifications, on produit un design. Ensuite on passe a la partie développement, puis le test et l'intégration, et enfin le déploiement.

L'agilité nécessite un feedback constant du client. La méthode agile ne peut fonctionner sans.

![cycle en V](cycle_v.png)

Le cycle en V est encore très ultilisé malgrés ses défauts. Par exemple son manque de souplesse, le cloisonnement des équipes. Si le projet est long, les techno peuvent devenir obsolettes. Enfin on travaille en silo, ce qui est mal adapté au monde du travail actuel.

La méthode classique, l'avantage est que le client exprime ce qu'il veut, et le livrable doit arriver a temps et en bon état. Cette méthode est souvent conseillé en industrie et sur les projets scientifiques.

Le problème principal est l'effet Tunnel. C'est a dire que le client exprime son besoin 1 an avant de voir le résultat fini. Ce résultat peut être assez éloigné du premier désir exprimé.

clase |méthodes agiles | méthodes classiques|
------|----------------|--------------------|
Objectif | satisfaire l'utilisateur |respect du besoin final|
changement| accepter le changement | difficultés a accepter le changement|
livraison| livraison fréquente | livré en une seule fois|
moteur | stimuler la motivation | stimuler la productivité|
équipe | travailler en synergie | travailler de façon segmentée|
communication | en directe | rare|

## Dans la philosophie agile

On favorise :

- Les individus et leurs interactions.
- ...

On se concentre sur la valeur et la motivation de l'équipe. Avec le moins de cruch possible. On limite les travail a ce qui est nécessaire. La hiérarchie est supposée être a plat (pas en hiérarchie). Apporter au client de la valeur.

Il y a une multitude de valeurs dans le monde Agile (comme un grand sac) avec de multiples méthodes :

- Des anciènes comme le RAD (Rapid Application Development)
- le DSDM (Dynamic Systems Development Method)
- mais aussi le Crystal Clear (qui est basé sur la transparence entre l'expression du besoin et les fréquentes livraison)
- l'XP (extreme programming) qui est une méthode itérative.
- Le LEAN : pour réduire le gaspillage et les couts
- Kanban : Très utilisé en SCRUM
  - Livraison rapide
  - Amélioration continue
  - Mettre en avant le travail d'équipe
  - Boucles d'amélioration continue
  - Avoir une vue globale du système

### Les rôles en méthode classique et le Framework SCRUM

Les rôles :

- Chef de projet
- Directeur de pôle
- Architecte
- Tech Lead
- Dev
- ...

En SCRUM :

- Un product Owner ("Chef de Projet")
- Un SCRUM Master
- La DEV team.

## Qu'est-ce que SCRUM

Youtube : [SCRUM life](https://www.youtube.com/channel/UCMCnZGIOeLVO65-LBxkkHyQ), [Le SCRUM Guide](https://scrumguides.org/download.html)

SCRUM est un **Framework**, pas une *méthodologie par étapes*.

"Un cadre de travail au sein duquel les acteurs peuvent aborder des problèmes complexes et adaptatifs, en livrant de manière efficace et créative des produits de la plus grande valeur possible". Les mots clé sont "sprint" "acteurs" et "évènements".

Le projet est divisé en sprint : On commence par une planification de sprint (définir l'objectif qu'on se donne). La fin du sprint fini par 2 réunion pour faire le point (focus produit) et une rétrospective pour échanger sur les manières de fonctionner (focus équipe). Tous les jours durant le sprint les équipes se réunissent (mêlées quotidiennes).

Le Feedback est l'itératif est au coeur de SCRUM. La revue est le point d'entrée de la planification suivante : elle permet de déterminer ce qui va être construit ensuite. Et de changer l'orientation du projet par feedback du client. La mêlée sert aussi de feedback quotidien pour voir comment le projet avance. Au final chaque réunion a une fonction de feedback qui va servir a l'ensemble du système. Tout est concret et tout est retours et itération.

Dans la construction du produit, il y a l'aspect produit (marketing ; Product Owner), l'aspect technique (expertise ; equipe de developpement), puis l'aspect organisationnel (pour l'amélioration continue, SCRUM master). Quand ces 3 éléments sont associés, le produit fonctionne. Le croisement de ces mondes est le SCRUM.

La méthode SCRUM est incrémentale et itérative :
Cela signifie que l'on définie le projet et qu'on va un peu plus en détail a chaque fois. La partie incrémentale fait qu'on ajoute un petit morceau du projet a chaque fois.

Dans la méthodologie Agile et le FrameWork SCRUM, le concepte de priorité est extrèmement important. Il faut savoir évaluer la priorité de chaque sprint, surtout quand il reste du temps disponible.

### SCRUM : le pilotage par la valeur

C'est le client qui défini son besoin, et qui le priorise. Avant chaque itération, le client et l'équiê définissent ensemble ce qui sera développé durant l'itération selon les priorités fixées.

Les besoins et les priorités peuvent évoluer entre et pendant chaque itération. La valeur acquise du logiciel est d'autant plus importante sur les premières itérations. Au final, la valeur ajoutée doit être délivrée rapidement et régulièrement, pour qu'un bon produit soit crée.

### L'empirisme

Le SCRUM se base sur l'empirisme :

- **Transparence** : on partage sans faille l'info, les ressources, le planning, le cout, l'avancement, les risuqes par rapport au projet, etc...
- **L'inpection** : inspection régulière de l'état du projet, l'objectif est de détecter rapidement les écarts. Les inspections ne doivent pas impacter la bonne marche du projet et le rythme imposé par SCRUM
- **L'adaptation** : elle survient pour limiter les risques liés aux écarts. Si l'écart sort des limites, des ajustements doivent êtres décidés. Le but est de réduire considérablement le risque. C'est assuré par les évènements SCRUM (mêlées, planification, revue, rétrospective de sprint).

### Les 5 **valeurs** de SCRUM et la maintrise du temps

Focus, Ouverture, Respect, Courage, Engagement

Toute les étapes du projet et les évènements sont timeboxés, ils ont un temps limité. Typiquement :

- Une itération dure entre 2 et 4 semaines
- La planification d'une itération dure 8 heures pour une itération de 4 semaine.
- La mêlée dure 15 minutes
- La démo dure 4 heures
- La rétrospective dure 3h pour une itération de 4 semaines

## Le SCRUM en résumé

C'est :

- Un **objectif général** : construire de manière itérative et incrémentale des morceaux applicatifs à forte valeur ajoutée.
- Un **moyen** : l'empirisme
- Un processus type et ses 3 piliers
- Cinq valeurs fondamentales
- Des artefacts

SCRUM n'est pas :

- Une *solution miracle* (nécessite une forte discipline)
- Simple a optimiser (Une véritable stratégie est nécessaire pour créer un processus de production adapté aux équipes, au produit et au contexte)
- Une méthode autonome (elle doit se faire a partir de l'expérience de l'équipe par intégration de process et d'outils propre a l'organisation)

## Les différents rôles

Il y a :

- Le product Owner
- La Dev Team
- Le SCRUM Master

### Le SCRUM Master

Il n'est pas un chef de projet... Car il n'y a pas de hiérarchie traditionnelle en SCRUM. Il manage des process et non pas des gens. Il est aussi utile pour réduire les conflits entre les équipes.

#### Rôles : méthode et organisation

- Une responsabilité méthodologie : la bonne application de SCRUM et de l'agilité
- Il est **organisateur** : il accompagne l'équipe sur tous les évènements SCUM et en contrôle la bonne tenue.
- Il est **facilitateur** : il fluidifie la communication entre tous les acteurs.
- Il est parfois **médiateur** : en charge de la gestion des conflits et faciliter la collaboration.

![Scrum](scrum.png)

Il se base sur une connaissance SCRUM et Agile. Il a une capacité à transmettre et a évaluer les acquis. Il dispose d'une bonne intelligence émotionnelle.

### L'équipe de DEV

Répartis les tâches; Evalue les risques, La qualité du developpement, les solutions techniques.

Le product owner est responsable du cahier des charges, de la définition des priorités, de l'évaluation des risques, de la communication avec la hiérarchie, de la collaboration avec le client, de la gestion des délais...

## Les outils du SCRUM Master

Un processus pour un objectif :

- Des mesures (KPI)
  - Burndown charts ![Burndown](burndown.png)
  - Le BurnUp chart (Même méthode mais a l'envers).
  - Météo de l'humeur de l'équipe: pour voir sur la durée si un membre de l'équipe a une humeur négative et en connaître la raison
  - Le moving motivator : quel est le moteur de la motivation des gens de l'équipe.
  - La matrice de compétences : permet de savoir ce que les gens savent faire et ce que les gens aiment faire.
- Des rôles
  
## Le Product Owner

Son focus est la valeur du produit. Il est plus proche du chef de projet que le SCRUM Master. C'est un genre de manager pour le backlog du produit. La ou le SCRUM Master porte une vision méthodologique sur le projet, le product owner s'occupe de la vision "produit".

Il a 5 rôles principaux :

- Il est **responsable** du produit. Initialisation, maintenance et ordonancement du product backlog (l'ensemble des tâches et des demandes a réaliser)
- Validateur : seul responsable du backlog il valide (ou non) les propositions de l'équipe et celles du client. Il priorise les tâches du Sprint.
- Il traduit le besoin : le backlog doit être compréhensible par tous (transparence).
- Maximiser la valeur du produit : un bon PO donne a l'équipe de pouvoir de contribuer au Backlog tout en gardant a l'esprit la valeur ajoutée de chaque sprint.
- Il pilote et encourage le feedback utilisateur. Repriorise ensuite le feedback.

Il est le point focal au centre de l'équipe. Il n'y a qu'un seul product owner. Ce rôle peut être assuré par le SCRUM master ou par une personne de l'équipe de dev. Mais c'est déconseillé par le SCRUM guide.

L'objectif est d'avoir **un seul** point d'entrée "humain" entre le produit et l'ensemble des équipes. Contrairement a un ched de projet tout puissant, il échange autant avec le client et avec la dev team.

### Le Product Backlog (ou carnet de produit dans la langue de Laurent Ruquier)

C'est la liste de tout les éléments nécessaire a la réalisation du projet:

- On commence par des épiques
- Découpées en user stories (je veux pouvoir faire ça)
- Puis elles sont priorisées par le product owner

#### Le product backlog

Il y a des priorités hautes, d'autres moyennes. Il est noté l'épique a laquelle la story est associé. A la fin se trouve le bac a sable, qui regroupe ce qui est dépriorisé ou qui n'est pas encore a implémenter.

- Il est **unique**
- Il est la bible du projet
- Il est partagé a tous le monde
- Il est parlé sans technique
- Chaque item est atomique
- Il vis avec le projet

L'item, ou la user story est l'élément unitaire du backlog. Il est défini par :

- Une description
- Des critères d'acceptation
- Une estimation du temps nécessaire pour avoir la feature
- Un ordonnancement

"En tant que <rôle>, je peux <action> afin de <résultat>

![user-story](user-story.png)

C'est le story maping :

- Il faut identifier l'utilisateur
- Identifier les grandes fonctions dont ils ont besoin (les épiques)
- Identifier les users story pour chaque fonction
- Puis trier les user stories pour identifier les releases (livraisons), donc le produit minimum viable (MVP).
- Faire estimer les users story par les équipes

![story-maping](story-maping.png)

