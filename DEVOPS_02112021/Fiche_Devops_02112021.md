# Terraform

## Rappel DevOps
Notes :

- NTFS et FAT32 sont simplement des logiciels qui vont déterminer comment les données vont être organisées sur l'espace de stockage.
- Les antivirus ne sureveillent pas ce que font les applications, mais surveillent ce que fait windows sur ordre des applications.
- On premise : en local.
- Terraform fonctionne sur tous les provider cloud
- Il est possible de faire du contrôle de cluster Kubernetes par Rancher, même si les stacks Kubernetes sont a distance.


On a le matériel, sur lequel on dépose un OS (en général Linux ou Windows). Par dessus, on installe nos logiciels : ici, Flask, une BDD... Toutes les applications installées communiquent avec l'OS, c'est l'OS qui fait le lien entre les logiciels et le matériel. Les firewall communiquent avec l'OS et demande a l'OS de bloquer l'accès a certain ports de la carte réseau (par exemple IPTable). Ce n'est pas le firewall qui bloquer quoique ce soit.

Deux protocoles : TCP,UDP. En UDP on ne demande pas de conformation sur les données reçues, il peut donc y avoir de la perte (exemple : météo, monitoring cpu/ram, stream vidéo...). En TCP, on demande confirmation a chaque paquet, donc il n'y a pas de perte, le header du TCP est en binaire sur 32 bits qui contient:

	- le port source
	- le port destination
	- la place du pacquet dans le fichier
	- etc...

Le protocole HTTP (hypertext transfer protocol) se place en surcouche du protocole TCP, c'est un vocabulaire. 

L'application va transiter par la carte réseau vers un ordinateur distant, en envoyant des blocs TCP. Chaque élément du fichier fractionné va être numéroté, et va être reconstruit pas l'ordinateur distant.

IaaS (Infrastructure as a Service) : Quand on utilise une solution Cloud, on peut le faire pour leur dévoluer la gestion de l'infrastructure du matériel. L'Iaas comprend aussi l'Hyperviseur qui permet de faire tourner une machine virtuelle. L'utilisateur choisi l'OS et la puissance. C'est a ce niveau la que se place Terraform (Il est utilisé pour le cloud en général). On peut indiquer a Terraform de prépare 2 VM d'une puissance X sur AWS, selont les spécification techniques suivantes...

Terraform n'est pas bon pour les installation, il prépare juste les machines. C'est pour cela que Terraform fonctionne en tandem avec Ansible. Ansuble ne sauvegarde pas l'état actueln et relance tout depuis le début. Terraform se souvient, et sauvegarde l'état. Donc si on demande 3 machines a Terraform et qu'on a une machine présente, il va en lancer 2. Terraform c'est de l'infrasctrcture as code.

PaaS (Plateform as a Service): Ici par exemple, on demande un linux avec un python préinstallé, sur lequel on va rajouter notre application.
Pour liéer Kubernetes, Terraform, Ansible... On lance les machines avec Terraform, et on demande a ansible d'installer docker dessus. On ajoute ensuite les modules via ansible. Kubernetes va être installé par ansible, puis servir a l'orchestration. On peut même rajouter Kubernetes par dessus.

OpenStack sert a gérrer les machines virtuelles a la manière de Terraform.