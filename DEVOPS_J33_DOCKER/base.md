## a quoi sert docker

installer, configurer, limiter les ressources.
Docker permet au final de faire communiquer un OS linux et un OS windows. Sur Windows, Docker lance une VM Linux, car Docker est codé pour linux, mais aussi pour un soucis de sécurité.

## telecharger une image

DockerHub :

- docker pull 'nom du conteneur'

Une image est une application. Elle est lancée par le fichier docker-compose.yml 

## lancer une image

- docker run 'nom du conteneur'

## exemple avec mysql

```shell
docker run --name test-mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:latest
```

- -e permet de définir des variables d'environnement

- -d permet de le lancer en détaché

- mysql: permet de définir la version de mysql à pull

## stopper un conteneur

- docker stop 'id du conteneur'

ou

- docker stop 'nom du conteneur'

## supprimer un conteneur

- docker rm 'id du conteneur'

ou

- docker rm 'nom du conteneur'

## entrer dans le conteneur

```
docker exec -ti 'nom du conteneur' bash
```

- -ti définit le terminal interactif connecté à notre conteneur (ici de type bash)
