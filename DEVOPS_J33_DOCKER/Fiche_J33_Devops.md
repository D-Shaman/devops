# Docker J2

Le dockerfile permet de préciser tout l'environnement pour faire fonctionner l'application et toutes les étapes de mise en place.

Par exemple, pour notre application, nous avons Flask. On a donc besoin de Python, de Mysql (qui est a part) de mysql python connetor, mais aussi flask, il faut aussi un terminal pour lancer l'appli. En fait le dockerfile permet d'automatiser l'initialisation de l'application.

Les extension .js sont en javascript. Le Hello.js est équivalent au app.py sur python. Ce fichier fonctionne avec express (l'équivalent Flask sur javascript). Le package .json est l'équivalent du venv.

Pour une application en npde il faut :

- node
- express
- commande npm i
- commande npm start

Pour créer l'image : docker build img_node .

Exemple dockerfile JS/Json :

```shell
# Je pull alpine
FROM node:alpine3.14

# Je précise un WORKDIT et créer un dossier app
WORKDIR /app
# J'ajoute les fichiers
ADD hello.js /app/
ADD package.json /app/

# On lance les commandes :
RUN npm install

#On une commande CMD qui va démarer le server
CMD "npm start"

# On précise le port pour entrer dans le conteneur
EXPOSE 3000
```

Il est possible d'associer les conteneur a plusieurs réseaux dans le docker-compose.

## Docker tutorial