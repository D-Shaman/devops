# DEVOPS J9 : FRAMEWORK SCRUM

Notes diverses (relatives aux quizz fil rouge):

- Les non-functional requirements : sont des features de performances comme le temps de latence du produit, mais qui ne sont pas une fonctionnalité essentielle.
- Le product owner est responsable de l'explication des items du product backlog
- Si le sprint est cancel mais que certaines features sont Done, alors elles peuvent quand même être implémentées par le PO (après revue).
- A la fin du sprint planning, deux éléments principaux ressortes : les tâches, et les items choisis dans le product backlog
- Le product backlog n'est jamais vraiment fini, donc le sprint ne démare pas juste après l'établissement du product backlog.
- Le PO est responsable du product backlog, et fait les changements sur ce dernier. Le product backlog change a mesure du projet et de l'orientation que donne le client.
- Pas de crunch en agile, les dev doivent travailler a une vitesse constante.
- C'est la Dev Team qui mesurent les performances du sprint. La SCRUM team utilise d'autre metrics, notamment dans le suivi et le reporting (par exemple le nombre d'US traité pour faire la burndown chart).
- Les bugs apparents sont patchés directement si ils sont facile ou critique sinon, ils apparaissent dans le product backlog pour le sprint suivant.
- Les tests d'acceptation : vérifier que le produit est conforme aux spécifications.

## Les différents niveaux de produits

![vision-produit](vision-produit.png)

Il y a dans la première itération une explication des choses. *Donc pas forcément de user stories durant l'itération zéro*. Elle prépare au bon démarrage. A ce moment le PO commence a prioriser les user stories. La DEV team commence alors a réflechir a comment organiser le travail et tous les risques.

### Priorisation des user stories

On commence par celles qui ont un **très fort impact** sur la marque ou la réputation, ou celles qui sont critique pour le succès du produit. Ou celles qui deviennent obsolettes si elles ne sont pas faites maintenant.

![prio](priorisation.png)

### Backlog : les erreurs a éviter

- Ne pas avoir de backlog
- Avoir plusieurs backlog
- Ne pas partager le backlog avec toute l'équipe
- Ne pas actualiser le backlog
- Avoir plus de 150 éléments actifs sur le backlog
- Ne pas prioriser les éléments. Ou trop d'élements prioritaires

### Proxy Product Owner

Cette pratique n'est pas reconnu par le SCRUM car il amène son lot de contraditions. Le Proxy Product Owner sera le bras droit du Product Owner quand celui-ci ne peut pas réaliser toutes ses tâches par manque de disponibilité.

Cela peut parfois arriver quand le PO est à peine disponible pout les développeirs car il est situé côté et non dans la SCRUM team directement.

## Le rôle des développeurs / de la SCRUM Team / DEV Team

Dans cette équipe, il n'y a pas que des développeurs. Le principe majeur de cette équipe c'est qu'elle est auto-gérée.
Dans le manifeste agile, principe n°11 : "Les meilleurs architectures et conceptions émergent d'équipes auto-organisées". Elle est seule a décider la manière dont elle va réaliser les fonctionnalités du sprint backlog.

La mission de cette équipe est de transforamer les user stories en tâche et de réaliser ces tâches. L'objectif est toujours de maximiser la valeur.

- Ils sont responsable de l'incrément
- Chargé de l'estimation
- Réalisateur.

### La responsabilité de la DEV TEAM

Ses droits :

- Être protégé des interventions extérieures pendant l'itération.
- Définir comment réaliser le produit.
- Refuser des développements trop lours ou pas assez bien décrits.

Ses devoirs :

- Estimer collectivement les tâches avant l'itération
- Gérer collectivement les problèmes techniques
- Communiquer avec le PO en terme métier
- Remonter avec courage les problèmes, idées d'améliorations... lors des rétrospectives.
- Être sincère sur son avancement lors du daily meeting.

### Qui sont-ils ?

Une équipe pluridisciplinaire de moins de 10 personnes (idéalement). Les testeurs, dev, et analystes sont mélangés.

### Les critères d'acceptation : la définition of done

C'est une checklist des critères pour considérer les fonctionnalités / user stories comme terminées. Cette définition peut (et doit, en général) évoluer d'un spront a l'autre. Cela responsabilise et implique l'équipe de développement.
Il y a par exemple :

- Les tests.
- La technique.
- Le côté fonctionnel.
- La sécurité...

Une mauvaise definition of done est une cause d'échecs majeurs de la gestion du projet.

### Du côté product Owner : la définition of ready

![dor](dor.png)

Est-ce que l'US (user stories) que le PO a écrit est respectée et prête. Est-ce qu'elle est désirable, suffisament décomposée, est-ce que tout le monde l'a discuté...

C'est côté product backlog, côté PO avec le client, tout est a plat, tout est identifié. Y conpris les risques. Cela montre au client que l'équipe est sérieuse quant au travail a accomplir.

La DOD est côté DEV team, ce que l'on a fabriqué, est-ce que cela répond aux critère et être validé.

### La dette technique

![dette-tech](dette-tech.png)

Au court d'un sprint, le développement a avancé, parfois rapidement, avec vaguement la DOD avec encore des bug, des console log, des boutons mal alignés... Donc tout marche, mais n'est pas propre.

L'accumulation des ces impuretés dans le code vont s'accumuler et peuvent provoquer des problèmes a long terme. C'est le concept de dette technique. Quand il y a trop de dette technique on ne peut plus trop toucher au projet et il serait même moins cher de recommencer plutôt que de réparer.

Le code doit donc être le plus propre possible (clean code) tout au long du projet, afin de le maintenir a long terme. Il existe des outils type SonarQube qui donnent des indications sur la présence de code sale dans le produit.

### Le planning poker

C'est la capacité d'une équipe par rapport aux point d'efforts a gerrer rapport a une US. Il se fait a plusieurs moment du sprint : quand on décide quelles US on ajoute au sprint, puis en milieu / fin de sprint (au cas ou il y a de l'avance).

Ce sont des cartes qui suivent la suite de fibonacci.

#### Le spike

 Spikee : quand on ne sait pas ce qu'on fait techniquement, on fait une recherche, de reciblage métier, poser les bonnes question aux bonnes personnes. A la fin il doit y avoir un proto ou une doc qui va être partagé a la retrospective du sprint.

### La vélocité de l'équipe

C'est le nombre de points traités en une itération.

#### Le concept no estimate

C'est un mouvement qui prone l'arrêt des estimations. Les estimations ne sont que des estimation, mais les clients ont du mal a les considérer comme n'ayant qu'une valeur estimatoire et pas une valeur de deadline. Les estimations peuvent avoir un effet pervert, car il faut prendre le temps de bien faire les choses. On peut par exemple estimer un nombre d'US plutôt qu'une deadline pour une feature. En tout cas les points d'effort ne doivent pas être transformer en unités temporelles.

Ne pas avoir d'estimation permet de se concentrer sur le valeur, et sur le besoin de l'utilisateur.

### Le backlog refinement

Les objectifs :

- Affiner et ordonner le Backlog de produit
- S'assurer que les items du backlog sont pret au démarage du sprint suivant
- Anticiper les impacts et activités a moyen terme

Qui participe :

- PO
- Equipe de dev
- SCRUM Master (pas obligatoire)

Quand ? :

- Pour le PO : autant que nécessaire
- Pour les développeurs : pas plus de 10% du temps de dev

### Le Sprint Backlog

Il est composé de fonctionnalités issues du product backlog en fonction de la priorisation exprimée par le PO. Le nombre de ces items est décidé par l'équipe. Ces fonctionnalités /items/ US sont découpés en tâches.

le sprint backlog évolue en permanence durant le sprint. Le sprint backlog est la responsabilité de l'équipe de DEV **et non pas du PO**.

### Le sprint goal

On veut avoir un objectif de sprint (pour le PO et redécoupé pour les DEV). Il est décidé par l'ensemble de l'équipe SCRUM : "Pourquoi construire cet incrément dans le sprint ?"

Il est utilisé pour voir au quotidien le travail restant par rapport aux objectifs. Il est utilisé par le PO pour communiquer avec les parties prenantes comme une aide a la décisions. Cependant **il n'évolue pas pendant le sprint**.

#### Objectif SMART

On se fixe des objectifs les plus découpés possible : spécifiques, mesurables, réalisables, utiles, et timeboxés.

Par exemple :

- Spécifique : je veux augmenter mes ventes de 25% durant 2021
- Mesurable : obtenir un CA de 10k
- Atteignable : est-ce que j'ai les compétences
- réaliste : 10000 visiteur par jour par rapport aux 10 ajd, est-ce possible ?
- Timeboxé : se fixer une deadline

## Les autres rôles

### Les parties prenantes (stakesholder)

Leur rôle est de s'investir dans la vue produit sans participer a sa réalisation. Ils sont en droit d'assister aux démo et avoir une vue sur l'avancement. Ils sont supposés fournir les moyens de réalisation et donner du feedback, mais auissi faire confiance a l'équipe.

### Le coach Agile

Il accompagne la transformation agile dans une entreprise :

- Durant les phases de transition agile
- Durant les phases de montée en compétence

Il a pour mission d'étudier le contexte et à partir de la :

- Former l'équipe
- Donner l'impulsion de mise en oeuvre

## Les évènements de SCRUM

### Le sprint

C'est un découpage temporisé du projet. Il se base sur le sprint Backlog. Il a une durée maximale d'un mois.

Il peut être annulé si le sprint n'est plus l'actualité. Si on est en avance, on travaille sur de nouvelles features ou des améliorations. Si on est en retard, les US non fini passent en sous-marin pour le sprint suivant.

### Le Daily Meeting (ou mêlée)

![melee](melee.png)

Elle dure 15 minutes max et se trouve toujours a la même heure et au même lieu pour faciliter l'organisation de l'évènement. Elle porte sur les éléments accomplis hier, et aujourd'hui, mais aussi sur les éléments qui peuvent être bloquants.

les acteurs :

- Seul l'équipe de DEV y PARTICIPE, mais tous peuvent y assister
- Le SCRUM Master veille a sa tenue et peut s'y greffer uniquement sur demande de l'équipe et pour aider l'organisation.

Le but de daily :

- Planifier et rythmer les developpements
- Libérer la communication et renforcer l'esprit d'équipe
- Garder le cap et rectifier en cas de dérive
- Anticiper les problématiques

### Le Sprint Review

Il dure 4 heures pour un sprint de 4 semaines. C'est une phrase d'inspection de l'itération.

Ses objectifs :

- Montrer en quoi le sprint est bel et bien terminé et répond aux exigences posées lors de la planiffication
- Faire le constat de ce qui s'est bien ou mal passé durant le sprint
- Faire le point sur le blacklog product, du planing de la réalisation et du budget.
- Prendre en compte les contraintes externes telles que l'évolution du marché
- Anticiper les priorités du prochain sprint.

### Le Sprint Planning

L'objectif est de définir :

- Le sprint goal : la vision macroscopique, pour le sprint
- Le sprint Backlog : la vision microscopique, au jour le jour.

Cette réunion dure 8 heures pour un sprint de 4 semaines.

Sont présent :

- Le SCRUM master
- Le product owner
- L'équipe de développement

#### Nouveauté 2020

La SCRUM team peut inviter des personnes a participer au sprint planning pour donner des conseils.

## Le sprint retrospective

3 heureus pour un sprint de 4 semaines.

Dans cette réunion, on se focalise sur l'équipe a plusieurs niveaux :

- Relationnel et humain
- Processus de réalisation du produit
- C'est là ou on apporte des bonbons :)

Ses objectifs sont :

- Tirer les enseignements de l'itération
- Analyser les plan d'actions précédents
- Prioriser les points pour le prochian sprint
- Déterminer ensemble un plan d'action réaliste ^pour la prochaine itération
- Acteurs présents :
  - l'ensemble de l'équipe SCRUM
- On discute la DOD
- Composition de l'équipe
- Point Sprint review
- Durée du sprint
