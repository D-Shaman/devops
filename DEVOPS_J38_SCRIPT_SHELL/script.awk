#! /bin/bash
nr=0
awk 'BEGIN {FS="="
    print "On affiche les lignes qui ne commencent pas par un ;";}

!/;/ && !/^$/ && /(Off|On)$/ {counter +=1; print $1 $2 $3;}

END { printf "%s / %s\n", counter, NR;}' ./php.ini | column -t