I. REGEX

Exercice 1:
sed 's/^[0-9].*/<data>&<\/data>/' dates_cours.txt

Exercice 2:
1)
nl .bash_profile | sed -e 's/^[ \t]*//'

2)
nl .bash_profile | sed -e 's/^[ \t]*//' -e 's/^[0-9]/[&]/'

II. AWK ligne de commande :

Exercice 1:
ls -l /etc | awk -F' ' '{print $9}'

Exercice 2:
1)
ls -la | awk -F' ' 'match($9,"^\.") {print $0}'

2)
ls -ap | grep -v / | grep '^\.'

Exercice 3:
1)
cat php.ini |awk '$0 ~ /^[^;]/' | awk '$0 ~ /(Off|On)$/'

2)
cat php.ini |awk '$0 ~ /^[^;]/' | awk -F '=' '/(Off|On)$/ {print $1 $2}' | column -t

3) (./script.awk)
#! /bin/bash
nr=0
awk 'BEGIN {FS="="
  print "On affiche les lignes qui ne commencent pas par un ;";}

!/;/ && !/^$/ && /(Off|On)$/ {counter +=1; print $1 $2 $3;}

END { printf "%s / %s\n", counter, NR;}' ./php.ini | column -t

III: Scripts AWK

1) (./delprefixe.awk)
#! /bin/bash
 cat fiche.php | awk ' BEGIN{FS=" "} {gsub(/fic/, "", $2)} {print "\t" $0}'

2)

3)




TESTS :

cat fiche.php | sed -e 's/^[ \t]*//' | awk '$0 ~ /^[^\*]/'

cat fiche.php | awk '{sub(/^\     /, ""); print $0}'

cat fiche.php | awk '$0 ~ /^\    /' | awk -F" " '{print $2}'




cat fiche.php |  awk -F" " '{print $2}' | awk '{gsub(/fic/, ""); print $0}'

cat fiche.php |  awk -F" " '{gsub(/fic/, "", $2)}; {print $0}'
