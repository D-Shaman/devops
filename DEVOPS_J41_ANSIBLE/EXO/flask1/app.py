import os
from flask import Flask, url_for

app = Flask("Projet")

# on récupère la valeur de la variable d'environnement "FLASK_PORT"
SRV_PORT = os.environ.get('APP_PORT', 7000)

# démarrage du serveur
app.run(host='localhost', port=SRV_PORT, debug=True)