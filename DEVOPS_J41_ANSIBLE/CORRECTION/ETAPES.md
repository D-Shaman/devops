# Etapes de création du projet avec Docker

## **Mise en place du réseau**

## **Application A**

### Mise en place d'une base de données bdd.mongo1

### Mise en place d'une application app.flask1

### Connexion entre entre app.flask1 et bdd.mongo1

## **Application B**

### Mise en place d'une base de données bdd.sql1

### Mise en place d'une application app.flask2

### Mise en place d'une seconde application app.flask2 et bdd.sql1

### Mise en place du Gateway