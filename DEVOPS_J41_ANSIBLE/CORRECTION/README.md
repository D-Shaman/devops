# Petite infra microservice en docker

## Description du projet

Exemple de mise en place d'un Gateway HTTP avec des applications Flask

![alt schema](./assets/schema.svg "Schéma du projet")

## Prérequis

- docker
- docker-compose
- VScode

## Demarrage

Dans une console à la racine du projet, taper :

``` bash
docker-compose up -d
```

Vérifier que les conteneurs sont démarrés

## Annexes
