import os
from flask import Flask, url_for

# on récupère la valeur de la variable d'environnement nommée "FLASK_PORT" 
# si la valeur n'est pas trouvée, nous utilisons le port 7000
SRV_PORT = int(os.environ.get('MON_APP_FLASK_PORT', 7000))

# création de l'application
app = Flask("Projet")

# démarrage du serveur
app.run(host='localhost', port=SRV_PORT, debug=True)
