# Application Scraper / Crawler

## Description

C'est un simple projet pour apprendre Jenkins et découvrir les tests unitaires

## Prérequis

docker
git
python >= 3.9 & < 4.
venv avec python
jenkins

## démarrage

Créer l'environnement virtuuel en ouvrant la console, à la racine du projet :

```shell
# venv :  est un programme en Python
# pour créer un copie du répertoire "python" dans notre projet
# .venv : répertoire de destination de notre copie. C'est lui qui contiendra
# les dépendances installées avec "pip install"
python -m venv .venv
# activation de l'environnement virtuel en PowerShell
.\.venv\Scripts\Activate.ps1
# activation de l'environnement virtuel en cmd
.\.venv\Scripts\activate.bat
# activation de l'environnement virtuel en shell/bash/bsh
shell ./.venv/Scripts/activate
# installation des dépendances du projet depuis le fichier requirements.txt
pip install -r .\requirements.txt
```

## notes

- Créer une clé API au format JSON sur votre cloud GCP
- Placer le fichier JSPN généré dans le répertoire de votre projet
- Si vous êtes en powershell : $env:GOOGLE_APPLICATION_CREDENTIALS=".\acces-gcp.json"
- Attention la variable a une durée de vie égale a la console ouverte ou on l'a tapée.
