#!/usr/bin/python3

"""
Point d'entrée du projet "__main__.py" Equivalent d'un fichier.exe
"""

import os
import logging
import json
from google.cloud import storage

# Configuration du logger
logging.getLogger().setLevel(logging.INFO)
# logging.info("Coucou")

# Permet de récupérer lune variable d'environnement qui contient le chemin vers le fichier d'authentification vers GCP
path_fichier_config_gcp = os.environ['GOOGLE_APPLICATION_CREDENTIALS']

# Cette idiome permet d'ouvrir un fichier, d'exécuter des actions et de le refermer automatiquement quand on sort du bloc
# Sinon on peut ouvrir le fichier avec la méthode open() mais il faut faire attention a bien le refermer avec close().
with open(path_fichier_config_gcp) as fichier_config_gcp:
    auth_gcp = json.load(fichier_config_gcp)

# On affichier la variable auth_gcp qui est un json et la valeur correspondant a la clé ['project_id']
# logging.info(f"auth GCP : {auth_gcp} pour le projet: {auth_gcp['project_id']}")

# Permet de se connecter au cloud storage a condition d'avoir chargé la variable d'environnement "GOOGLE_APPLICATION_CREDENTIALS"
# qui contient le chemin vers le fichier json de l'authentification de GCP
storage_client = storage.Client()

# Permet de loader le bucker "poe-devops"
bucket = storage_client.get_bucket("poe-devops")

#Blob = Binary long object
# ICI C'EST LE FICHIER DE DESTINATION
file_dest = 'demontration/diwan.txt'
blob = bucket.blob(file_dest)

# FICHIER HOST
file_path = 'ressources/diwan.txt'

# r pour read, b pour binary
with open(file_path, 'rb') as mon_fichier:
    blob.upload_from_file(mon_fichier)
