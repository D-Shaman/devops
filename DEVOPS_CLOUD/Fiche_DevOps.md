GCP :

Bucket : disque dur

Libellé : 
Etiquettes en système de clé valeur qui donne des information sur le contenu.

Régions : 
On choisi la région. En général plus on est proche, moins c'est cher.

Contrôle des accès:
Uniforme pour que tous les utilisateurs aient accès a tout
Ultraprécis pour que les autorisations soient en fonction de la personne.

Choississez comment prétéger les données :
Aucun (sécurité si quelqu'un écrase un fichier)

On peut importer des fichiers a la dernière objet.

On a donc crée un bucket qui est un genre de serveur de fichier. Il y a les données + l'historique et une description complete du fichier.
Dedans on peut créer un répertoire dans lequel mettre des données.

# Comment y accéder a distance 

Il existe un GCP terminal (cloud shell : console en ligne) qui permet de créer des buckets en ligne de commande, etc...

Créer un bucket :

    gsutil mb gs://<nom du bucket>

Si on souhaite le faire en python, on peut télécharger

    

Et créer des script python qui permettent de tout faire. On peut aussi utiliser Terraform pour démarer une machine virtuel avec l'application que l'on désire via un script. Mais aussi par Curl ou par Postman.

Ne pas oublier de créer l'autorisation des GCP "app Engine default service account"

![gcp.png](gcp.png)


Depuis le terminal cloud :

    gcloud init

    a taper dans la console : 
    $env:GOOGLE_APPLICATION_CREDENTIALS=".\acces-gcp.json"