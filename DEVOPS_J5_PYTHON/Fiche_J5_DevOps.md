# Le débug

## L'outil de débugage

Le début en utilisant print est une mauvaise pratique.
Pour lancer le débug depuis VS, il faut cliquer a coté de la ligne (pour mettre le point rouge) puis executer/démarrer le débugage. Cela permet d'accéder aux variables au moment t ou le point a été posé. On peut ensuite avancer pas par pas.

## Alternatives

Il est nécessaire de prendre de la hauteur par rapport a son code. Par exemple avec la méthode du canard en plastique. C'est un objet a qui on explique son code, on reprend donc la logique initiale et on peut s'apperçevoir de ses propres erreurs.

La dernière méthode utile est d'aller prendre l'air. De faire une pose complète et faire autre chose.

## Correction des exos : morpion

````python
_MATRICE_DE_JEU_ = [[" "," "," "],[" "," "," "], [" ", " ", " "]]
victory = 0

def input_selection ():
    choice_j1 = input("Imput du premier joueur ?\n")
    choice_j2 = input("Input du second joueur ? \n")
    a,b = list(choice_j1)
    c,d = list(choice_j2)
    writing_j1(int(a), int(b))
    writing_j2(int(c), int(d))

def writing_j1(a,b):
    if _MATRICE_DE_JEU_[a][b] == " ":
        _MATRICE_DE_JEU_[a][b] = "o"
        victory_check(_MATRICE_DE_JEU_)
    else : print("J1 Mauvais imput")

def writing_j2(c,d):
    if _MATRICE_DE_JEU_[c][d] == " ":
        _MATRICE_DE_JEU_[c][d] = "x"
        print(*_MATRICE_DE_JEU_, sep="\n")
        victory_check(_MATRICE_DE_JEU_)
    else:
        print("J2 Mauvais imput")

def victory_check (_MATRICE_DE_JEU_):
    global victory 
    if _MATRICE_DE_JEU_[0][:] == ["x", "x", "x"]:
        print("J2  victory")
        victory = True
    elif _MATRICE_DE_JEU_[0][:] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[1][:] == ["x", "x", "x"]:
        print("J2  victory")
        victory = True
    elif _MATRICE_DE_JEU_[1][:] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[2][:] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[2][:] == ["x", "x", "x"]:
        print("J2  victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][0] == ["x", "x", "x"]:
        print("J2  victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][0] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][1] == ["x", "x", "x"]:
        print("J2  victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][1] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][2] == ["o", "o", "o"]:
        print("J1 victory")
        victory = True
    elif _MATRICE_DE_JEU_[:][2] == ["x", "x", "x"]:
        print("J2  victory")
        victory  = True

while not victory :
    input_selection()
````

## Correction Lorem Ipsum

````python
mon_lorem = open(".\lorem.txt", "r")
data = mon_lorem.read()
list_data = data.split()
elements = 0

def truc_qui_fait_le_machin(list_data) :
    for i in set(list_data):
        print(list_data.count(i),i)

set_liste = set(list_data)
print(len(set_liste))

truc_qui_fait_le_machin(list_data)
````

## Correction triangle rectangle

````python
def get_and_split_input():
    taille = input("Quelle est la taille ? \n")
    taille_split = taille.split(" ")
    return taille_split


def separating(tailles):
    max_tailles = max(tailles)
    tailles.remove(max_tailles)
    return max_tailles, tailles


def calculation(grand_coté, other_sides):
    is_right = True if int(grand_coté)**2 ==\
        int(other_sides[0])**2 + int(other_sides[1])**2 else False
    return is_right


def main():
    grand_coté, other_sides = separating(get_and_split_input())
    print("Le triangle est rectangle") if calculation(grand_coté, other_sides)\
        else print("Le triangle n'est pas rectangle")


main()
````

## Correction tri a bulle

````python
def input_get_and_transform():
    user_input = input("Quel sont les chiffres que vous souhaitez trier ?\n")
    splited_input = user_input.split(" ")
    splited_ints = [int(i) for i in splited_input]
    return splited_ints


def swaping(splited_ints, i):
    if splited_ints[i] > splited_ints[i+1]:
        splited_ints[i], splited_ints[i+1] = splited_ints[i+1], splited_ints[i]
    return(splited_ints)


def sorting(splited_ints):
    for i in range(len(splited_ints)-1):
        swaping(splited_ints, i)
    return(splited_ints)


def sorted_verif(splited_ints):
    for i in range((len(splited_ints)-1)):
        sorted_success = True
        if splited_ints[i] > splited_ints[i+1]:
            sorted_success = False
            break
    return(sorted_success)


def main():
    splited_ints = input_get_and_transform()
    while not sorted_verif(splited_ints):
        sorting(splited_ints)
    print(splited_ints)


main()
````