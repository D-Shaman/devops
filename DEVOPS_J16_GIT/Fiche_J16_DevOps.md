# GIT : Retours sur les commandes simples

git ini : initialise le git pour le projet.
git add . : On désigne les fichiers a enregistrer
git commit -m "  " : On enregistre les fichiers.

Au cas ou on ne veut pas ajouter des fichiers au git. On a un fichier **.git ignore.** On a juste a ajouter les fichiers/dossier qu'on ne veut pas ajouter. Il les exclus donc des add et des commit. Il faut donner le path relatif par rapport au fichier.

Quand on envoie hors du pc (vers internet):
4 commandes : fetch, pull, push, merge.

Fetch ne fait pas de mise a jour instantanée alors que pull le fait. Le fetch cherche l'information mais je merge pas.
Le pull fait l'effet de fetch et de merge.

## En local

On a la branche principale, le main (anciennement master). Si le projet est un arbre, alors le main serait le tronc.
De ce main partent des branches. On travaille sur les branches pour les fonctionnalités. Une fois que la feature est finie, elle est merge avec le main (et donc elle disparait).

Il est interdit de travailler sur le main. On créer un branche parallelle qui s'appelle la "develop". C'est la branche sur laquelle on travaille. On va aussi protéger la main (interdiction de commit et de push). La develop est aussi protégée en général (via un système de merge + pull request).
Sur la develop on développe une feature. Puis on merge en faisant un pull request depuis le developpe ce qui supprime la branche feature.

![branches](branches.png)

A partir de ce moment on fait une branche, que l'on va finir par merger (par merge request). On fait alors une association entre l'issue et la branche via le merge request. Ce n'est pas un élément de code mais un simple élément visuel qui permet de voir le travail avancer. Donc quand une issue est merge request, elle est done.

```shell
# On initialise le repo
git init
# Les fichiers ou dossiers qui ne sont pas encore commit, ou les modifications
git status
# On ajoute les fichiers
git add .
git add <nom du fichier>
# On commit
git commit -m "ajout de l'index"
# Pour avoir l'interface graphique
gitK
# Pour voir les branches:
git branch
# Pour créer une branche
git branche <nom de la branche>
# Ou encore, pour créer une branche et me mettre directement dessus
git checkout -b <nom de la branche>
# Pour changer de branche
git checkout <nom de la branche>
#Pour fusionner les branches
git merge <la branche principale> <a branche a absorber>
# Pour fusionner la branche avec la branche actuelle
git merge <nom de la branche a fusionner>
# En cas de conflit : un même fichier modifié différamment dans le main et les branches
# On a alors un choix a faire sur la version a conserver, ou conserver les 2.
# Pour supprimer une branche
git branch -d <branche a supprimer>
```

Si on a plusieurs projet, il faut avoir un git par projet. Pour les regrouper sous un même git, il faut un Git master.

## Sur GitLab ou GitHub

On créer un projet blanc. Il faut lui renseigner un nom. Il va donc créer une URL, puis il va rejouter le titre. On doit ensuite défini la visibilité du projet. Les git pro doivent être passés en privé.

Quand le projet est créer, la plateforme nous donne des lignes de commandes.
D'abord les settup globales : qui permettent d'identifier QUI a créer le projet. On a ensuite l'opportunité de créer un nouveau repo (depuis le online vers le local), si on a deja un .git qui existe il fait les associer.

La partie 3 correspond a si on a deja un repo online pour le projet. La 2 correspond au repos offline qui existe déjà.

On fait alors les mêmes étapes qu'en local : add, commit, puis push sur le gitlab.

```shell
# On pousse la branche main, si la branche n'existe pas, alors elle est crée automatiquement.
git push origin main
```

On peut aussi créer une branche:

```shell
# On créer + se déplace vers la branche
git branch try
git checkout try
# On se prépare a push la branche
git add .
git commit -m "Ajout de la branche"
# On push
git push origin try
# On fait une branche de la branche, elle va contenir les infos de la branche try (cas j'étais dessus)
git checkout-b "try2"
# Pour récupérer les données
git pull
# Pour envoyer les données
git push origin <nom de la branche>
```

### Les merge request

C'est un outil qui permet de merge deux branches. Elle peut être assignée a différentes personnes, il y a aussi la possibilité de mettre une review, et de créer des labels (des titres).
Dans les obptions je peut choisir si la branches a merge va être delete ou non.

La personne en charge des merge va accepter ou non la merge request, et la lancer.

En local, la branche qui a été merge existe toujours. Je vais alors sur la branche master et je fais un git pull (si ça ne fonctionne pas je peut toujours les delete manuellement).

On a les issue : c'est une card qui va définir une fonctionnalité a faire. Par exemple : authentification. Ca sert a définir une tâche a accomplir sur le travail en cours. On l'associe a des développeurs. Une fois que le dev a fini l'issue, alors il peut demander un merge vers la branche develop.

### La sécurité des branches

Il est nécessaire d'empécher les autres de push sur la branche main.
Dans settings -> Repository on peut définir la branche par défaut. En général c'est la dévelop et non pas la master.

Dans "protected branches" on peut définir qui a la possibilité de push ou de merger. En général on protège toujours la master et la develop, il faut donc travailler par des merge request.

## Organisation des branches

On a en général 4 branches principales : la master, la dev, la feature, la hotfix.

La hotflix est en général une branche de la main pour des imoplémentations rapides et importantes. La feature (ou on développe les nouvelles features) est issue de la dev, puis est merge quand la feature est propre. Au final la dev est une fille de la main. Il n'y a que le chef de projet qui peut merge sur la main.

![branches2](branches2.png)

Quand on arrive sur un projet on commence tout de suite par pull. Une feature peut aussi faire plusieurs branches, qui vont ultimement être merge.

### Le HEAD

C'est un pointeur. Il peut pointer sur nimporte quelle branche ou nimporte quel commit qui a été fait. L'avantage est de retourner sur un commit précédent et supprimer le suivant qui contient une erreur.

## GITFLOW

GitFlow est un outil aidant a organiser l'arborescence git. Mais aussi mettre en place des features et hotfix.

```shell
gitflow init main
#Gitflow demande alors plusieurs branches (main, develop, hotfix, release, bugfix....)
```

Gitflow est un ensemble de commandes comme :

```shell
#Ajout d'une feature
git flow feature start <nom de la branche>
#Quand la feature est finie :
git flow feature finish
#Pour une release
git flow release start 0.1.0
git flow release finish -m "Commentaire" '0.1.0'
```

- Une branche develop est créee à partir de master
- Une branche release est crée à partir de la branche develop
- Des branches feature sont crées à partir de la branche develop
- A la fin d'une fonctionnalité, elles est merge a la develop puis supprimée.
