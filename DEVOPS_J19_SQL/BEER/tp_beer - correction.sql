use beer;

# 1 Quels sont les tickets qui comportent l’article d’ID 500, afficher le numéro de ticket uniquement ?

select ventes.NUMERO_TICKET from ventes where ventes.ID_ARTICLE=500;

# 2 Afficher les tickets du 15/01/2014.

select numero_ticket from ticket where date_vente = '2014-01-15';

# 3 Afficher les tickets émis du 15/01/2014 et le 17/01/2014.

select numero_ticket from ticket where date_vente between '2014-01-15' and '2014-01-17' ;

# 4 Editer la liste des articles apparaissant à 50 et plus exemplaires sur un ticket.

select NUMERO_TICKET, nom_article, quantite from ventes 
join Article on article.ID_article = Ventes.ID_Article 
where Ventes.quantite >= 50;

# 5 Quelles sont les tickets émis au mois de mars 2014.

select numero_ticket, date_vente   from ticket where month(date_vente) between 3 and 4;

# 6 Quelles sont les tickets émis entre les mois de mars et avril 2014 ?

select * from ticket where annee = 2014 and month(date_vente) in  (3, 6);

# 7 Quelles sont les tickets émis au mois de mars et juin 2014 ?

select id_article, nom_article , nom_couleur from article join couleur on Article.ID_couleur = Couleur.ID_couleur Order by couleur.id_couleur;

# 8 Afficher la liste des bières classée par couleur. (Afficher l’id et le nom)

select id_couleur, nom_article from Article where id_couleur is null;

# 9 Afficher la liste des bières n’ayant pas de couleur. (Afficher l’id et le nom)

select NUMERO_TICKET, nom_article, quantite from ventes 
join Article on article.ID_article = ventes.ID_Article 
order by NUMERO_TICKET DESC;

# 10 Lister pour chaque ticket la quantité totale d’articles vendus. (Classer par quantité décroissante)

select numero_ticket , sum(quantite) as 'Total de vente' from Ventes group by numero_ticket order by sum(quantite) desc;

# 11 Lister chaque ticket pour lequel la quantité totale d’articles vendus est supérieure à 500. (Classer par quantité décroissante)

SELECT NUMERO_TICKET, SUM(QUANTITE) AS quantite_articles_vendus
FROM ventes
GROUP BY NUMERO_TICKET
HAVING quantite_articles_vendus > 500
ORDER BY quantite_articles_vendus DESC;


# 12 Lister chaque ticket pour lequel la quantité totale d’articles vendus est supérieure à 500.
# On exclura du total, les ventes ayant une quantité supérieure à 50 (classer par quantité décroissante)

select NUMERO_TICKET, ID_ARTICLE, sum(QUANTITE) as somme from ventes
where QUANTITE < 50
group by NUMERO_TICKET 
having somme > 500 order by somme desc;

# 13 Lister les bières de type ‘Trappiste’. (id, nom de la bière, volume et titrage)

select id_article, nom_type, volume, titrage from article 
join Type on Article.ID_type = type.ID_type
where type.nom_type = 'Trappiste';

# 14  Lister les marques de bières du continent ‘Afrique’

select id_marque,nom_marque,nom_pays,nom_continent  from Marque 
join pays on Marque.ID_pays = pays.ID_pays
join continent on Pays.ID_continent = continent.ID_continent
where continent.nom_continent = 'Afrique' ;

# 15 Lister les bières du continent ‘Afrique’

select id_article, nom_article, nom_marque,nom_pays,nom_continent  from Article
join Marque on Article.id_marque = Marque.Id_marque
join pays on Marque.ID_pays = pays.ID_pays
join continent on Pays.ID_continent = continent.ID_continent
where continent.nom_continent = 'Afrique' ;

-- 16. Lister les tickets (année, numéro de ticket, montant total payé). En sachant que le prix
-- de vente est égal au prix d’achat augmenté de 15% et que l’on n’est pas assujetti à la TVA.

SELECT ticket.ANNEE, ticket.NUMERO_TICKET, SUM(quantite*article.PRIX_ACHAT)*1.15 as PRIX_VENTE FROM ticket
INNER JOIN ventes using(numero_ticket,annee)
INNER JOIN article ON article.ID_ARTICLE = ventes.ID_ARTICLE
group by NUMERO_TICKET, ticket.annee;

# 17  Donner le C.A. par année.

 select annee, round(sum((QUANTITE * PRIX_ACHAT)*1.15),2) as 'prix' from Ventes
join Article on article.id_article = ventes.ID_ARTICLE 
group by  ANNEE;


-- 18. Lister les quantités vendues de chaque article pour l’année 2016.
SELECT ventes.ID_ARTICLE, NOM_ARTICLE, SUM(QUANTITE) AS VENTES_PAR_ARTICLE, ANNEE
FROM ventes
INNER JOIN article ON ventes.ID_ARTICLE = article.ID_ARTICLE
WHERE ANNEE = 2016
GROUP BY ventes.ID_ARTICLE
ORDER BY ventes.ID_ARTICLE;

-- 19. Lister les quantités vendues de chaque article pour les années 2014, 2015, 2016.
SELECT ventes.ID_ARTICLE, NOM_ARTICLE, SUM(QUANTITE) AS VENTES_PAR_ARTICLE, ANNEE
FROM ventes
INNER JOIN article ON ventes.ID_ARTICLE = article.ID_ARTICLE
WHERE ANNEE BETWEEN 2014 AND 2016
GROUP BY article.id_article, annee
ORDER BY ANNEE, ventes.ID_ARTICLE;

# 20. Lister les articles qui n’ont fait l’objet d’aucune vente en 2014.

select id_article,nom_article
from article
where( select sum(quantite)
from ventes where id_article =article.id_article and annee = 2014) is null;

# 21. Coder de 3 manières différentes la requête suivante : Lister les pays qui fabriquent des bières de type ‘Trappiste’.


select distinct P.ID_PAYS,NOM_PAYS from pays as P
join Marque as M on M.ID_PAYS = P.ID_PAYS
join Article as A on a.ID_MARQUE = M.ID_MARQUE
join Type as T on a.ID_TYPE =  t.ID_TYPE
where NOM_TYPE = 'Trappiste';


select P.ID_PAYS,NOM_PAYS from pays as P
join Marque as M on M.ID_PAYS = P.ID_PAYS
join Article as A on a.ID_MARQUE = M.ID_MARQUE
join Type as T on a.ID_TYPE =  t.ID_TYPE
where NOM_TYPE = 'Trappiste'
group by p.ID_PAYS,NOM_PAYS;

select P.ID_PAYS,NOM_PAYS from pays as P
where Id_Pays in  (select distinct (select ID_PAYS from Marque
 where ID_MARQUE = a.ID_MARQUE ) from article as A
where ID_TYPE = (select ID_TYPE from Type where NOM_TYPE = 'Trappiste'));


# 22 Lister les tickets sur lesquels apparaissent un des articles apparaissant aussi sur le ticket 2014-856.
select distinct ANNEE,NUMERO_TICKET from Ventes
where ID_ARTICLE in (select ID_ARTICLE from ventes
where annee = 2014 and NUMERO_TICKET = 856);

# 23. Lister les articles ayant un degré d’alcool plus élevé que la plus forte des trappistes.

select ID_ARTICLE, NOM_ARTICLE, TITRAGE from article 
where titrage > (select max(titrage) from article where ID_TYPE = (select ID_TYPE from type where NOM_TYPE = 'Trappiste'))
order by TITRAGE desc;

# 24 Editer les quantités vendues pour chaque couleur en 2014.
select ID_Couleur,NOM_COULEUR, (select sum(QUANTITE) from Ventes
								where ANNEE = 2014 and ID_ARTICLE in (select ID_ARTICLE from Article where ID_Couleur = C.ID_Couleur)) as Quantite
from Couleur as C;

# 25. Donner pour chaque fabricant, le nombre de tickets sur lesquels apparait un de ses produits en 2014.

select ID_FABRICANT,NOM_FABRICANT, (select count(*) from Ticket where CONCAT( annee , NUMERO_TICKET) in (select CONCAT( ANNEE,NUMERO_TICKET)  from Ventes where annee = 2014 and ID_ARTICLE in 
(select ID_ARTICLE from Article where ID_MARQUE in 
	(select id_marque from marque where id_fabricant = F.ID_FABRICANT)))) as 'Nbre Tickets 2014' 
from Fabricant as F
Order by  'Nbre Tickets 2014' desc;

# 26. Donner l’ID, le nom, le volume et la quantité vendue des 20 articles les plus vendus en 2016.

SELECT ID_ARTICLE,NOM_ARTICLE,VOLUME,
	(select sum(quantite) from Ventes
where ID_ARTICLE = A.ID_ARTICLE and ANNEE = 2016) as Qté 
from Article as A
LIMIT 20;

# 27. Donner l’ID, le nom, le volume et la quantité vendue des 5 ‘Trappistes’ les plus vendus en 2016.

SELECT ID_ARTICLE,NOM_ARTICLE,VOLUME,
	(select sum(quantite) from Ventes
where ID_ARTICLE = A.ID_ARTICLE and ANNEE = 2016) as Qté
from Article as A
where ID_TYPE = (select id_type from Type where NOM_TYPE = 'Trappiste')
LIMIT 5;

# 28. Donner l’ID, le nom, le volume et les quantités vendues en 2015 et 2016, des bières dont les ventes ont été stables. (Moins de 1% de variation)

select ID_ARTICLE, NOM_ARTICLE, qte15, qte16, ((qte16 - qte15) / qte15 * 100) as variation from article as a
inner join (select id_article as id15, sum(quantite) as qte15 from ventes where annee = 2015 group by id_article) as r15
inner join (select id_article as id16, sum(quantite) as qte16 from ventes where annee = 2016 group by id_article) as r16
on  a.id_article = r15.id15 and r15.id15 = r16.id16 and a.id_article = r16.id16
having variation between -1 and 1;

# 29  Lister les types de bières suivant l’évolution de leurs ventes entre 2015 et 2016.
# Classer le résultat par ordre décroissant des performances.

SELECT NOM_TYPE, 
((SELECT SUM(QUANTITE) FROM ventes INNER JOIN article using(ID_ARTICLE)	WHERE article.ID_TYPE = type.ID_TYPE AND ANNEE = 2016)-(SELECT SUM(QUANTITE) FROM ventes INNER JOIN article using(ID_ARTICLE)
		WHERE article.ID_TYPE = type.ID_TYPE
		AND ANNEE = 2015	
	)) AS EVOLUTION
FROM type
GROUP BY ID_TYPE
ORDER BY EVOLUTION DESC;

# 30  Existe-t-il des tickets sans vente ?

select t.* from beer.ticket t
left join beer.ventes v on t.NUMERO_TICKET=v.NUMERO_TICKET and t.ANNEE=v.ANNEE
where v.NUMERO_TICKET is null;

# 31  Lister les produits vendus en 2016 dans des quantités jusqu’à -15% des quantités de l’article le plus vendu.

select id_article, annee, sum(quantite) as qte from ventes where annee = 2016 group by id_article
having qte >= (select max(total*0.85) from (select sum(quantite) as total from ventes where annee = 2016 group by id_article) as somme );

# 32 Appliquer une augmentation de tarif de 10% pour toutes les bières ‘Trappistes’ de couleur ‘Blonde’

select * from Article as a
inner join type as t
inner join couleur as c
on a.ID_Couleur = c.ID_Couleur and t.ID_TYPE = a.ID_TYPE
where c.NOM_COULEUR like "blonde"
and t.NOM_TYPE like "trappiste";

SET SQL_SAFE_UPDATES=0;

update article as a
inner join type as t
inner join couleur as c
on t.id_type = a.id_type and c.id_couleur = a.id_couleur
set a.prix_achat = a.prix_achat*0.9
where t.nom_type like "trappiste" and c.nom_couleur like "blonde";

# 33 Mettre à jour le degré d’alcool des toutes les bières n’ayant pas cette information. On y mettra le degré d’alcool de la moins forte des bières du même type et de même couleur.

select * from article as a
inner join (select id_type, id_couleur,min(titrage) from article as a group by id_couleur, id_type) as min
on a.id_type = min.id_type and a.id_couleur = min.id_couleur
where titrage is null;

update article as a
inner join (select id_type, id_couleur,min(titrage) as mt from article as a group by id_couleur, id_type) as min
on a.id_type = min.id_type and a.id_couleur = min.id_couleur
set titrage = mt
where titrage is null;

# 34 Suppression des bières qui ne sont pas des bières ! (Type ‘Bière Aromatisée’)

delete from Article
where ID_TYPE = (select ID_TYPE from type where NOM_TYPE = 'Bière Aromatisée');
alter table article add constraint
foreign key (id_type)
references type (id_type) on delete cascade;
delete from type where nom_type = "Bière Aromatisée";
SET FOREIGN_KEY_CHECKS=1;

