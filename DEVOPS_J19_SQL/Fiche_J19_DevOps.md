# My SQL

## L'Update

Il y a deux commandes : SET et UPDATE. On précise la colonne a update, puis on SET la nouvelle valeur.
On update la table infirmieres, spécifiquement le nom de l'infirmiere a l'ID 1.

```sql
UPDATE cabinet.infirmieres SET nom = "Cul" WHERE idinfirmieres = 1;
```

## Le Delete

On précise la table, et ce quoi on veut delete.

```sql
DELETE FROM cabinet.adresses WHERE idadresses = 2;
```

## Le Select

```sql
# récupérer toutes les données d'une table
select * from user;  

# récupérer les données de l'id 5
SELECT * FROM user WHERE id = 5;

# récupérer les données quand le nom est durant
SELECT * FROM user WHERE nom = 'dupont';

# récupérer les données quand plusieurs conditions 
SELECT * FROM user WHERE nom = 'dupont' AND prenom = 'jean';


SELECT *
FROM   USER
WHERE  nom = 'dupont'
OR prenom = 'pierre'; 


SELECT * FROM user WHERE nom IN('dupont','durant');

# SUPPRIMER LES DOUBLONS 
SELECT DISTINCT nom FROM user;

# récupérer sur une plage de données
SELECT * FROM  user WHERE age BETWEEN 15 AND 45;

# récupérer des données quand le comparateur contient un element spécifique
SELECT * FROM user WHERE prenom LIKE '%a%';

SELECT * FROM user WHERE prenom LIKE 'm%';

SELECT * FROM user WHERE prenom LIKE '%e';

SELECT * FROM user WHERE prenom = 'marie';
SELECT * FROM user WHERE prenom LIKE "marie";

# récupérer toutes les valeurs quand le champ est null
SELECT * FROM user WHERE address_id is null;

# récupérer toutes les valeurs quand le champs n est pas null
SELECT * FROM user WHERE address_id is not null;

# ordonner la réponse
# croissant
SELECT * FROM user ORDER BY age ASC;

# décroissant
SELECT * FROM user ORDER BY age DESC;

# délimiter le nombre de réponse
select * from user order by age limit 1;

# les UNION
SELECT * from test.b1
UNION
SELECT * from test.b2;

# les fonctions 
# récupérer la valeur maximale
SELECT MAX(recette) AS recette FROM test.b1 as b1;

# récupérer la valeur minimale
SELECT MIN(recette) AS recette FROM test.b1 as b1;

# récupérer la somme des recettes
SELECT SUM(recette) AS recette FROM test.b1 as b1;

# récupérer la moyenne des recettes
SELECT ROUND(AVG(recette), 0) AS recette FROM test.b1 as b1;

# grouper par ville
SELECT * FROM test.b1;
SELECT *, AVG(recette) FROM test.b1 group by ville;

# clause having quand on récupère le résultat d une fonction (remplace WHERE)
SELECT *, AVG(recette) FROM test.b1 group by ville having ville = 'lille';

# les jointures

SELECT u.nom, u.prenom, u.age, a.rue, a.cp, a.numero, a.ville FROM user as u
INNER JOIN address as a ON u.address_id = a.id;

SELECT u.nom, u.prenom, u.age, a.rue, a.cp, a.numero, a.ville FROM user as u
NATURAL JOIN address;

SELECT u.nom, u.prenom, u.age, a.rue, a.cp, a.numero, a.ville FROM user as u
LEFT JOIN address as a ON u.address_id = a.id;

SELECT u.nom, u.prenom, u.age, a.rue, a.cp, a.numero, a.ville FROM user as u
RIGHT JOIN address as a ON u.address_id = a.id;
```

## Les Jointures

Le inner joint fait la relation entre une clé primaire et une clé étrangère. Le natural join fait la même chose quand la clé primaire et étrangère ont le même nom.

La jointure classique par INNER JOIN :

```sql
# On fait la relation entre les deux tables avec cléprimaire = cléétrangère
SELECT code, libelle, numero, rue, cp, ville FROM pays
INNER JOIN address ON pays.id = adress.pays_id;
```

Tout ce qui est dans la table de gauche est affiché, et s'il n'y a pas de correspondance avec la table de droite, alors NULL.

```sql
SELECT numero, rue, cp, ville FROM adress
LEFT JOIN pays ON address.pays_id = pays.id;
```

```sql
```

```sql
```

```sql
```

```sql
```
