# def input_get_and_transform():
#     user_input = input("Quel sont les chiffres que vous souhaitez trier ?\n")
#     splited_input = user_input.split(" ")
#     splited_ints = [int(i) for i in splited_input]
#     return splited_ints

# def sorting(splited_ints):
#     for i in range(len(splited_ints)-1):
#         if splited_ints[i + 1] < splited_ints[i]:
#             temp = splited_ints[i+1]
#             splited_ints[i+1] = splited_ints[i]
#             splited_ints[i] = temp
#     return(splited_ints)

# def sorted_verif(splited_ints) :
#     for i in range((len(splited_ints)-1)):
#         sorted_success = True
#         if splited_ints[i] > splited_ints[i+1]:
#             sorted_success = False
#             break
#     return(sorted_success)


# def main():
#     splited_ints = input_get_and_transform()
#     while not sorted_verif(splited_ints):
#         sorting(splited_ints)
#     print(splited_ints)

# main()

def input_get_and_transform():
    user_input = input("Quel sont les chiffres que vous souhaitez trier ?\n")
    splited_input = user_input.split(" ")
    splited_ints = [int(i) for i in splited_input]
    return splited_ints


def swaping(splited_ints, i):
    if splited_ints[i] > splited_ints[i+1]:
        splited_ints[i], splited_ints[i+1] = splited_ints[i+1], splited_ints[i]
    return(splited_ints)


def sorting(splited_ints):
    for i in range(len(splited_ints)-1):
        swaping(splited_ints, i)
    return(splited_ints)


def sorted_verif(splited_ints):
    for i in range((len(splited_ints)-1)):
        sorted_success = True
        if splited_ints[i] > splited_ints[i+1]:
            sorted_success = False
            break
    return(sorted_success)


def main():
    splited_ints = input_get_and_transform()
    while not sorted_verif(splited_ints):
        sorting(splited_ints)
    print(splited_ints)


main()
