def input_get_and_transform():
    user_input = input("Quel sont les chiffres que vous souhaitez trier ?\n")
    splited_input = user_input.split(" ")
    splited_ints = [int(i) for i in splited_input]
    return splited_ints

def resolve(splited_ints):
    unsorted = True
    while unsorted :
        unsorted = False
        for i in range(len(splited_ints)-1):
            if splited_ints[i] > splited_ints[i+1] :
                splited_ints[i], splited_ints[i+1] = splited_ints[i+1], splited_ints[i]
                unsorted = True
    return(splited_ints)

def main ():
    splited_ints = input_get_and_transform()
    resolve(splited_ints)
    print(splited_ints)

main()