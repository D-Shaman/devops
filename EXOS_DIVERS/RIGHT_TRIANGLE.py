# def get_input () :
#     taille = input("Quelle est la taille ? \n")
#     return(taille)

# def split_input (taille) :
#     taille_split = taille.split(" ")
#     return(taille_split)

# def get_max (tailles) :
#     max_tailles = max(tailles)
#     return(max_tailles)

# def get_other_sides (tailles, max_tailles) :
#     tailles.remove(max_tailles)
#     return(tailles)

# def calculation (grand_coté, other_sides) :
#     if int(grand_coté)**2 == int(other_sides[0])**2 \
#     + int(other_sides[1])**2 :
#         is_right = True
#     else :
#         is_right = False
#     return(is_right)

# def main () :
#     taille = get_input()
#     tailles = split_input(taille)
#     grand_coté = get_max(tailles)
#     other_sides = get_other_sides(tailles, grand_coté)
#     is_right = calculation(grand_coté, other_sides)
#     print("Le triangle est rectangle")\
#     if is_right else print("Le triangle n'est pas rectangle")

# main()


def get_and_split_input():
    taille = input("Quelle est la taille ? \n")
    taille_split = taille.split(" ")
    return taille_split


def separating(tailles):
    max_tailles = max(tailles)
    tailles.remove(max_tailles)
    return max_tailles, tailles


def calculation(grand_coté, other_sides):
    is_right = True if int(grand_coté)**2 ==\
        int(other_sides[0])**2 + int(other_sides[1])**2 else False
    return is_right


def main():
    grand_coté, other_sides = separating(get_and_split_input())
    print("Le triangle est rectangle") if calculation(grand_coté, other_sides)\
        else print("Le triangle n'est pas rectangle")


main()
