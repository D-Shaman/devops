def get_input():
    number_input = input("Quelle est vôtre suite de chiffre ? \n")
    return(number_input)

def splitting(number_input):
    receiving_list = [0] * len(number_input)
    for i in range(len(number_input)):
        receiving_list[i] = number_input[i]
    return(receiving_list)

def inting(receiving_list):
    int_list = [int(i) for i in receiving_list]
    return(int_list)

def adding(int_list):
    final_number = 0
    for i in range(len(int_list)):
        final_number = final_number + int_list[i]
    return(final_number)

def main():
    receiving_list = splitting(get_input())
    int_list = inting(receiving_list)
    final_number = adding(int_list)
    print(final_number)

main()