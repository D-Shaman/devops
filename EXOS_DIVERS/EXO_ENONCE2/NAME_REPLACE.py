def get_and_transform():
    name_input = input("Quels sont les noms ?\n")
    target_name = input("Quel est le nom cible ?\n")
    new_name = input("Quel est le nom a ajouter ?\n")
    name_list = name_input.split(" ")
    return name_list, target_name, new_name


def operating(name_list, target_name, new_name):
    for i in range(len(name_list)):
        if name_list[i] == target_name:
            name_list[i] = new_name
    return name_list


def main():
    name_list, target_name, new_name = get_and_transform()
    operating(name_list, target_name, new_name)
    print(name_list)


main()
