# Les serveurs Cloud

Ce sont du stockage ou des services a distance. Il y en a 3 types. On peut voir ça comme des niveaux de personnalisation possible pour l'architecture cloud :

- IaaS : Infrastructure as a service. C'est de l'infrastructure technique, de la bande passante, des serveurs, du rooting... C'est la qu'on va avoir la main mise sur nôtre infra. On nous donne le matériel et on se débrouille avec. La maintenance est sous la garantie du cloud provider (CPU, etc). Il n'y a donc pas d'investissement matériel, et on bénéficie de la sécurité matériel, car tout est dans un datacenter. S'il y a une panne sur le matériel, il ne doit pas y avoir d'interruption de service, car le service est instantanément répliqué. On peut moduler les ressources en cas de besoin.
- PaaS : Plateform as a service. Dans ce mode on gèrre des fonctionnalités, comme une base de donnée, des serveurs de cache, d'email, des serveurs HTTP... On est pas sur des applications. Ils facilitent la création des serveurs par exemple. Mais on y a pas proprement accès.
- SaaS : Software as a service. Ici on est sur de l'applicatif, comme gmail, office 365, dropbox. Les mises a jours sont automatiques. On nous fournie seulement l'accès, via une location. On achète en général pas une licence.

![cloud.png](cloud.png)

Le service AWS est cloisonné entre régions. Il n'y a pas de réplication possible entre région, et cela implique aussi des zones de disponibilité (point positif, quand il y a un problème sur une région, cela n'impacte pas les autres).

1) Les service Elastic Beanstalk (PaaS):

Il permet de lancer un environnement : un OS et le software qui va avec. C'est possible via le site et en version ligne de commande. On peut charger le code directement.

Dans ce modèle on a l'application liée a un environnement. On peut avoir plusieurs enviornnement pour la même application (prod, com, dev...).

2) Le EC2 : Elastic Cloud Computing

C'est un service d'instanciation de machines virtuelle. Un EC2 est une coquille vide, qui ne va avoir que de la RAM et un CPU (donc c'est de l'IaaS). Il faut donc un OS (AMI : Amazon Machine Images, c'est comme une image ISO). Certaines AMI sont payant ou une infra est déjà installée.

Il faut en plus plugger un storage : a voir dans la section volume. L'EC2 doit pouvoir communiquer avec l'extérieur. De base il est crée sans aucun accès, il faut donc créer un groupe de sécurité qui va donner les règles de ports (un peu comme un firewall). Cela permet les entrées et les sorties.

Quand on lance une EC2 on a une IP dynamique. On peut plugger une IP elastic, et donc avoir une IP "fixe". Pour se connecter au EC2 en SSH, la connection se fait obligatoirement par une Key PAIR. On peut aussi mettre en place un load balancer et un autoscalling pour les EC2.

Au final on a :
- Récupéré une AMI : attention les ami sont région specific
- Configuré le type d'instance : t2.micro
- Configurer le groupe de sécurité
- Récupérer la keypair


## En mode client

Les commandes fonctionnent de la manière suivante :

aws SERVICE OPTION

Il va falloir créer un utilisateur pour communiquer avec AWS et donner des droits. 

Sous ce système, les permissions peuvent faire parti d'un groupe ou non. Les utilisateurs peuvent être dans des groupes, ou avoir directement des permissions, ou avoir un rôle, qui contient un ensemble de permission. Un groupe peut aussi contenir des rôles. Un utilisateur peut être humain, ou une machine (application, service AWS, etc...). On parle donc d'acteur plutôt que d'utilisateur.

Configurer l'utilisateur crée depuis le web. L'utilisateur qui crée le compte amazon est l'utilisateur racine, on peut ensuite créer des utilisateur, puis se connecter avec pour ne pas utiliser l'utilisateur racine qui est en admin.

    aws configure

Il faut remplir l'ID et la clé secrète. Bien choisir la région, puis le mode de diplay (text ou json).

    aws ec2 describe-instances

Pour récupérer une info spécifique 
La query permet de faire remonter de l'information.

    aws ec2 describe-instances --query "Reservations[].Instances[].InstanceId

    aws ec2 describe-instances --query "Reservations[].Instances[].State.Name"

Pour filtrer on utilise --filter. Le filter permet de filtrer les données que l'on recherche.

    aws ec2 describe-instances --filter "Name=instance-type,Values=t2.micro"

    aws ec2 describe-instances --filter "Name=instance-state-name,Values=stopped"

Donc pour lancer une instance en ligne de commande :

Trouver une image

    aws ec2 describe-images --region eu-west-3 --owners self amazon --filter "Name=description,Values=*linux*"

Décrire une image


    aws ec2 describe-images --owners 309956199498 --query 'sort_by(Images, &CreationDate)[*].[CreationDate,Name,ImageId]' --filters "Name=name,Values=RHEL-7.?*GA*" --region us-east-1 --output table

    aws ec2 describe-images --region eu-west-3 --owners amazon --filter "Name=name,Values=*linux*" --query 'sort_by(Images, &CreationDate)[*].[CreationDate,Name,ImageId]'

Créer un groupe de sécurité

    aws ec2 create-security-group --description "Groupe de sécurité M2i" --group-name "M2i-security-group"

    L'output est un ID du groupe de sécurité exemple  : sg-0dcd6e6cd73a6b388

Note : ingress -> règle entrante
        egress -> règle sortante

Générer les KeyPair

    aws ec2 create-key-pair --key-name m2i-key --query KeyMaterial --output text > m2ikey.pem

Ajout d'une règle entrante :

    aws ec2 authorize-security-group-ingress --group-name m2i-security-group --protocol tcp --port 22 --cidr 0.0.0.0/0

Commande de lancement du ec2

    aws ec2 run-instances --image_ids ami-0f7cd40eac2214b37 --instance-type t2.micro --security-group-ids sg-020fc0b9e94e04b2b --key-name exokey

     aws ec2 run-instances --image-id ami-0f7cd40eac2214b37 --instance-type t2.micro --security-group-ids sg-020fc0b9e94e04b2b --key-name exokey

Récupération de l'ip publique de l'instance crée 

    aws ec2 describe-instances --instance-ids i-004789a598956ee66 --query "Reservations[].Instances[].PublicIpAddress"

Voir les instances en cours :

    aws ec2 describe-instances --filters "Name=instance-state-name,Values=running"

Récuperer l'ip public des instances en cours:

    aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[].Instances[].PublicIpAddress" --output text

Terminer une instance

    aws ec2 terminate-instances --instance-ids <>

## Retours sur elastic beanstalk

Indiquer que le dossier va être en lien avec elasticbeanstalk

    eb init

Créer l'environnement associé a l'application

    eb create

Lancer l'application

    eb open

Status de l'application

    eb status

Santé de l'application

    eb health

Résilier l'application

    eb terminate --all

Prégiser a une branche git d'utiliser un environnement particulier (faire le lien entre environnement)

    eb use <>

Deployer les changements

    eb deploy

On créer donc un eb init par projet et ce sont les environnement eb create qu'il faut versionner

## Le S3

Le S3 peut être vu comme un DropBox

C'est le seul service Amazon qui n'est pas régionalisé. Chaque S3 a des buckets (ou compartiments) chaque bucket contient des objets. Nimporte quel type d'objet (fichiers texte, vidéo, musique...). Le s3 est extensible, il n'a pas de limite de taille.

Docs utile : AWS S3 api, et aws s3 cli

Créer un bucket

    aws s3 mb <nom en protocole s3://lemegabucket>

Lister les compartiment ou objets

    aws s3 ls <bucket url>

Supprimer un bucket

    aws s3 rb <url> --force

suppriler un fichier dans le bucket

    aws s3 rm s3://<url bucket>/<nom du fichier>
    -- pour le récursif

deplacer un objet

    aws s3 mv s3://<url>/<chemin> s3://<url>/<destination>

Copier un objet

    aws s3 cp s3://<url>/<chemin> s3://<url>/<destination>

Copier depuis le host vers le remote 

    aws s3 ./<file on host> s3://<url>/<destination>
    Fonctionne avec les fichiers et les dossiers en ajoutant le tag --recursive

Synchroniser un dossier cloud

    aws s3 sync . s3://lemegabucket
    a refaire a chaque fois qu'on ajoute un fichier

Si on remove un élément il faut refaire

    aws s3 sync . s3://lemegabucket --delete

Si on veut suppr uniquement a distance

    aws s3 sync s3://<url>/fichier . --delete

### AWS API

Lister tous les buckets

    aws s3api list-buckets

Lister les objets d'un bucket

    aws s3api list-objects --bucket "<nom du bucket>"

Supprimer un objet spécifique

    aws s3 delete-object --bucket <nom du bucket> --key <nom du fichier>

Upload ou download un fichier

Upload 

    aws s3api put-object --bucket <nom du bucket> --key <nom et destination que le fichier devra prendre> --body <chemin du fichier a upload>

delete un bucket

    aws s3api delete-bucket --bucket <nom du bucket>

## Code DEPLOY

Avec EBS beaucoup d'étapes sont automatiques:
- Cobstruction des rôles et permissions pour les services AWS
- On ne gère pas l'instance ec2
- On ne gère pas les installations (docker, php, python)

Avec CD on va pouvoir gérer tout ça.

Créer un rôle pour code deploy (de base il ne peut communiquer ni avec un EC2 ni S3). Voir CodeDeploy-trust.json qu'on va attacher au service code deploy.

Attribuer la stratégie au service
Créer le rôle de service pour code deploy
    
    aws iam create-role --role-name CodeDeployServiceRole --role-name-policy-document file://iam/CodeDeploy-trust.json

Attacher la stratégie au rôle de service

    aws iam attach-role-policy --role-name CodeDeployServiceRole --policy-arn <arn de la policy a aller chercher sur le site>

Créer la stragégie pour le profil d'instance

    aws iam create-role --role-name CodeDeploy-EC2-Instance-Profile --asume-role-policy-document file://iam/CodeDeploy-EC2-trust.json

    aws iam put-role-policy --role-name CodeDeploy-EC2-Instance-Profile --policy-name CodeDeploy-EC2-Permissions --policy-document file://iam/CodeDeploy-EC2-permission.json

Création du profil d'instance (on peut attacher des rôle a une instance pour accéder aux autres services)

    aws iam create-instance-profile --instance-profile-name CodeDeploy-EC2-Instance-Profile

Ajout du rôle au profil d'instance

    aws iam add-role-to-instance-profile --instance-profile-name CodeDeploy-EC2-Instance-Profile --role-name CodeDeploy-EC2-Instance-Profile

Verification 

    aws iam get-instance-profile --instance-profile-name CodeDeploy-EC2-Instance-Profile

Créer de déploiement pour l'instance (Des HOOKS, il y en a 4 : before install, after install, ApplicationStart, ApplicationStop)

Les hooks prennent des script shell qui seront exécutés lors du déployment

Dans un fichier appspec.yml

    Voir yml
    Voir dossier script

Création du bucket

    aws s3 mb s3://<nom>

Attacher la stratégie a notre bucket

    (création du bucketpolicy.json) puis

    aws s3api put-bucket-policy --bucket kouzine --policy file://iam/BuckerPolicy.json

Vérifier les startégies du bucket 

    aws s3api get-bucket-policy --bucket kouzine


INSTALLATION DE CODEDEPLOY SUR LA MACHINE EC2
Les machines EC2 ont besoin d'un agent EC2 qui doit être installé.
On lance une machine ec2

    > sudo yum install ruby
    > wget https://aws-codedeploy-eu-west-3.s3.eu-west-3.amazonaws.com/latest/install

Installation du script install

    > sudo ./install auto

Lancement de code deploy sur l'EC2 

    > sudo service codedeploy-agent start

Vérification 

    > sudo service codedeploy-agent status


Il faut maintenant préciser a codedeploy sur quel EC2 il va deploy VIA les balises
Créer l'application 

    aws deploy create-application --application-name kouizine

Push le dossier dans lequel on est vers le S3

    aws deploy push --application-name kouizine --s3-location s3://kouzine/app.zip
    Ici app.zip est un nouveau nom

Créer un groupe de deploy

    aws deploy create-deployment-group --application-name kouizine --deployment-group-name app-prod --deployment-config-name CodeDeployDefault.OneAtATime --ec2-tag-filters Key=server,Value=dev,Type=KEY_AND_VALUE --service-role-arn <arn du CodeDeployServiceRole>

Lancer le deployment

    aws deploy create-deployment --application-name kouizine --deployment-group app-prod --deployment-config-name CodeDeployDefault.OneAtATime --s3-location bucket=kouzine,bundleType=zip,key=app.zip

### LANDBA (PaaS)

C'est de l'exécution sans serveur. Donc sans besoin de lancer un EC2. C'est en général pas de choses dont on se sert en prod.

On peut ajouter un déclencheur, et une destination ou va arriver un retours.

### RDS (PaaS)

RDS est une base de donnée MySQL

Se connecter en remote sur le sql amazon


    Depuis un container mariadb lancé avec docker run -d -e MYSQL_ROOT_PASSWORD=root
    docker ps

    docker exec -it <id container> bash
    Dans le conteneur je me connecter a la db amazon    
    mysql -h <url de la bdd amazon> -P 3306 -u root -p

en cli (avec les auth ajouté au profil cli : AmazonRDSFullAccess)

    aws rds create-db-instance --db-name toto --db-instance-identifier toto-db --engine mysql --db-instance-class sb.t2.micro --master-username root --master-password rootroot --vpc-security-group-ids sg-01265e40a2f3aa9a4 --publicly-accessible --port 3306 --alocated-storage 20

Vérification 

    aws rds describe-db-instances --db-instance-identifier <identifier de la db> --query "DBInstances[].DBInstanceStatus"
    aws rds describe-ds-instances --db-instance-identifier <identifier de la db> --query "DBInstances[].Endpoint.Address

### DynamoDB

Avec dynamoDB on parle de tale et pas de collection, mais ça reste du noSQL

On peut utiliser boto3 le SDK pour dynamo qui permet de CRUD dans dynamo. Il peut se connecter a tous les services AWS donc on peut aussi l'utiliser pour créer et manager des EC2 et des S3.