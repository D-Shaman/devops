#! /bin/bash

# $@ = tous les arguments envoyés
#for lettre in $@
#do
#  echo $lettre
#done

#Ici on récupère séquentiellement les valeurs du in
#for lettre in a b c d e f
#do
#  echo $lettre
#done

for ((i=0 ; i<$1 ; i++))
do
    echo $1
done
