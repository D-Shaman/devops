# Les scripts

On peut mettre toutes les commandes qu'on veut dans un script. Par exemple :

```shell
#! /bin/zsh
echo "Hello World!"
```
