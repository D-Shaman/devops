#! /bin/bash

tableau=($@)

#Pour afficher un tableau ${tableau[indice]}

#Afficher tout le tableau
echo "Le contenu du tableau est ${tableau[@]}"

#Dernier élément du tableau
echo "Dernier élément du tableau ${tableau[-1]}"

#Afficher la taille du tableau
echo "La taille du tableau est de ${#tableau[@]}"

#echo "Tableau de taille ${#tableau[@]} qui contient ${tableau[@]}"

for elem in ${tableau[@]}
do
    echo $elem
done

for (( i=1; i <= ${#tableau[@]}; i++ ));
do
    echo ${tableau[$i]}
done
