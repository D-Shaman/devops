#! /bin/bash

nameFlag=false

#echo "Quel est le nom du dossier ?"

#Lecture de la console.
read -r -p  "Quel est le nom du dossier ? " name

#Creation du dossier.

if [[ -d $name ]];
then
        echo "Le dossier existe deja"
elif [[ -z $name ]]
then
        echo "Pas de nom renseigné"
        exit 1
else
        #Création du dossier
        echo "Création du dossier $name a l'emplacement $PWD"
        mkdir $name
        nameFlag=true
fi

#Si la dernière commande s'est bien passée
if [[ $? -eq 0 ]] && [[ "$nameFlag" = true ]];
then
        tree
else
        echo "Erreur $? à la création du dossier"
fi
