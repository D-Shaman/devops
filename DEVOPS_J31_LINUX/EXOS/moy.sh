#! /bin/bash

tours=0
total=0
read -r -p "Quel est votre note ?" note 

while [[ $note -gt -1 ]] &&  [[ note != "q" ]]; do
    ((tours++))
    total=$(( $total + $note ))
    echo $(($total/$tours))
    read -r -p "Quel est votre note ?" note
done
