#! /bin/bash

user=$@
if [[ -z $user ]];
then
    read -r -p "Quel est l'user a vérifier ? " user
fi

function menu() {
    echo "1 - Vérifier l'existance d'un utilisateur"
    echo "2 - Connaître l'UID d'un utilisateur"
    echo "q - Quitter"
    read -r -p "Quel est votre choix ? " choix
    return $choix
}

function verif() {
    if [[ $(cat /etc/passwd|grep $user|wc -l) != 0 ]];
    then
        echo "L'utilisateur existe"
    else
        echo "L'utilisateur n'existe pas"
    fi
}

function blabla() {
    if [[ $(cat /etc/passwd|grep $user|wc -l) != 0 ]];
    then
        echo "L'UID est $(cat /etc/passwd | grep $user | cut -d: -f3)."
    else
        echo "L'utilisateur n'existe pas"
    fi
}

function tree() {
    if [[ $choix -eq 1 ]];
    then
        verif
    elif [[ $choix -eq 2 ]];
    then
        blabla
    else
        exit
    fi
}

function main(){
    menu
    tree
}

main
