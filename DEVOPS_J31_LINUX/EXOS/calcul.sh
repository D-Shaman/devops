#! /bin/bash

calcul=$@
if [[ -z $calcul ]];
then
    read -r -p "Quel est votre calcul " calcul
fi

function determine() {
    if [[ $(echo "$calcul" | cut -d \  -f2) = "+" ]];
    then
        echo "Addition"
    elif [[ $(echo "$calcul" | cut -d \  -f2) = "-" ]];
    then
        echo "Soustraction"
    elif [[ $(echo "$calcul" | cut -d \  -f2) = "*" ]];
    then
        echo "Multiplication"
    fi
}

determine