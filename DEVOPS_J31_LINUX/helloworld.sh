#! /bin/zsh

echo " "
echo "Hello World, $USER."
echo "Vous êtes dans le répertoire $PWD"

#Attribution de variable.
message="Blabla"
echo $message

#Syntaxe pour l'arithmétique.
result=$((5+8))
echo $result

#Syntaxe arithmetique 2, en réutilisant des variables.
a=5
b=12
echo $((a+b))

#Attribution d'un résultat arithmétique a une variable
let res=12+5
echo "res = $res"

#Récupération des arguments.
echo $1 $2

#Ajouter une variable d'environnement
export x=23
