# Les droits en SQL

Pour savoir qui est connecté actuellement sur le compte

```SQL
SELECT CURRENT_USER();
```

Créer un utilisateur. On renseigne l'user name et l'host name. Pour le host name on rentre une IP ou localhost (si on a localhost en host name, l'utilisateur ne peut pas se co depuis une autre IP. ).

```SQL
# Créer un user
CREATE USER 'nom'@hostname;
# Nimporte qui peut se connecter avec la bonne IP.
CREATE USER '%'@'127.0.0.1'
# Je me connecte en temps que marie, de nimporte ou.
CREATE USER 'marie'@'%'
```

Pour le password on peut set a la création ou après.

```SQL
# Pdt la création :
CREATE USER 'nom'@'localhost' IDENTIFIED BY 'root'
# Après la création :
SET PASSWORD FOR 'nom'@'hostname' = 'password';
```

Supprimer un user :

```SQL
DROP USER 'nom'@'hostname';
```

Donner un droit (select) :

```SQL
# Toutes les tables du TP beer
GRANT SELECT ON beer.* TO 'nom'@'hostname';

# Retirer un droit :
REVOKE SELECT ON beer.* FROM 'nom'@'hostname';

# Donner un droit pour une table spé
GRANT SELECT ON beer.contienent TO 'nom'@'hostname';

#Plusieurs droits :
GRANT SELECT, DELETE ON beer.continent to 'nom'@'hostname';

#Donner tout les droits :
GRANT ALL PRIVILEGES ON beer.article TO 'nom'@'hostname';

#Retirer tout les droits
REVOKE ALL PRIVILEGE, GRANT OPTION FROM 'nom'@'hostname';

#Voir les droits d une personne :
SHOW GRANT FROM 'nom'@'hostname';
```

Pour donner des droits a plusieurs personne on crée des rôles qui contiennent des droit, après on attribue le rôle a des utilisateurs. Quand on donne des droit a une personne il faut le set.

```SQL
#Créer un rôle
CREATE ROLE 'guest';

#Donner des droits au rôle
GRANT SELECT ON beer.* TO 'guest';

#Donner le role a un user
GRANT 'guest' TO 'nom'@'hostname';

#Activer le rôle a l utilisateur
SET DEFAULT ROLE 'guest' TO 'nom'@'hostname';
```

## Les sessions

Permet de simuler une connection au serveur.

## Fiche prof

```SQL
# les droits

# afficher l utilisateur en cours
select current_user();

# création d un utilisateur
CREATE USER 'test44'@'localhost';

# n importe qui pourra se connecter depuis localhost
CREATE USER '%'@'localhost';

# l utilisateur test444 pourra se connecter depuis n importe ou
CREATE USER 'test444'@'%';

# ajout d un mdp à l utilisateur ou modification 
SET password FOR 'test44'@'localhost' = 'root';
ALTER USER 'test44' IDENTIFIED BY 'root';

# création d un utilisateur avec un mdp 
CREATE USER 'test45'@'localhost' IDENTIFIED BY 'root';

# suppression d un utilisateur
DROP USER 'test45'@'localhost';


# ajout du droit select pour la bdd beer,  la table continent à l utilisateur
GRANT SELECT ON beer.continent TO 'test44'@'localhost';

# ajouter tous les droits à un utilisateur sur une table 
GRANT ALL PRIVILEGES ON beer.continent TO 'test44'@'localhost';

# supprimer tous les droits d un utilisateur 
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'test44'@'localhost';

# supprimer un droit à un utilisateur
REVOKE SELECT  ON beer.* FROM 'test44'@'localhost';

# visualiser les droits d un utilisateur
SHOW GRANTS FOR 'test44'@'localhost';

# création d un role 
CREATE ROLE 'monRole';

# supprimer un role 
DROP ROLE 'monRole';

# ajouter un droit à un role
GRANT SELECT ON beer.continent TO 'monRole';

# attribuer un role à un utilisateur
GRANT 'monRole' TO 'test44'@'localhost';

# rendre actif un role
SET DEFAULT ROLE 'monRole' TO 'test44'@'localhost';

# rendre actif tous les roles d un utilisateur
SET DEFAULT ROLE ALL TO 'test44'@'localhost';
```

## Les vues

Quand on fait une vue on fait un select dans une table virtuelle (un peut comme quand on fait une table avec une jointure, c'est uje table avec laquelle on ne peut pas interragir, elle n'est pas existante dans la base de donnée). On se sert d'une vue pour venir la chercher après sans avoir accès a la base de donnée, et sans voir ni les tables ni leurs structure. On cache la complexité de nos tables. On ne donne aucune information a la personne qui fait des requètes. La requètes est une seule fois et est stocké dans le cache. Dans après on l'appelle juste.

Le definer permet de savoir qui a crée la vue. Pour appeller la vue, on fait comme si on appellait une table. Personne ne peut modifier la vue, ni supprimer ni ajoute, uniquement SELECT. 

```sql
VIEW 'nom de la vue' AS
    SELECT
    FROM
```

## Les procédures stockées

Cela sert a faire une ou plusieurs requettes en une seule fois, et en s'assurant de ce que l'on récupère comme données. Cela permet de prévoir le comportement de certaines requètes. Les procédures stockées sont des fonctions, elles prenennt les arguments d'entrée. Les personnes qui font les requètes évitent alors de travailler directement sur les bases. Elles permettent aussi de gagner en performance. Elles ne gèrent pas les commit globales (uniquement auto-commit).

On les appelle via la commande CALL 

```sql
CALL beer.Add_Continent(ID, NOM)
```

Exemple :

```sql
CREATE DEFINER=`root`@`localhost` PROCEDURE `Add_Adresses`(IN num INT, IN rue VARCHAR(45), IN ville VARCHAR(45), IN cp INT)

BEGIN

INSERT INTO cabinet.adresses (num, rue, ville, cp) 
VALUES (num, rue, ville, cp);

SELECT *
FROM cabinet.adresses;

END
```

Ou même 

```sql
CREATE DEFINER=`root`@`localhost` PROCEDURE `Add_Adresses`(IN _num INT, IN _rue VARCHAR(45), IN _ville VARCHAR(45), IN _cp INT)
BEGIN

SET @verif = (SELECT idadresses FROM adresses WHERE `num` = _num AND `rue` = _rue AND `ville`= _ville AND `cp`= _cp);

IF (@verif = '0' OR @verif IS NULL)
THEN
INSERT INTO cabinet.adresses (num, rue, ville, cp) 
VALUES (_num, _rue, _ville, _cp);
END IF;

SELECT *
FROM cabinet.adresses;

END
```

## Les vérrous

Poser un vérrou fait que la session qui le pose ne peut plus accéder qu'aux tables sur lesquelles elle a posée ses verrous. Elle ne peut accéder a ces tables que par leur nom ou les alias qu'elle définie pour les appeler. En cas de vérrou de lecture elle ne peut que les consulter, pas les modifier. Pour vérouiller plusieurs tables ou colonnes il faudra le faire en une seule requete, les autres verrous seront retirés.

## Les transactions

On enregistre deux actions, puis on enregistre le resultat final. C'est ce résultat final qui va être envoyé sur le serveur. Tout d'abord il faut set l'autocommit a zero, puis le resset a 1 a la fin de la transaction.

```sql
# On retire l autocommit
SET autocommit = 0;

# On démare la transaction
START TRANSACTION;
#code;
#code;

#Valider le changement
COMMIT;

SET autocommit = 1;
```

Exemple :

```sql
SET autocommit = 0;
START TRANSACTION;
#Ajoute 100

UPDATE banque.compte
SET balance = 650
WHERE idcompte = 1;
#Retire 100

UPDATE banque.compte
SET balance = 899
WHERE idcompte = 2;

SELECT *
FROM banque.compte;

COMMIT;
```

On peut utiliser les savepoints comme point de sauvegarde en tandem avec le rollback

```sql
SAVEPOINT jalon;

ROLLBACK TO jalon;
```
