# Le HTML

C'est un langage de balisage, qui donne le squellette de page web. On met en place des balises. Le CSS, c'est un langage de style qui permet de rendre les pages plus jolies. Il faut voir l'HTML comme un arbre : une balise parente dans laquelle on inclut d'autres balises. C'est le DOM (Document Object Model), quand on parle d'une structure HTML on parle de DOM. Chaque élément de l'arbre est appellé un "node".

Un fichier HTML prend l'extension HTML. On met toujours en premier le DocType : il permet de définir la norme HTML a utiliser.

````html
<! DOCTYPE html>
````

Pour ouvrir une balise ouvrantes on fait :

```html
<NOM_DE_LA_BALISE>
```

```html
</NOM_DE_LA_BALISE>
```

Pour les input on peut utiliser des balises autofermantes :

```html
<BALISE/>
```

Entre la balise ouvrante et la balise fermante, le code va être interprété.

Après le doctype on ouvre la balise HTML qui est la plus élevée. A l'intérieur, on a la balise head ET body (en dessous, pas dedans). Si il y a des appels HTML il faut les mettre en fin de body.

Dans le HEAD on trouve les métadonnes. En premier la balise title et le charset pour dire quelle norme on utilise. Puis les link qui permettent d'ajouter des fichiers. Enfin, parmis les accessoires : l'auteur et la description.

Pour changer la petite icone on utilise la favicon.

*Charset est une clé, UTF-8 est une valeur, style.css est un fichier.*

```html
    <head>

        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
        <link rel="shortcut icon" href="assets/favicon/favicon.ico" type = "image/x-icon">
        <meta name="author" content = "Diwan Lefebvre">
        <!-- SEO : référencement du site-->
        <meta name = "description" content = "lorem...">
        <meta name = "keywords" content = "tag1, tag2, tag3">

        <title>
            Première page HTML.
        </title>

        <style>
            /* Ici on fait du CSS */
        </style>

        <script>
            // Script JS
        </script>

    </head>
```

Dans la balise body on a la structure de la page.
*Le span est utile pour mettre un élément dans une balise et le pimper après*

```html


```

![recap](recap.png)

## Le HTTPS

Le HTTP:// est un protocole de communication par le port 443. HTTPS a des méthodes, qui vont spécifier le type de requètes (une demande aux serveurs), une requète est composée d'un header (on retrouve la méthode utilisée, mais aussi qui l'a demandé) et d'un body (de contenu).

Il existe plusieurs types de méthode :

- Get : c'est la plus courante. Je demande une info au serveur qui va renvoie (une response) un résultat. Pas de body
- Post (création) : on envoie des données. Elle provoque une mise en db de ce qu'il y a dans le body. Il y a une response.
- Put/Patch (modification): on modifie un élement en db. Réponse du serveur (code HTTP).
- Delete : va venir supprimer. Réponse du serveur.

Les codes en 200 : OK, en 300 : redirection, 400 : erreur client, erreur 500 : serveur.