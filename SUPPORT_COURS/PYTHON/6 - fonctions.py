# déclaration d'une fonction

def carre():
    return "carre"


# appel de fonction
carre()


# renvoie de plusieurs objets
# ou plutot d'un objet de type tuple
def message():
    return ["Bonjour", "au revoir"]


# affectation des resultats à la volée
el1, el2 = message()

# valeurs par défaut


def alpha(x=1):
    return x

# comment choisir quel argument doit changer


def beta(a=1, b=2, c=3):
    return a + b + c


beta(c=8)

# dans le cas de mélange entre argument positionnel et par mot cle
# il faudra toujours renseigner les arguments positionnels en premier

# pour modifier une variable globale dans une fonction
g1 = 12


def rdg():
    global g1
    g1 = 15


rdg()

print(g1)
