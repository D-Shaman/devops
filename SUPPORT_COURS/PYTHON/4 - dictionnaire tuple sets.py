# dictionnaire
# pas d'indice
# acces via cle
dico = {}
dico['c1'] = 'jean'
dico['alpha'] = 24

# pour récupérer une valeur, on spécifie sa cle
dico['alpha']

# itération sur un dictionnaire
for key in dico:
    key, dico[key]

# il existe 3 methodes natives pour intéragir avec les dictionnaires
# .key renvoie toutes les clefs d'un dictionnaire
print(dico.keys())

# .values renvoie toutes les valeurs d'un dictionnaire
print(dico.values())

# .items revoie un ensemble de clé value
print(dico.items())

# pour savoir si une cle existe
if "test" in dico:
    "ok"

# les listes de dictionnaire
dico2 = [{"key1": "jean", "age": 24}, {"nom": "paul", "taille": 1.78}]

# il est possible de convertir une liste en dictionnaire via la fonction dict()
# attention, la liste devra etre un objet séquentiel contenant 2 autres objets séquentiels de 2 elements pour respecter la syntaxe cle value

maListe = [["animal", "lion"], ["arbre", "bouleau"]]
dict(maListe)

# tuples
# ressemblent aux listes mais sont non modifiables
x = (1, 2, 3)

# il est possible d'ajouter des elements en concatenant 2 tuples
x = x + (4,)
print(x)

# on peut caster en tuple une séquence
l = "ceci est un test"
print(tuple(l))

# les sets
# comme les tuples mais sont unique
s = {1, 2, 3, 4, 5}

# cast d'une séquence en set
set([1, 2, 3])
