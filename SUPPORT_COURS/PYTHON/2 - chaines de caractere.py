# les chaines de caractère peuvent être traité comme des listes particulières
# les méthodes liées aux listes peuvent donc s'appliquer

message = "ceci est un message à tester"

# obtenir sa taille
len(message)

# utiliser la propriété tranche
message[0: 5]

# a la différence des listes, les chaines de caractères ne sont pas modifiable

# méthodes spécifiques au type str
# tout passer en majuscule
message.upper()

# tout passer en minuscule
message.lower()

# passer la premier lettre en majuscule
message.capitalize()

# découper une chaine de caractère
message.split()

# on peut définir le nombre de fois que l'on souhaite découper la chaine de caractère
message.split(maxsplit=3)

# obtenir l'emplacement d'une chaine de caractère passée en argument
# seul la première occurence est renvoyée
message.find("message")

# remplacer des caratères par d'autres
message.replace("e", "z")

# compter le nombre d'occurence dans une chaine de caractère
message.count("z")

# vérifier qu'une chaine de caractere commence par la chaine rentrée en paramètre
message.startswith("ce")

# supprimer les espaces superflue en debut et fin de chaine de caractère
message.strip()

# comment extraire les valeurs numérique d'une chaine de caractère
# on commence par tout split
# on cast les nombres

message2 = "nous voulons additionner 2 et 5"
calc = message2.split()
print(calc)
int(calc[3]) + int(calc[5])

# conversion d'une liste de chaine de caractère en chaine de caractère
conc = message2.split()
print("/".join(conc))
