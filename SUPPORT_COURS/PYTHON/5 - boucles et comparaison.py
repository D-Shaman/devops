villes = ["Paris", "Marseille", "Lille", "Bordeaux"]

# la boucle for
for ville in villes:
    ville

for i in range(5):
    i

# enumerate() pour récupérer l'indice en plus de l'element
for i, ville in enumerate(villes):
    i, ville

# la boucle while
i = 0
while i <= 4:
    print(i)
    i += 1

# la condition if
j = 0
if j == 0:
    print(j)

# test sur un cas
j = 5
if j == 0:
    print("ok")
else:
    print("pas ok")

# test sur plusieurs cas
j = 4
if j == 0:
    print("i = 0")
elif j == 5:
    print("i = 5")
else:
    print("i != 0 ou 5")

# les test multiples
if j < 10 and j > 0:
    print("j entre 0 et 10")

if 10 > j > 0:
    print("methode 2 j entre 0 et 10")

# beak et continue
for i in range(5):
    if i > 2:
        break
    print(i)

for i in range(5):
    if i == 2:
        continue
    print(i)

# comparaisons flottant
# une valeur flottante étant stockée de manière arrondie
# il faut passer par un delta et vérifier que la valeur à mesurer est comprise dans un intervalle comprenant ce delta
# deux solutions :
delta = 0.0001
nombre = 3.0 - 2.7

# solution comparative simple
print(0.3 - delta < nombre < 0.3 + delta)

# solution via la méthode abs qui ira chercher la valeur absolue
print(abs(nombre - 0.3) < delta)
