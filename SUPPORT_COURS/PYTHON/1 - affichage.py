from math import *
import math

# afficher dans la console
print()

# pour retirer les retours de ligne apres chaque appel de la fonction, on passe en argument le mot clé end

print("hello")
print("bonjour", end=" ")
print("michel")

# pour afficher plusieurs variables
print("Bonjour", 12, True)

# pour séparer les différents éléments selon notre volonté, on passe par le mot clé sep
print("Bonjour", 12, True, sep="/")

# formater notre écriture avec la méthode format
print("{} a {} ans".format("michel", 45))

# préciser le format
a = math.pi
b = 10

print(
    "nous récupérons la valeur de {:.2f} avec deux chiffres apres la virgule".format(a))


# il est possible de définir la position des valeurs dans le format

print("cette {0:} est une {0:} bleue adossée à la {1:}".format(
    "maison", "colline"))

print("formater en décimal : {:d}".format(b))

c = 100000000

# préciser le nombre de caractère pour un résultat et spécifier l'alignement
print("{:.>6}".format(c))
print("{:.<6}".format(c))
print("{:.^6}".format(c))

# fonctionne aussi avec les str
d = "ceci est un message qui va disparaitre"
print("{:.>100}".format(d))
