# déclaration de variables
# sensible à la casse
a = 1
b = "Bonjour"
c = True
d = 1.45
e = 1e6

# On peut séparer les groupes de nombre pour une meilleure visibilité avec _
f = 124_48_7411_89

# Les opérations
g = 15
h = 30.5

g += 2
h += 5

# le mélange de type float et entier renvoie un float
# les divisions renvoient systématiquement un float
# les puissances s'écrivent avec un **

i = 5**2

# reste d'une division
j = h % g

# quotient d'une division
k = h // g

# opération sur les chaînes de caractères
l = "Message 1"
m = "Message 2"

# addition
n = l + m

# multiplication
o = l*3

# récupérer le type d'une variable
type(g)

# conversion de type (cast)
# str() change le type en string
# float() change le type en float
# int() change le type en entier
p = 10
p = str(p)

# récupérer les min et max d'un ensemble de valeur
# pour les str, il fera la comparaison entre le premier caractère de chaque élément
min(10, 45, 7, 1, 89)
max('dlqsnd', 'Azerty', 'aou')

# une variable locale est définie dans une fonction
# un variable globale est définie en dehors d'une fonction
