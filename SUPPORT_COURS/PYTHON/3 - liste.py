# les listes
import copy
liste = ["bonjour", 12, True, 45.14]
liste.append('coucou')
print(liste)

# récupérer via l'index
liste[0]

# opérations sur liste
# concaténer
liste2 = ["triste", 14, False]
liste3= liste + liste2

print(liste3)

# multiplier
liste*3

# ajouter des elements à notre liste
liste += [15]
liste.append(30)

# les indices négatifs
# a la différence des indices positifs qui vont de gauche a droite, les négatifs vont de droite à gauche
# tres utile pour récupérer le dernier element d'une liste sans connaitre sa taille
liste[-1]

# Les tranches
# on peut récupérer une partie de notre tableau via les tranches
# on spécifie l'indice du premier element à selectionner et le nombre d'element à extraire
liste[0:3]

# il est possible de préciser le pas
liste1= list(range(15))
print(liste1[0:8:2])

# connaitre la longueur d'une liste
len(liste)

# generation de nombre entier compris dans un interval
range(10)

# avec la fonction liste, génération d'une liste d'entier ici de 0 à 9
list(range(10))

# arguments
azer = list(range(10, 45))
azer.reverse()
print(azer)


# avec un pas
list(range(10, 45, 5))

# décroissant
list(range(10, 0, -1))

# les listes imbriquées
enclos = ['girafe', 4]
enclos2 = ['tigre', 2]
enclos3 = ['singe', 5]
zoo = [enclos, enclos2, enclos3]

# accéder à un élement de la sous liste
zoo[0][0]

# min() pour un tableau
tab = list(range(0, 10))
min(tab)

# max() pour un tableau
max(tab)

# sum() pour un tableau
sum(tab)

# insérer un objet à un endroit spécifique
liste.insert(1, "insertion depuis insert")

# suppression d'un objet d'une liste depuis sa position
del liste[2]

# suppresion d'un objet d'une liste depuis sa valeur
liste.remove(True)

# trie d'une liste
numlist = [12, 14, 87, 0, 1, ]
numlist.sort()

strlist = ["ok", "merci", "aplha", "beta", "Aazzer"]
strlist.sort()

# inverser une liste
strlist.reverse()

# connaitre le nombre d'élement specifique dans une liste
strlist.count("merci")

# convertir une chaine de caractere en liste
seq = "message à convertir"
print(list(seq))

# savoir si un element fait parti d'une liste
"ok" in strlist

# les tableaux fonctionnent par référence
# pour copier les valeurs d'une liste et non sa référence mémoire on passe par [:]
# ne marche pas pour les listes a plusieurs dimmensions
tab = [1, 2, 3]
tab2 = tab[:]
# méthode qui marche à tout les coups

tab3 = [[1, 2, 3][1, 2, 3]]
tab4 = copy.deepcopy(tab3)
