# Info diverses Linux

Décomposition de la première ligne de l'invité de commande :

```shell
user@localhost:~#
```

- User : c'est le pseudonyme de l'utilisateur.
- @ : signifie "chez"
- localhost : nom de la session en cours, ou de la machine.
- : : séparateur
- ~ : le dossier dans lequel on se trouve actuellement. Ici le dossier home.
- $ : C'est le symbole qui fait référence au niveau d'autorisation sur la machine.
  - $ signifie un compte user aux droits limités
  - '#' signifie superuser (root) avec tous les privilèges.

Quand on fait un ls, la colonne suivant les autorisation représente le nombre de fichiers qui partagent le même inode (numero d'identification)

## Commandes utiles

```shell
# Renvoie la date et l'heure
date
```

```shell
# cat -n permet d'afficher des numerots de ligne dans le fichier qui est caté
cat -n
```

```shell
#less permet d'afficher un long fichier page par page
less /var/log/messages
```

## Racourcis dans le shell

CTRL + A : ramenne le curseur au début de la commande.
CTRL + E : amène le curseur a la fin de la commande.
CTRL + U : supprime tout ce qui est a gauche du curseur.
CTRL + K : supprime tout ce qui est a droite du curseur.

## Les dossiers principaux

- bin : contient les exécutables utilisés par tous les utilisateurs de la machine.
- boot : fichiers permettant le démarage de Linux.
- dev : dossier qui contient les périphériques.
- etc : fichier de configuration.
- home : répertoire des utilisateurs (symbolisé par le ~)
- lib: contient les bibliothèques partagées utilisées par les programmes (équivalent des .dll de Windows)
- sbin : contient des programmes system important.
- tmp : dossier temporaire utilisé par les programme. Il est flush au reboot.

![arbo.png](arbo.png)

## Chemins relatifs et absolu

Un chemin relatif est un chemin qui dépend du dossier dans lequel je me trouve actuellement.
Le chemin absolue dépend de la racine et commence toujours par '/'.

## Liens physique et lien symbolique

Le lien physique permet d'avoir deux noms de fichiers qui partagent totalement le même contenu. Cette méthode fait pointer les deux fichiers vers le même inode.

Pour ce faire :

```shell
# On crée le premier fichier, puis on lui fait un lien physique avec un second fichier qui va être crée précédemment dans l'arborescence
echo "blablabla" > test.txt && ln test.txt ../plink.txt
```

Le lien synbolique est une forme de racourcis. Le fichier linké pointe donc vers le fichier original et non pas vers son inode.

Pour ce faire:

```shell
# On crée le fichier test2, puis on lui fait un lien symbolibre avec un second fichier qui va être crée dans le même dossier.
echo "bliblibli" test2.txt && ln -s slinktest2

# Dans le ls -l, on observe que le premier symbole des autorisation est un l, qui représente un lien symbolique.
```

## Les utilisateurs

On peut ajouter un utilisateur, et le supprimer via deux commandes :

```shell
#Ajouter un utilisateur, et on set son password a 1234
sudo useradd -p 1234
#Supprimer un utilisateur :
sudo userdel
```

Pour afficher les utilisateurs on va chercher dans le fichier /etc/passwd, via un grep :

```shell
#On affiche le contenu de etc/passwd puis on pipe le resultat dans un grep qui va chercher une chaine de caractères précise.
cat etc/passwd | grep nom_du_user
```

## Les groupes

Chaque utilisateur appartient a un groupe. On peut voir a quel user/group appartient un fichier dans le ls. La 3ème colone est l'user auquel le fichier appartient et la 4ème est le group auquel il appartient.

Pour créer un groupe on utilise la commande :

```shell
#Créer un groupe
sudo groupadd
#Supprimer un groupe
sudo groupdel
#Voir les groupes
cat /etc/group
```

Pour attribuer un user a un groupe on utilise le commande usermod (-l pour renommer l'utilisateur, -g pour attribuer un utilisateur a un groupe) :

```shell
#Pour attribuer un groupe a un user
usermod -g group user
#Pour attribuer plusieurs groupe a un utilisateur
usermod -G user group1 group2 group3
```

Pour attribuer un fichier a un utilisateur on utilise la commande chown.

```shell
#On attribue le dossier et tous les fichiers  a l'intérieur a user
chown -R user dossier
```

Pour attribuer un fichier a un groupe, on utilise le commande chgrp :

```shell
#On attribue le dossier et tous les fichiers  a l'intérieur au group
chgrp -R group dossier
```

### Les droits

Le premier triplet indique les autorisations du PROPRIETAIRE DU FICHIER, le second le groupe, le 3ème les autres (donc hors propriétaire / groupe propriétaire) :

- r : read
- w : write
- x : execute

On modifie les droits via la commande chmod. De la manière suivante :

```shell
# On attribue les droit d'exécution du dossier et de tout ce qu'il contient a l'utilisateur
sudo chmod -r u+x dossier
# On attribue les droits d'écriture du fichier au group
sudo chmod g+w fichier
# On retire les droits de lecture du fichier aux "autres"
sudo chmod o-r fichier
```
