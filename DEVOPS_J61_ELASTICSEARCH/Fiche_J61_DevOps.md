Les shard permettent de gagner en vitesse sur l'indexation. Elles sont de base au nombre de 1 et fonctionnent avec les replica pour avoir des recherches plus rapide.

Il faut définir le mapping avant pour les types. On ne peut pas modifier le mapping, il faut tout supprimer. C'est fortement indexé donc quand on fait de l'insertion c'est beaucoup plus long mais la récupération est beaucoup plus rapide.

    VERBE + INDEX + URL + QUERY

Le score correspond a la pertinance de la donnée. Utuliser un filtre est plus rapide parcequ'on ne prend pas en compte le score.

Bulk est une API qui permet de rentrer des gros volumes de donnée, il faut rentrer l'index avant chaque donnée. Logstash : va chercher des log, les met en json, et les envoie a elasticsearch.

## API REST

C'est un principe architectural, qui fait la liaison entre le client et le serveur.
On utilise des verbes : GET/POST/PUT/DELETE/PATCH.
C'est différent d'une API SOAP. REST permet plus de sécurité et la communication entre plusieurs langages.