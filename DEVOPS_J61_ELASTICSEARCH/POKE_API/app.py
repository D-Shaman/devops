from flask import Flask, request, render_template, redirect, url_for, Blueprint, session
import requests
from elasticsearch import Elasticsearch
from db import Connect

es = Elasticsearch("http://elastic:changeme@localhost:9200")
app = Flask(__name__, static_folder="templates/assets")
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['DEBUG'] = True
conn = Connect.log()

@app.route("/")
def home():
    return render_template('home2.html')

@app.route("/pokemons/<int:id>")
def pokemons(id):
    res = es.get(index="pokedex", id=id)
    print(type(res))
    return render_template('home.html', res=res)

@app.route("/name/<name>")
def name(name):
    search_params = {
    "query":{
        "query_string":{
        "query": name
        }
    }
    }
    res = es.search(index="pokedex", body=search_params)
    res = res["hits"]["hits"][0]
    id_pokemon = int(res.get("_id"))
    data = sql_query(id_pokemon)
    return data

@app.route("/cul", methods=['POST', 'GET'])
def cul():
    return (render_template('cul.html'))

@app.route("/rendering", methods=['POST'])
def render():
    data = request.form
    cul = data.get('name')
    cul2 = name(cul)
    print(cul2)
    return render_template('home.html', data=cul2)


def sql_query(id_pokemon):
    conn.execute(f'''
    SELECT *
    FROM pokemon
    WHERE id = {id_pokemon}
    ''')
    rows = conn.fetchall()
    return rows
