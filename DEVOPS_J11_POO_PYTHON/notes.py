# # Première création de classe : la voiture
# # Ligne 2 : le constructeur, quand j'instancie ma classe (v1)
# # avancer et freiner son les méthodes
# # couleur et carburant sont des attributs
# # v1 est une instance
# # Une fois que v1 est instancié, alors on peut appeller.


# class voiture:
#     def __init__(self, carburant, couleur):
#         self.set_couleur(couleur)
#         self.set_carburant(carburant)

#     def set_couleur(self, couleur):
#             self._couleur = couleur

#     def get_couleur(self):
#         return self._couleur
    
#     def get_carburant(self):
#         return self._carburant

#     def set_carburant(self, carburant):
#         self._carburant = carburant
    
#     def avancer(self):
#         return "la voiture roule"
#     def freiner(self):
#         return "la voiture freine"

#     def message(self):
#         print(self.avancer())
#         print(self.freiner())


# v1 = voiture("Diesel", "verte")
# v1.message()
# print(v1.get_couleur())
# print(v1.get_carburant())

# #Récupérer attributs et valeurs
# print(v1.__dict__)

class Test:

    def __init__(self, nom, prenom):
        self.set_nom(nom)
        self.set_prenom(prenom)

    def set_nom(self, nom):
        self._nom = nom

    def get_nom(self):
        return self._nom

    def set_prenom(self, prenom):
        self._prenom = prenom

    def get_prenom(self):
        return self._prenom
    
    def message(self):
        print(f"Le nom est {self.get_nom()}, et le prénom est {self.get_prenom()}")



instance = Test("Oxlong", "Mike")
instance.message()
