class Animal:
    def __init__(self, races, nom):
        self.set_races(races)
        self.set_nom(nom)
    
    def set_pattes(self, nbr_pattes):
        self.__pattes = nbr_pattes
    
    def get_pattes(self):
        return self.__pattes
    
    def set_races(self, races):
        self.__races = races
    
    def get_races(self):
        return self.__races

animal1 = Animal("Labrador", "Rex")
print(animal1.get_races(), animal1.get)
