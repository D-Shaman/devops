# L'objet et la programmation orientée objet

C'est un paradigme qui se différcie de la programmation orienté fonctionnel (ou procédural). On s'assure que le code soit plus lisible, plus facile a maintenir...

Dans le procédural, si il y a un soucis sur une ligne de code, alors il faut chercher partout pour la trouver. C'est un genre de code sur lequel il est difficile de faire des modifications. De plus la lisibilité est difficile. La programmation procédurale est encore utile pour les petits bouts de code...

La programmation orientée objet rêgle (en un sens ce genre de problèmes). On préfère l'utiliser pour tous les projets de moyenne ou grande taille.

Les objets sont des éléments qui existent dans le réel. C'est une case qui contient de éléments : des atributs (un peu comme des variables), et des méthodes (des fonctions).

Par exemple : la voiture

Cette voiture a des caarctéristiques : la couleur, le nombre de portes... Ce sont **ses attributs** c'est ce qui la caractérise en temps que voiture, c'est ce qui fait que c'est une voiture. Pour sa méthode par exemple, on a le fait de rouler et de freiner (donc ses actions).

Un objet est donc un conteneur qui possède des attributs et des méthodes.

On peut donc ajouter des valeurs aux attributs, par exemple, couleur = rouge, nombre de portes = 5. Il en va de même pour les méthodes, on peut leurs donner des instructions.

## L'encapsulation

Encapsulation : on s'assure que la variable correspond a ce que j'attend. Par exemple, la variable couleur = rouge. Donc le développeur s'attend donc a une string. Sans encapsulation, on pourraut envoyer un int alors que l'objet attend un str.

L'encapsulation est un ensemble de 2 méthodes : un getter et un setter. Par exemple getter_couleur, qui est une fonction qui va retourner couleur. le setter dit que l'attribut prend cette valeur en particulier et que c'est bien le type qu'on attend.

Dans le setter il y a deux éléments a voir. On envoie un if, si v est de type str => alors couleur = v (on s'assure qu'une chaine de char est rentrée, la couleur = 1 est donc impossible).

En objet on ne passe jamais dirrectement la variable. On vient interroger le getter et le setter.

## L'héritage

Il a plusieurs intérets : éviter la duplication de code (c'est une mauvaise pratique) et permettre une bonne structuration depuis la classe parent vers la classe enfant.

On a une classe mère, qui a des attributs et des méthodes. La classe fille va en hériter. Les attributs et méthodes chez les parents sont héritées chez l'enfant et n'ont pas besoin de réécrire. Par exemple l'attribut couleur. De même quand une méthode est définie chez un parent, elle l'est chez l'enfant.
Cela se passe de la manière suivant (comme une fonction) : enfant(parent).

Par exemple, on a une classe parent : les animaux. On a dans cette classe des attributs et des méthode. Par exemple les attributs "poils" et "pattes", et une méthode "marcher".

On fait hériter cette classe a deux autres classes : chat et chien. Ce qui est défini dans la classe animaux, je n'ai pas besoin de le redéfinir dans chat et chien. MAIS je peut définir de nouveaux attributs dans chaque héritant.

## Le polymorphisme

C'est une notion POO qui n'existe pas en Python.

C'est quand on a une méthode (par exemple marcher), qui appartient a une classe. La manière dont est traitée cette méthode est différente pour les classes filles.

On va aller rechercher la méthode chez la classe mère et la modifier dans les classes filles (par exemple, courrir ou ramper).

## La classe

La classe est le "moule" de l'objet. On la défini par class, puis on lui donne un nom. Puis on instancie la variable. On définie ensuite les attribue qu'on va lui passer (dans les paramètres). On passe ensuite par un constructeur qui se déclare par init(couleur). Puis on passe a l'encapsulation.

Les instances sont les résultats du passage par la classe.

En python, on a le self, qui doit être défini au moment de l'init. Quand on utilise SELF (on travaille sur l'instance et permet que chaque instance soit différente).

On défini la classe, les attributs, on init la classe. On recommande de créer un fichier par classe.

```py
# Première création de classe : la voiture
# Ligne 2 : le constructeur, quand j'instancie ma classe (v1)
# avancer et freiner son les méthodes
# couleur et carburant sont des attributs
# v1 est une instance
# Une fois que v1 est instancié, alors on peut appeller.


class voiture:
    def __init__(self, carburant, couleur):
        self.set_couleur(couleur)
        self.set_carburant(carburant)

    def get_couleur(self):
        return self._couleur
    
    def set_couleur(self, couleur):
        self._couleur = couleur

    def get_carburant(self):
        return self._carburant

    def set_carburant(self, carburant):
        self._carburant = carburant
    
    def avancer(self):
        return "la voiture roule"
    def freiner(self):
        return "la voiture freine"

    def message(self):
        print(self.avancer())
        print(self.freiner())


v1 = voiture("Diesel", "verte")
v1.message()
print(v1.get_couleur())
print(v1.get_carburant())
```

### Vocabulaire

Instancier une classe c'est créer un objet.
Self, fait référence à l'objet en cours.
Les attributs sont dynamiques, ils sont créer quand on en a besoin.
Setter : défini un attribut en concordance avec l'état attendu (âge > 0 et < 150 par exemple>).
Getter : afficher l'attribut
Attribut : variable de classe
Méthode : fonction de classe

```py
class Test:

    def __init__(self, nom, prenom):
        self.set_nom(nom)
        self.set_prenom(prenom)

    def set_nom(self, nom):
        self._nom = nom

    def get_nom(self):
        return self._nom

    def set_prenom(self, prenom):
        self._prenom = prenom

    def get_prenom(self):
        return self._prenom
    
    def message(self):
        print(f"Le nom est {self.get_nom()}, et le prénom est {self.get_prenom()}")
```

## La visibilité 

On peut simuler la visibilité privée avec double underscore : __. C'est a dire que sans getter, il est impossible de d'accéder directement a l'atribut. Point important, quand on écrit une méthode, il faut la documenter, c'est a dire écrire en commentaire ce qu'elle fait.

## Pour l'héritage

On peut faire dans le fichier 1 :

```py
class animal:
    def __init__(self, races,nom):
        self.set_races(races)
        self.nom(nom)
    
    def set_pattes(self, nbr_pattes):
        self.__pattes = nbr_pattes
    
    def get_pattes(self):
        return self.__pattes
    
    def set_races(self, races):
        self.__races = races
    
    def get_races(self):
        return self.__races

animal1 = animal("Labrador")
print(animal1.get_races())
```

Dans un second fichier, faire un héritage :

```py
from DEVOPS_J11_POO_PYTHON.ANIMAUX.animal import animal


class Chien(animal):
    def __init__(self, races):
        super().__init__(races) #HERITAGE DU CONSTRUCTEUR INIT 
```

Quand on hérite, on hérite aussi du getter du parent.

On peut hériter d'un attribut publique. Quand l'attribut est privé, alors il faut passé par le getter du parent.

## La composition

Ce n'est pas vraiment un héritage. C'est le fait d'atribuer a une méthode, un paramètre qui est une classe.