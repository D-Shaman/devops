class Somme:
    def __init__(self, nbr1, nbr2):
        self.set_nbr1(int(nbr1))
        self.set_nbr2(int(nbr2))
        self.calculation()
        self.message()
    
    def set_nbr1(self, nbr1):
        self._nbr1 = nbr1
    def get_nbr1(self):
        return self._nbr1
    
    def set_nbr2(self, nbr2):
        self._nbr2 = nbr2
    def get_nbr2(self):
        return self._nbr2

    def calculation(self):
        self._sum = self._nbr1 + self._nbr2
        return self._sum

    def message(self):
        print(f"La somme de {self.get_nbr1()} et de {self.get_nbr2()} est {self.calculation()}")
