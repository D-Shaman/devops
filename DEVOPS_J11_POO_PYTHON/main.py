from Exo2_1 import Rectangle
from Exo2_2 import Somme
from Exo2_3 import Student
from Exo2_4 import Student2

#Exo2_1.py : Rectangle
rectangle1 = Rectangle(input("Longueur ? \n"), input("Largeur ? \n"))

#Exo2_2.py : Somme
calcul1 = Somme(input("Quel est vôtre premier nombre ?\n"), input("Quel est le second nombre ?\n"))

#Exo2_3 : Student
étudiant1 = Student(
    input("Quel est vôtre nom ?\n"),
    input("Quelle est vôtre première note ?\n"),
    input("Quelle est vôtre seconde note ?\n")
)

#Exo2_4 : Student2
def get_data():
    nom = input("Quel est vôtre nom ?\n")
    notes = []
    i = 0
    nbr_notes = input("Combien de notes souhaitez vous rentrer ?\n")
    while i < int(nbr_notes):
        new_note = int((input("Quelle est la note ?\n")))
        notes.append(new_note)
        i += 1
    return nom, notes

nom, notes = get_data()
student1 = Student2(nom, notes)