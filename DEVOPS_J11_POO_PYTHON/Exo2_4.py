from statistics import mean

# def get_data():
#     nom = input("Quel est vôtre nom ?\n")
#     notes = []
#     i = 0
#     nbr_notes = input("Combien de notes souhaitez vous rentrer ?\n")
#     while i < int(nbr_notes):
#         new_note = int((input("Quelle est la note ?\n")))
#         notes.append(new_note)
#         i += 1
#     return nom, notes

class Student2:
    def __init__(self, nom, notes):
        self.set_nom(nom)
        self.set_notes(notes)
        self.calculation()
        self.display()
    
    def set_nom(self, nom):
        self._nom = nom
    def get_nom(self):
        return self._nom
    
    def set_notes(self, notes):
        self._notes = notes
    def get_notes(self):
        return self._notes
    
    def calculation(self):
        self._moyo = mean(self._notes)
        return self._moyo 
    
    def display(self):
        print(f"Pour l'élève {self.get_nom()}, la moyenne est de {self.calculation()}")

# nom, notes = get_data()
# student1 = Student2(nom, notes)