class Student:
    def __init__(self, nom, note1, note2):
        self.set_nom(nom)
        self.set_note1(int(note1))
        self.set_note2(int(note2))
        self.moyenne()
        self.display()
    
    def set_nom(self, nom):
        self._nom = nom
    def get_nom(self):
        return self._nom
    
    def set_note1(self, note1):
        self._note1 = note1
    def get_note1(self):
        return self._note1

    def set_note2(self, note2):
        self._note2 = note2
    def get_note1(self):
        return self._note2

    def moyenne (self):
        self._moyenne = (self._note1 + self._note2)//2
        return float(self._moyenne)
    
    def display(self):
        print(f"Pour l'étudiant {self.get_nom()}, la moyenne est de {self.moyenne()}")

