#Créer un tyramisu, l'utilisateur instance les proportions, avec les etter getter. A la fin s'affiche un message qui regroupe l'intégralité des étapes

class Tiramisu:
    def __init__(self, sucre, vanille, biscuit, oeufs, mascarpone, café, cacao):
        self.set_sucre(sucre)
        self.set_vanille(vanille)
        self.set_biscuit(biscuit)
        self.set_oeufs(oeufs)
        self.set_mascarpone(mascarpone)
        self.set_café(café)
        self.set_cacao(cacao)
    
    #Attributs

    def set_sucre(self, sucre):
        self._sucre = sucre

    def get_sucre(self):
        return self._sucre
    
    def set_vanille(self, vanille):
        self._vanille = vanille

    def get_vanille(self):
        return self._vanille

    def set_biscuit(self, biscuit):
        self._biscuit = biscuit

    def get_biscuit(self):
        return self._biscuit
    
    def set_oeufs(self, oeufs):
        self._oeufs = oeufs
    
    def get_oeufs(self):
        return self._oeufs

    def set_mascarpone(self, mascarpone):
        self._mascarpone = mascarpone

    def get_mascarpone(self):
        return self._mascarpone
    
    def set_café(self, café):
        self._café = café

    def get_café(self):
        return self._café
    
    def set_cacao(self, cacao):
        self._cacao = cacao

    def get_cacao(self):
        return self._cacao
    
    def message(self):
        print(f"Séparer les {self.get_oeufs()} oeuf, puis mélanger les {self.get_oeufs()} jaunes aux grammes de sucre {self.get_sucre()}, ensuite ajoutez les {self.get_mascarpone()} grammes de mascarpone\
, poursuivez en montant les {self.get_oeufs()} blancs en neige et incorporez au mélange précédent. Ensuite mouillez les biscuit dans les {self.get_café()} ml de café. Ajoutez la mascarpone par couche, \
puis saupoudrez des {self.get_cacao()} grammes de cacao")


tiramisu1 = Tiramisu(100, 2, 15, 3, 75, 100, 30)
tiramisu1.message()
