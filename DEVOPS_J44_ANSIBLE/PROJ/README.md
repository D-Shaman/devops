# **Ansible**

## **Description**

Lab pour apprendre Ansible.

Comporte :

- un conteneur avec Ansible
- des noeuds Centos et Debian

## **Prérequis**

1. docker
2. docker-compose
3. VScode ou autre

## **Extensions VScode**

1. [plugin docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
2. [plugin redhat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
3. [plugin ansible](https://marketplace.visualstudio.com/items?itemName=zbr.vscode-ansible)

## **Découpage du projet**

- docker-compose.override.yml : contient le réseau et le conteneur avec Ansible
- docker-compose.yml : contient les noeuds
- .env : contient les versions

## **Notes**

Documentations :

- [Rappel commandes docker](https://dockerlabs.collabnix.com/docker/cheatsheet/)
- [Rappel commandes docker-compose](https://devhints.io/docker-compose)
- [Réseaux Docker](https://blog.alphorm.com/reseau-docker-partie-1-bridge/)
- [Documentation Jinja 3](https://jinja.palletsprojects.com/en/3.0.x/)

Ansible :

- [Le fichier ansible.cfg peut être déplacé](https://stackoverflow.com/a/35969858/8502023)

## Explications

Ici l'infra contient Ubuntu avec ansible installé. Et un autre Ubuntu avec node (node 1). Ces deux machines font parti du même réseau.

1) test de connectivité :

```shell
ping :

ping node1
ping 10.12.0.11
```

En utilisant Ansible :

ansible -m ping node1
ansible -m ping 10.12.0.11
ansible -m ping all

ssh :
ssh test@node1 (mdp test)

Générer une clé ssh :
ssh-keygen (trouvé dans ~ avec ls -a)

Envoyer la clé publique au serveur :
ssh-copy-id -i .ssh/id_rsa.pub test@node1

ansible node -i inventory -m ping

espace libre sur les nodes :
ansible node1 -i inventory -a "df -h"

ansible node1 -i inventory -m setup

ansible node1 -i inventory -m debug

ansible tous -i home/ansible/exercices/exemple1/inventaires/inventory -a "df -h" -f 1 (une seule tâche a la fois)

ansible node2 -i /home/ansible/exercices/exemple1/inventaires/infra.yml -m ping

ansible app -i /home/ansible/exercices/exemple1/inventaires/inventory -m ping

ansible apache -i inventory -b -m apt -a name=apache2 --ask-become-pass

ansible -i inventory -b -m service -a "name=apache2 state=started enabled=yes" apache --ask-become-pass

ansible -i inventory -b -m copy -a "src=index.html owner=www-data group=www-data mode=644 dest=/var/www/html" apache -K

ansible-playbook playbook_ssh.yml