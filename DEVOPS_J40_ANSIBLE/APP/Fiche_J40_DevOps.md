# Rappel DOCKER

Docker permet d'installer et de configurer une application très rapidement. La contenerisation permet de limiter l'accès aux ressources (RAM, usage CPU, répertoires OS, les utilisateurs, le réseau).

Dans un conteneur par exemple, Flask, il ne peut pas communiquer avec les répertoires extérieurs (c'est comme un zip). Et les logiciels qui sont a l'extérieur ne peuvent pas accéder a l'intérieur du conteneur.

Pour fonctionner, flaks a besoin de python mais ne peut pas accéder a celui de la machine. Il faut donc installer Python dans le conteneur. Pour se connecter dans le conteneur il faut un Shell (quand je suis dans le conteneur je ne peut communiquer qu'avec les programmes qui sont dans le conteneur). On créer un répertoire bin, dans lequel on met les programme que l'on souhaite avoir dans le conteneur : bash, zsh (et les binaires spécifiques ls mkdir, apt install, etc...). Le répertoire etc est utilisé pour la configuration des logiciel. var stocke des fichiers créer par des applications comme les logs, etc...

```shell
docker run -ti debian bash
```

Avec cette commande on rentre dans le conteneur via un shell, et on lance un bash. Quand on lance le conteneur il faut faire un apt update et upgrade.

Docker ne rajoute pas de couche supplémentaire, il ajoute juste les éléments dans un répertoire commun.

Avec les systèmes de conteneur on peut déterminer l'usage CPU, la RAM maximum (C'est le logiciel CGROUP).

Dans le conteneur, on va créer un utilisateur root qui n'aura le droit de modifier que l'intérieur du conteneur (c'est le logiciel NAMESPACE).

Quand j'écris un Dockerfile je décris l'architecture et l'installation que je veux (FROM... RUN... ENTRYPOINT... CMD...). L'entrypoint force les gens a run ce qu'il y a derrière, le CMD devient alors un argument du entrypoint.

__________

## **Description**

Exemple de mise en pratique d'un Docker compose avec des réseaux.

## **Prérequis**

- Docker
- docker-compose
- VSCode
- Internet sans limitation

## **Démarrage**

## **Notes**

[google] (<https://google.com>)

__________

## docker-compose

Il execute des commandes terminale de Docker. Il permet de démarer x conteneur en même temps.

Un service est un conteneur + un réseau. Un service est un "conteneur +". Il est utile pour de l'applicatif, du web, du réseau. Chaque conteneur va avoir une IP, ils vont être inscrit dans un réseau. De la même manière, les conteneurs du même réseau peuvent communiquer.

Connaitre l'IP du conteneur :

```shell
# Voir les services actifs
docker-compose ps
# Voir le fonctionnement interne du conteneur. Possible de visualiser dans l'extension docker de VSCode.
docker inspect <id du conteneur>
# Supprimer le conteneur
docker stop
docker rm <id du conteneur>
```

Le service a un DNS (sous forme clé-valeur). Un serveur DNS est une base de donnée faisant le lien entre l'IP et un domaine.. Et tout les alias sont visible dans la partie Network. Il est bon de nommer un bdd en ".bdd" par exemple "mongo.bdd".

## Créer un réseau

Il est possible de le faire en ligne de commande, mais c'est déconseillé.

Il est mieux de le mettre dans la partie network du docker-compose.

Rappel : deux sorties pour un logiciel :

- La sortie standard qui renvoie le chiffre 0 pour dire que tout s'est bien passé (stdout).
- Quand il y a une erreur, un chiffre autre que 0 est renvoyé. C'est en général un code erreur.

## Les images

### Les versions

La version d'un logiciel : majeure.mineur.patch
Donc par exemple : 2.4.3.

- Majeur représente une grosse modification possible
- Mineur : ajouts ou suppression de fonctionnalités
- Patch : les correctifs.

### Les volumes

On synchronise un répertoire du conteneur, avec un autre répertoire hors du conteneur. Par exemple, le répertoire var qui en général contient les db dans le conteneur, est synchronisé avec un répertoire sauvegarde dans mon windows.

## Notes

Se connecter a la db mongo :

mongo admin -u root -p 'example'
db.movie.insert({"name":"tutorials point"})