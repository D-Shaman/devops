# DevOPS_J2

## Les caractères invisibles

Il faut éviter de copier-coller du code depuis internet. Déjà pour la mémoire musculaire, mais aussi pour les caractères invisibles. De temps en temps, les espaces contiennent des caractères invisibles qui n’apparaissent pas mais seront interprétés.

## Variables

Pour ajouter 1 a une variable, on peut faire :

````python
a = 2
a = a +1
````

* On peut aussi mettre des caractères spéciaux dans les noms de variable, mais pas en premier caractère *

mais on peut faire plus simplement :

````python
#Pour une addition
a += 1
#Pour une soustraction
a -= 1
#Pour une multiplication
a *= 1
#Pour une puissance
a **= 1
#Pour une division
a /= 1
#Pour un modulo 
a %= 1
#Pour le résultat entier
a //= 1
````

* Plus spécifiquement, *

 ```` python
a %= 4
````

* Calcule le reste de la de la division entière de a par 4 et affecte ce reste à a *

## Les opérateurs de comparaisons

Ces opérateurs renvoient de bool (donc True ou False)!
On a l'égalité (==); l'inégalité (!=); la supériorité (>); infériorité (<), et les comparaisons de type avec is et is not

```` python
a = 'Bonjour !'
print(type(a) is str)
print(type(a) is not int)
````

* Ici les deux renvoient True ! *

Dans le cas ou on a plusieurs condition a vérifier en même temps on utilisé 'and' (il est cumulatif : les deux conditions doivent être vrai pour que cela renvoie vrai), ou 'or' (renvoie vrai si l'une des deux condition est vraie, il est alternatif).

* Voir tables de vérité *

Un petit exemple :

````python  
first_var = 'Bonjour !'
second_var = 12

if type(first_var) is str and type(second_var) is int:
    print('La première variable est une string')

if type(first_var) is str or type(second_var) is str:
    print('L\'une des var est une string !')
````

Pour arondir des float et déterminer le nombre de décimales affichées, on peut faire :

```` python  
import math
a = math.py
print("{} a {} ans".format("michel", 45))
print("Nous récupérons la valeur de {:.2f} avec deux chiffres apres la virgule".format(a))

b = 0.2356
print('variable{:.2f}'.format(b))

````

On peut séparer les nombres pour avoir une meilleure visibilité :

````python
f = 124_54564_54
````

Caster une variable c'est changer son type, par exemple :

````python
a = 10
a = str(a)
is type(a) == str
````

* Ici la dernière ligne renvoie True car la variable 'a' a été castée grace a la fonction str() *

On peut utiliser les fonction max et min pour des strings comme pour des int :

````python
a = 1, 2, 15, 854, 24
b = 'lol', 'ok', 'pourquoi'
print(max(a))
print(min(b))
````

On peut récupérer l'index de départ d'une chaine de caractère donnée grace a la fonction find():

````python
chaine = "ceci est une chaine de caractères"
print(chaine.find("une"))
````

On peut aussi remplacer des caractères dans une chaine grace a la fonction replace() :

````python
chaine = "ceci est une chaine de caractères"
print(chaine.replace("une", "fliqhqe"))

#Par exemple, on peut faire
path = path.remplace("\\","/")
````

On peut aussi rajouter un élément a la fin d'une liste grace a la fonction append() :

````python
liste = ["a", "b", "c", "d"]
liste.append("coucou")
print(liste)
````

On peut comparer une liste pour voir si des éléments sont communs :

````python
liste1 = ["a", "b", "c", "d"]
liste2 = ["b", "c", "d"]
liste3 = set(liste1) - set(liste2)
print(list(liste3))
````

* Ici, seul les éléments différents apparaissent *

On peut utiliser des indices negatifs dans les listes :

````python
liste1 = ["a", "b", "c", "d"]
print(list[-1])

#Pour accéder au dernier élément
print(liste1[len(liste1)-1])
````

On peut préciser le pas (par exemple 2 par 2):

````python
liste1 = list(range(15))
print(liste[0:8:2])

#On peut le faire dans l'autre ordre en mettant un pas négatif
liste2=list(range(10)[1:10:1])
print(liste2)

#mais aussi
liste2=list(range(10)[1:10:1])
liste2.reverse()
print(liste2)
#attention le .reverse() travaille sur la liste en elle même
````

On peut aussi faire des listes imbriquées. Par exemple, ici, liste_full[1][1] pointe vers la liste d'indice [1] (donc la seconde), et la variable d'indice [1] de cette même liste (donc 5).

````python
liste1 = [1, 2, 3]
liste2 = [4, 5, 6]
liste_full = [liste1, liste2]
print(liste_full[1][1])
````

On peut aussi faire des opérations sur les listes :

````python
#ranger la liste dans l'ordre croissant
liste1 = [45848, 2, 325]
liste1.sort()
print(liste1)

#Ou cherche un élément dans une liste, l'opératuer 'in' renvoie un bool
liste1 = [45848, 2, 325]
if 2 in liste1:
    print('Le chiffre {} est dans la liste'.format(liste1[1]))
````

## ATTENTION le fonctionnement par référence

````python
liste1[1,154,2]
liste2 = liste1
liste2[1]=120
print(liste1)
print(liste2)
````

Ca fonctionne en fait comme un pointeur en C, on ne crée pas une nouvelle liste, mais on pointe vers le premier élément (en mémoire) de la liste1

Si on veut modifier le contenu il faut donc faire :

````python
liste1 = [1,154,2]
liste2 = liste1[:]
liste2[1]=120
print(liste1)
print(liste2)
#Ici on CREE une nouvelle liste a un emplacement différent, qui contient les valeurs copies de la liste1, qui est donc modifiable indépendament.
````

## Le dictionnaire

un dictionnaire n'a pas d'indices, et on accède a la valeur par sa clé.

````python
dico = {
    "premier" : 12,
    "toto" : 59
}
print(dico["premier"])
````

* Ici la clé est "premier"  est la value est 12*

On peut récupérer l'ensemble des clé en utilisant la fonction keys(), et l'ensemble des valeurs en utilisant la fonction values(), enfin toute la liste d'items en utilisant la fonction items() :

````python
dico = {
    "premier" : 12,
    "toto" : 59
}

print(dico["premier"])
print(dico.keys())
print(dico.values())
print(dico.items())
````

Pour savoir si une clé existe dans le dico (uniquement une clé), on peut (comme pour la liste) utiliser l'opérateur 'in':

````python
print("premier" in dico)
````

Pour les tuples, on ne peut pas add, il faut concatener :

````python
tuple1 = (1, 2, 3)
tuple2 = 4,
tuple1 += tuple2
print(tuple1)

#Pour ajouter des valeurs, on peut aussi utiliser __add__
tuple1 = tuple1.__add__((1,2))
````

Pour les sets c'est comme les tuples, sauf que chaque indice est unique.

## Correction des exos

````python
# si la variable est un str, print la variable, sinon ne rien afficher

message = "je suis bien une chaine de caractère"
if type(message) is str:
    print(message)

#Solution alternative
# if(isinstance(message, str)):
    #print

# remplacer une partie d'une chaine de caractère
message = "bonjour"
print(message.replace("jour", "nuit"))

# ordonner une chaine de caractère
message = "Pierre, Paul, Jacques"
message2 = message.split(', ')
message2.sort()
message2 =", ".join(message2)
print(message2)

# calculer le volume d'un sphère
import math
rayon = 10
vol = ((4*math.pi)*(rayon**3))/3
print('{:.2f}'.format(vol))

# tester si un nombre est plus grand que l'autre
nbr = 25
test = 10
print(nbr>test); print(nbr<10)

# créer une liste de nombre de 6 à 21 et avoir un pas de 3
print(list(range(21)[6:21:3]))

# connaitre le nombre d'occurences du caractère "e" dans une chaine
message = "Ceci est le message à analyser"
print(message.count("e"))

# récuperer le premier element d'une liste
# récupérer le dernier élément d'une liste
# récupérer les valeurs de la liste en commançant par l'indice 2 et finissant à l'indice 6 avec un pas de 2
# ajouter plusieurs elements à une liste
liste = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
print(liste[0])
print(liste[-1])
print(liste[2:6:2])
liste.append(17)
liste.extend(["k", "l", "m"])
print(liste)
````

## Correction exo supplémentaire

````python
# Créer un tuple, un set, un dico, une liste contenant les jours de la semaine.
# Supprimez le Lundi et le remplace par un Dimanche bis.
# Pointez vers le samedi.

mon_tuple = "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"
mon_set = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"}
ma_liste = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
mon_dico = {
    "J1" : "Lundi",
    "J2" : "Mardi",
    "J3" : "Mercredi",
    "J4" : "Jeudi",
    "J5" : "Vendredi",
    "J6" : "Samedi",
    "J7" : "Dimanche",
}

#Bloc tuple; les éléments sont impossible a supprimer sans convertir en liste; Un tuple, une fois crée est inchangeable et immuable
tuple_list = list(mon_tuple)

#Je vérifie le type de 'tuple_list'
print(type(tuple_list))

#Je change la valeur de lundi dans ma liste
tuple_list[0] = "Dimanche Bis"

#Switch back to a tuple
mon_tuple = tuple(tuple_list)
print(mon_tuple)

#Pointer vers samedi
print(mon_tuple[5])


#Même règle pour le set, mais on peut utiliser les fonctions remove() et add(), de plus on ne peut pas pointer vers un de ses éléments
mon_set.remove("Lundi")
mon_set.add("Dimanche Bis")
print(mon_set)

#Pour le dico, il faut supprimer le couple clé-valeur entièrement, et en ajouter un nouveau
del mon_dico["J1"]
#On ajoute la nouvelle entrée
mon_dico["J1"] = "Dimanche bis"
#On peut aussi faire :
new_entry = {"J1" : "Dimanche Bis"}
new_dico = {**new_entry, **mon_dico}
print(new_dico)
#Pour afficher samedi :
print(new_dico["J6"])

#Pour les liste, on pourrait utiliser la fonction remove(), mais mardi passerait a l'index 0, donc on peut faire
ma_liste[0] = "Dimanche Bis"
print(ma_liste)
#Afficher Samedi
print(ma_liste[5])
````