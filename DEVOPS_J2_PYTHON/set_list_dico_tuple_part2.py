# Créer un tuple, un set, un dico, une liste contenant les jours de la semaine.
# Supprimez le Lundi et le remplace par un Dimanche bis.
# Pointez vers le samedi.

mon_tuple = "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"
mon_set = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"}
ma_liste = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
mon_dico = {
    "J1" : "Lundi",
    "J2" : "Mardi",
    "J3" : "Mercredi",
    "J4" : "Jeudi",
    "J5" : "Vendredi",
    "J6" : "Samedi",
    "J7" : "Dimanche",
}

#Bloc tuple; les éléments sont impossible a supprimer sans convertir en liste; Un tuple, une fois crée est inchangeable et immuable
tuple_list = list(mon_tuple)

#Je vérifie le type de 'tuple_list'
print(type(tuple_list))

#Je change la valeur de lundi dans ma liste
tuple_list[0] = "Dimanche Bis"

#Switch back to a tuple
mon_tuple = tuple(tuple_list)
print(mon_tuple)

#Pointer vers samedi
print(mon_tuple[5])


#Même règle pour le set, mais on peut utiliser les fonctions remove() et add(), de plus on ne peut pas pointer vers un de ses éléments
mon_set.remove("Lundi")
mon_set.add("Dimanche Bis")
print(mon_set)

#Pour le dico, il faut supprimer le couple clé-valeur entièrement, et en ajouter un nouveau
del mon_dico["J1"]
#On ajoute la nouvelle entrée
mon_dico["J1"] = "Dimanche bis"
#On peut aussi faire :
new_entry = {"J1" : "Dimanche Bis"}
new_dico = {**new_entry, **mon_dico}
print(new_dico)
#Pour afficher samedi :
print(new_dico["J6"])

#Pour les liste, on pourrait utiliser la fonction remove(), mais mardi passerait a l'index 0, donc on peut faire
ma_liste[0] = "Dimanche Bis"
print(ma_liste)
#Afficher Samedi
print(ma_liste[5])