a = 'bonjour!'
print(type(a) is str)
print(type(a) is not int)

print(True or False)



# a = 1 
# b = 'fqlhzl'
# if type(a) is int and type(b) is str:
#     print('Ma bite')
# break

print("{} a {} ans".format("michel", 45))

a = 10
a = str(a)
print(type(a) == str)

a = 1, 2, 15, 854, 24
b = 'lol', 'ok', 'pourquoi'
print(max(a))
print(min(b))

chaine = "ceci est une chaine de caractères"
print(chaine.find("une"))

chaine = "ceci est une chaine de caractères"
print(chaine.replace("une", "fliqhqe"))

liste = ["a", "b", "c", "d"]
liste.append("coucou")
print(liste)
liste[2] = "balalhqleflhqif"
print(liste)

#BUG
liste1 = ["a", "b", "c", "d"]
liste2 = ["d", "e", "f"]
liste3 = set(liste1) - set(liste2)
print(liste3)

liste1 = ["a", "b", "c", "d"]
print(liste1[-1])

print(liste1[len(liste1)-1])

liste1 = list(range(15))
print(liste1[0:8:2])

liste2 = list(range(10,45))
liste2.reverse()
print(liste2)

liste1 = [1, 2, 3]
liste2 = [4, 5, 6]
liste_full = [liste1, liste2]
print(liste_full[1][1])

liste1 = [45848, 2, 325]
liste1.sort()

print(liste1)

liste1 = [1,154,2]
liste2 = liste1
liste2[1]=120
print(liste1)
print(liste2)

liste1 = [1,154,2]
liste2 = liste1[:]
liste2[1]=120
print(liste1)
print(liste2)

a = 0.2356
print('variable{:.2f}'.format(a))

import copy
tab = [1, 2, 3]
tab2 = tab[:]
tab3 = [[1, 2, 3], [1, 2, 3]]
tab4 = copy.deepcopy(tab3)
print(tab4)
print(tab4[0][0])

dico = {"premier" : 12, "toto" : 59}
#print(dico["premier"])
#print(dico.keys())
#print(dico.values())
#print(dico.items())

print("premier" in dico)

#BUG
for key in dico:
    print(key)

dico2 = [{"key1" : "jean", "age" : 24}, {"nom" : "paul", "taille" : 1.78}]
print(dico2[0]["key1"])

liste = [["bla", "bla"], ["blo", "blu"]]
dico = dict(liste)
print(dico)

tuple1 = (1, 2, 3)
tuple2 = 4,
tuple1 = tuple1.__add__((1,2))
tuple1 += tuple2
print(tuple1)

s = {1, 2, 3, 4, 5}
momo = s - set({2, 3})
print(momo)

tuple_lol = "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"
momo = {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"}
dico = {"lundi" : 1,  "mardi" : 2, "mercredi" : 3, "jeudi" : 4, "vendredi" : 5, "samedi" : 6, "dimanche" : 7}
lista = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

list_tuple.remove("lundi")
print(tuple_lol)
print(tuple_lol[5])

momo.remove("lundi")
print(momo)
#print(momo[5])

del dico["lundi"]
print(dico)
key_list = list(dico)
print(key_list[5])

lista.remove("lundi")
print(lista)
