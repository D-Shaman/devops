# si la variable est un str, print la variable, sinon ne rien afficher

message = "je suis bien une chaine de caractère"
if type(message) is str:
    print(message)
# if(isinstance(message, str)):
    #print

# remplacer une partie d'une chaine de caractère
message = "bonjour"
print(message.replace("jour", "nuit"))

# ordonner une chaine de caractère
message = "Pierre, Paul, Jacques"
message2 = message.split(', ')
message2.sort()
message2 =", ".join(message2)
print(message2)

# calculer le volume d'un sphère
import math
rayon = 10
vol = ((4*math.pi)*(rayon**3))/3
print('{:.2f}'.format(vol))

# tester si un nombre est plus grand que l'autre
nbr = 25
test = 10
print(nbr>test); print(nbr<10)

# créer une liste de nombre de 6 à 21 et avoir un pas de 3
liste1 = list(range(21))
print(liste1[6:22:3])

# connaitre le nombre d'occurences du caractère "e" dans une chaine
message = "Ceci est le message à analyser"
print(message.count("e"))

# récuperer le premier element d'une liste
# récupérer le dernier élément d'une liste
# récupérer les valeurs de la liste en commançant par l'indice 2 et finissant à l'indice 6 avec un pas de 2
# ajouter plusieurs elements à une liste
liste = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
print(liste[0])
print(liste[-1])
print(liste[2:6:2])
liste.append(17)
liste.extend(["k", "l", "m"])
print(liste)
print(len(liste))