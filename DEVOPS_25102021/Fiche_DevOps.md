# CLOUD : Azure

C'est la solution cloud de microsoft. On retrouve les mêmes éléments que chez AWS. Comme pour AWS on retrouve des bibliothèque qui permettent d'utiliser les services dans du code. Azure est aussi divisé en régions.

On y retrouve du IaaS et PaaS, mais pas de SaaS.

On retrouve les images (comme les AMI chez AWS). Cette image contient la configuration système. Les machines et services Azure sont appelés des ressources. Il existe aussi une gestion des rôles.

Les limites : le nombre de ressources crées, l'équilibreur de charge.

## Interface 

A gauche on retrouve tous les services Azure (les ressources). Comme Azure Cosmo DB qui est l'équivalent de DynamoDB, qui est l'équivalent de MongoDB.

L'équivalent de l'IAM est active directory. C'est là ou on gère les utilisateurs.

Azure fonctionne par management group qui gère des abonement, dans lesquels se placent notre architecture.

Niveau interface utilisateur, on utilise nativement powershell.

## Les services

Service de calcul :

Il regroupe les machines virtuelle (émulation logicielle d'un ordinateur physique), et les groupes de machines virtuelle. On retrouve aussi les conteneurs et kubernetes.

On peut aussi lancer des fonction, comme les lanbda chez AWS.

Pour les services de stockage, on a l'azure storage, qui est comparable a un S3, il prend plusieur type de stockage : les objets blob azure (pour stocker de grandes quantité de données) et les azure file pour partager des fichiers sur le réseau.

On a aussi un système de file d'attente Azure qui est un broker de message. Les Tables Azure pour des données structurées non relationnelles.

Différent type de stockage : hot, optimisé pour le stockage des données fréquenment utilisé, des stockages froids pour les données rarement solicitées, et les archives qui sont des serveurs glaciers.

Les services de base de données :

Azure Cosmo DB : non relationnel comparable a dynamoDB.
Azure SQL Database : moteur de base de données PaaS de microsoft SQL server.
Il y a aussi de Postgresql.

Les service de relevé de metrics :

Azure Synapse Analytics, Azure HDInsight, Azure Databricks, Azure DataLake.

Les machines virtuelle :

On peut utiliser n'importe quel type d'OS. Certaines sont optimisées pour le calcul, d'autre mémoire, GPU, ou calcul haute parformance.

Les réseaux virtuels Azure:

Ce sont des blocs de construction dans lesquels on stocke nos machines. Il y a des règles a respecter. Par exemple les plages d'IP ne doivent pas se chevaucher. Les sous-réseaux ne doivent par couvrir l'espace d'adressage entier du réseau. Il vaux mieux ne par trop fragmenter le réseau.

