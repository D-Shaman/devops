# *Ansible*

Rappel :

- Les logiciel ne communiquent jamais avec le matériel, mais uniquement avec l'OS, qui va parler avec le matériel.
- CVE : listing des failles de sécurité. On peut chercher sur des gitHub par le numero CVE, pour voir si des user ont deja mis des exploit en ligne.
- Mitre : site qui regroupe tous les types d'attaque.

Ansible apporte les variables d'environnement avec Docker, installe Docker, et derrière Terraform fait les infrastructure de manière globale.

Un hyperviseur est un logiciel de gestion de machine virtuel (comme VirtualBox). VirtualBox simule la partie matériel.
Pour les yperviseur de première génération, il y a un petit OS et des drivers pré-installés. Dans ce cas on a supprimé la couche OS et les hyperviseurs de première génération sont ultra-optimisés pour le traitement de machines virtuelle.

Ceux de seconde géénration comme virtualBox communiquent avec l'OS, et donc sont plus lent.

Ansible utilise le protocole SSH, ainsi qu'un système clé publique et clé privé.

## ASH, chiffrement

Le ASH est un algorythme (md5, sha...), qui transforme une chaine de caractère sans chiffrement (bonjour -> dzqihqlfq). C'est en fait un identifiant unique pour une chaine de caractères.

Donc quand on loggin, le mdp va être ASH (sha3 par exemple) et c'est le ASH qui est maintenu en DB, comme une chaine x renvoie toujours la même chaine Y après ASH, a chaque tentative de loggin, le ASH est comparé avec celui en serveur. Ici on ne peut pas revenir en arrière

Pour le chiffrement, il y a un algorythme de déchiffrement. Ici on peut revenir en arrière.

En HTTPS, le serveur a une clé privée qui n'est JAMAIS donnée au publique, et une clé publique autogénérée. Le serveurs envoie les pages en mode chiffré. Le client déchiffre la page avec la clé publique qu'il a reçu du serveur.
