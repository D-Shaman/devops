mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log/'
echo "________________" >> $FPATH/restart_mysql.log
echo "Le service mysql a été relancé :" >> $FPATH/restart_mysql.log
date >> $FPATH/restart_mysql.log
echo $USER >> $FPATH/restart_mysql.log
/etc/init.d/mysql restart >> $FPATH/restart_mysql.log