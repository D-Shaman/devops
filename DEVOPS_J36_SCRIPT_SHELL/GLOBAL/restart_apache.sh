mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log/'
echo "________________" >> $FPATH/restart_apache.log
echo "Le service mysql a été relancé :" >> $FPATH/restart_apache.log
date >> $FPATH/restart_apache.log
echo $USER >> $FPATH/restart_apache.log
/etc/init.d/apache2 restart >> $FPATH/restart_apache.log
