mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log/'
echo "________________" >> $FPATH/stop_apache.log
echo "Le service apache a été stoppé :" >> $FPATH/stop_apache.log
date >> $FPATH/stop_apache.log
echo $USER >> $FPATH/stop_apache.log
/etc/init.d/apache2 stop 1>> $FPATH/stop_apache.log 2>> $FPATH/errservice.log
