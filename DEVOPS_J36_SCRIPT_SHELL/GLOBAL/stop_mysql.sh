mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log/'
echo "________________" >> $FPATH/stop_mysql.log
echo "Le service mysql a été stoppé :" >> $FPATH/stop_mysql.log
date >> $FPATH/stop_mysql.log
echo $USER >> $FPATH/stop_mysql.log
/etc/init.d/mysql stop 1>> $FPATH/stop_mysql.log 2>> $FPATH/errservice.log