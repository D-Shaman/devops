#! /bin/zsh
#! /bin/bash

CPATH='~/Desktop/DEVOPS_J36/GLOBAL/'

echo "_-'Choisissez'-_"
echo "Démarrage MYSQL : 1"
echo "Arrêt MYSQL : 2"
echo "Démarrage Apache : 3"
echo "Arrêt Apache : 4"
echo "Database dump : 5"
echo "Display error log : 6"

read choice

case "$choice" in
    1) 
        sh -c "$CPATH/start_mysql.sh"
        ;;
    2)
        sh -c "$CPATH/stop_mysql.sh"
        ;;
    3)
        sh -c "$CPATH/start_apache.sh"
        ;;
    4)
        sh -c "$CPATH/stop_apache.sh"
        ;;
    5)
        echo "Database Dump"
        ;;
    6)
        cat errservice.log
        ;;
    *)
        echo "Option invalide"
        ;;
esac