mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log'
echo "________________" >> $FPATH/start_mysql.log
echo "Le service mysql a démaré :" >> $FPATH/start_mysql.log
date >> $FPATH/start_mysql.log
echo $USER >> $FPATH/start_mysql.log
/etc/init.d/mysql start 2>> $FPATH/errservice.log 1>> $FPATH/start_mysql.log