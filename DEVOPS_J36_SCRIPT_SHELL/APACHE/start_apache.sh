mkdir -p /home/$USER/Desktop/log 2> /dev/null
FPATH='/home/'$USER'/Desktop/log/'
echo "________________" >> $FPATH/start_apache.log
echo "Le service apache a démaré :" >> $FPATH/start_apache.log
date >> $FPATH/start_apache.log
echo $USER >> $FPATH/start_apache.log
/etc/init.d/apache2 start 2>> $FPATH/errservice.log 1>> $FPATH/start_apache.log