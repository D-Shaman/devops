# SHELL

D'autres commandes utiles:

```shell
#Afficher la première ligne d'un fichier
head -n 1 mon_fichier
#Afficher les 10 dernières lignes de mon fichier
tail mon_fichier
```

## Les gestionnaire de paquets

Par exemple : apt, wget, zypper. Ils permettent d'obtenir de nouveaux paquets.

```shell
# Pour mettre a jour les dépendances :
sudo apt-get update
sudo apt-get upgrade
# Ajouter un paquet 
sudo apt install mon_paquet
# retirer un paquet 
sudo apt autoremove mon_paquet
```

```shell
# Pour récupérer une page sur internet:
wget mon_url.com
```

## Les pipes

Avec le ">" on peut envoyer le résultat dans un fichier texte. Avec un seul ">" on remplace tout ce qui est a droite avec ce qui est a gauche. On peut AJOUTER les infos en mettant deux chevrons : ">>". C'est un append

Traceroute est un utilitaire que décrit le passage d'un paquet vers une destination. La commande équivalente sur windows est pathping.

```shell
touch ping.txt
ping 8.8.8.8 -c 10 > ping.txt && cat ping.txt

# ou 

sudo traceroute 8.8.8.8 >> ping.txt && cat ping.txt
```

Ecrire dans un fichier :

```shell
echo "blabla" > mon_fichier.txt
echo "blibli" >> mon_fichier.txt
```

dans le dossier /etc/ se trouve le hostname, ainsi que le hosts qui permet de configurer les DNS.

## network/interface

Pour obtenir le gateway pas defaut : ipconfig /all (ligne passerelle par défaut)

Sur OpenSUSE, le dossier se trouve dans /etc/sysconfig/network.

Le loopback fait référence au 127.0.0.1. Le auto lo est le auto-loopback.

Le primary network interface : le allow-hutplug permet de réveiller l'interface quand on branche un élément.  

