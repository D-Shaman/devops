# Changement et nouveau métier

Intervenant : Estelle SENESSE

## Objectifs pédagogiques

- Appréhender la notion de changement
- Comprendre les mécanismes du changement
- Découvrir la conduite du changement
- Appréhender son propre changement
- Maîtriser les outils pour mieux gérer ce changement de métier
- Construire ses projets professionnels.

## Exo 1 : Deux visions d'entreprise

Lecture du texte CREDITOC et CREDISAINE, questions :

- Quelles sont les 2 approches différentes des organismes de formations dans les entreprises Créditoc et Crédisaine ?
- Quels sont les effets de ces différentes approches sur la manière dont s'est déroulé le changement dans les entreprises ?
- Qui sont les différents protagonistes dans les 2 entreprises et quel rôle tiennent-ils dans le changement ?
- Selon vous, quels sont les leviers possible d'un changement réussi en entreprise ?

## Définitions

Le changement : c'est une démarche qui accompagne la vie de toute entreprise face a l'évolution de son environnement (facteurs internes et/ou externes). Le normbre de changements au sein de l'entreprise a été multiplié par 2,5 en 15 ans. Le changement est une rupture entre un **existant continu** et un **avenir promis synonyme de progrès**. Un changement est une remise en cause de **l'existant** à plusieurs niveaux: opérationnel (pratique, procédures), managérial (style de management et outils), culturel (culture d'entreprise) et stratégique (finalité des objectifs).

SWOT : Matrice synthétique des forces et faiblesse internes a l'entreprise (par rapport au changement). Mais aussi en externe : ce qui est autours de l'entreprise (opportunités et menaces).

Le changement est un processus : un enchaînement ordonné d'activités qui produisent un résultat escompté:

- Elements d'entrée / éléments de sortie.
- Ressources
- VA
- Indicateurs de résultats
- PDCA (roue de DEMING)

<center>

![DEMING](deming.png)

</center>

### Typologie des changements

| Changement prescrit | Changement construit|
|---------------------|---------------------|
| Facteurs externes | Facteurs internes |
| Origines légales ou technologiques | Portés par les acteurs|
S'imposent à l'entreprise : leur non respect met l'entreprise en péril| Volonté de faire partager les changements pour un grand nombre

| Changement de crise | Changement adaptatif
|---------------------|---------------------|
Solution a un dysfonctionnement | Transformation des pratiques rendue nécessaire pat l'évolution de l'organisation
Durée 1 jour a 3 jours | 6 mois a 18 mois
Accidents, grèves | Transformation digitale

### La transformation digitale des entreprises 

Le contexte :

- Environnement de crise sanitaire
- Crise économique, donc aides gouvernementales
- Fermeture des points de ventes puis jauge
- Changement de comportement d'achat des consommateurs
- Développemeny de circuits de distribution digitaux (click and collect, drive...)
- Incapacité ou capacité des entreprises à mettre en place leur transformation digitale (ressources, compétences, culture d'entreprise)
- ...

En temps de DevOps, nous sommes au centre de la transformation digitale des entreprises.

La conduite du changement est donc un ensemble de méthodes, de techniques et de moyens mis en oeuvre oar ube entreprise pour accompagner une transformation dans des colnditions optimales.

Les parties prenante de la conduite du changement :

L'équipe projet :

- Nombre limité de personnes
- Pluridisciplinarité : compétences et visions complémentaires
- Transversalité : fonctionnement hors hiérarchique
- Démarche, esprit et objectifs communs
- Pas de conflits d'intérêt*
- Solidairement responsables

Les facteurs clé de succès d'uné équipe projet :

- L'équipe est constituée en fonction des compétences qui permettront de répondre aux besoins exprimés
- L'équipe énonces ses propres règles de fonctionnement
- La progression de l'équipe est mesurée vis-à-vis d'objectifs à atteindre fixés par le chef de projet
- Le déroulement du projet est jalonné de réunions régulières
- L'équipe (ou le chef de projet) communique l'avancement du projet à toutes les parties prenantes
- Les co-équipiers sont dans une dynamique de créativité collaborative permanente : trouver des solutions ensemble et décider ensemble.

Motivation : La raison pour laquelle je souhaite rentrer dans l'entreprise. 

Moyens :
- Temps
- CV
- Pitch mail
- Pitch oral
- Profil Linkedin
- Fichier qualifié de recruteur
