from dice import Dice

class Gobelet():
    def __init__(self):
        self._total_value = 0
        self._total_dice = int(input("Combien de dés souhaitez vous lancer ?\n"))
        self._dice_array = []

    def get_data(self):
        return self._total_dice

    def load_roll(self):
        for dice in range(self._total_dice):
            #print("ddqzdqdqz", dice)
            new_dice = Dice()
            new_dice.roll()
            self._dice_array.append(new_dice.get_valeur())
        return self._dice_array

    def dice_sum(self):
        self._total_value = sum(self._dice_array)
        return self._total_value

# gobelet1 = Gobelet()
# print(gobelet1.load_roll())
# print(gobelet1.dice_sum())