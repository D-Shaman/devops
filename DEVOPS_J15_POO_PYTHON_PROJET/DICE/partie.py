from player import Joueur

class Partie():
    def __init__(self, nb_tours, nb_dice):
        self.set_nb_tours(nb_tours)
        self.set_nb_dice(nb_dice)
        self.set_nb_player()
        self.get_nb_tours()
        self.initialiser()

    def set_nb_player(self):
        self._nb_player = int(input("Combien de joueurs ?\n"))
        return self._nb_player

    def set_nb_tours(self, nb_tours):
        self._nb_tours = nb_tours
    def get_nb_tours(self):
        return self._nb_tours

    def set_nb_dice (self, nb_dice):
        self._nb_dice = nb_dice
    def get_nb_dice(self):
        return self._nb_dice

    def initialiser(self):
        array_player = []
        for i in range((self._nb_player)):
            new_name = input("Quel est votre nom ? \n")
            array_player.append(new_name)
            self._array_player = array_player
        return self._array_player

    def lancer(self):
        k=0
        while k <= self.get_nb_tours():
            for i in range(len(self._array_player)):
                player = Joueur(self._array_player[i])
                player.afficher_score()

partie1 = Partie(10, 2)
print(partie1.lancer())