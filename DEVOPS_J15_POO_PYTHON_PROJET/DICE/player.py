from gobelet import Gobelet

class Joueur():
    def __init__(self, name, score =0):
        self.set_name(name)
        self.set_score(score)
        self.jouer(Gobelet())
        self.afficher_score()
    
    def set_name(self, name):
        self._name = name
    def get_name(self):
        return self._name
    
    def set_score(self, score):
        self._score = score
    def get_score(self):
        return self._score

    def jouer(self, gobelet):
        gobelet.load_roll()
        self.set_score(gobelet.dice_sum())
        print(gobelet._dice_array)
        #print(self.get_score())
    
    def afficher_score(self):
        print(f"Le joueur {self.get_name()} a obtenu le score {self.get_score()}")

# name = input("Quel est votre input ?\n")
# joueur1 = Joueur(name)



