import random

class Dice:
    def __init__(self):
        self._valeur = 0

    def get_valeur(self):
        return self._valeur
    
    def roll(self):
        self._valeur = random.randint(1,6)

# dice1 = Dice()
# print(dice1.get_valeur())
