# La stack ELK

**Node permet de faire du JS en back**
**ElasticSearch est codé en Java, son type est du multiindex (fortement indexé), il est possible de le configurer en json ou yaml**

**Points fort : vitesse, scalabilité, résilience (duplication), modulaire**

Un cluster englobe in ensemble de noeuds qui contiennent les index. Parmis ces noeuds il y a un master, si le master tombe, il va être remplacé par un autre. Ce sont les nodes en eux même qui décident qui va être le master.

Le système de shard et le système de replica :

- La réplica permet de faire qu'une data qui est envoyée a un élément du cluster est envoyé a d'autre noeuds.
- Le système de shard permet l'indexation. Il doit y avoir un équilibre entre les deux. Ces éléments sont a définir dans le mapping.

Cette techno peut faire partie d'une stack ELK :

- ElasticSearch : moteur de recherche extrèmement puissant, avec sa propre base de donnée. Cette BDD tourne en noSQL (car elle est plus rapide).
- Logstash : c'est un outil qui va chercher des logs, les met au format json, et les envoie dans l'elasticsearch. Il est très configurable. C'est un pipeline.
- Kibana : c'est un outil d'affichage.

## ElasticSearch

Mode de fonctionnement : ElasticSearch est une instance qui contient des noeuds. Quand on entre une donnée elle vient se placer dans l'instance. La requète fonctionne sur le système VERBE (GET, POST) + URL (La ou je vais l'envoyer) / -search. Pour une query on met "q =" (q pour une query).

user : elastic 
mdp : changeme

Exemple :

    GET cave/_search?q=forge

Créer un index :

    PUT user

Regarder user :

    GET user/_search

Mettre des données :

    POST user/_doc
    {
        "nom":"Dupont"
        "prenom":"Jean"
    }

Le mapping correspond a peut près aux collones sur mysql. On peut voir le maping d'elasticsearch en faisant un GET sur un index

    GET user

On ne peut pas changer le type dans le maping. Par contre on peut ajouter des éléments dynamiquement. Par exemple un user1 peut avoir un âge et une ville renseignés, mais un user2 ne peut pas l'avoir.

ON NE PEUT PAS MODIFIER DYNAMIQUEMENT, il faut alors :

    DELETE user

Faire un mapping (il est conseillé de créer le mapping préalablement) :

    PUT user 
    {
    "mappings": {
        "properties": {
        "nom": {
            "type": "keyword"
            },
            "prenom":{
            "type":"keyword"
            },
        "age":{
            "type" : "integer"
        }
        }
    }
    }

On va remplir le doc :

    POST user/_doc
    {
    "nom":"Chirac",
    "prenom":"Jacques"
    }

On peut ensuite rechercher :

    GET user/_search?q=Jean-Jacques

On peut modifier le mapping :

    POST user/_mapping
    {
    "properties":{
        "message": {
        "type":"text"
        }
    }
    }

On peut update un élément :

    POST user/_update/nR5uAnwBMctsCE3cZpAR
    {
    "doc":{
        "prenom":"Yves"
    }
    }

Création si le document n'existe pas :

    POST user/_update/nR5uAnwBMctsCE3cZpZR
    {
    "doc":{
        "prenom":"Pierre"
    },
    "upsert": {
        "prenom":"Pierre",
        "nom":"Chirak",
        "age": 139,
        "message" : "Mangey lé pom"
    }
    }

Créer une query pour aller chercher spécifiquement un nom en précisant une colonne :

    GET user/_doc/_search
    {
    "query":{
        "query_string":{
        "query":"pom",
        "default_field":"prenom"
        }
    }
    }

On peut aussi passer par les filtre pour gagner en rapidité et ne pas avoir le système de score - la pertinance n'est pas prise en compte :

    GET user/_doc/_search
    {
    "query":{
        "bool":{
        "filter":[
            {
            "term":{
                "prenom":"pom"
            }
            }
            ]
        }
    }
    }

Pour les DELETE :

    DELETE _all

Va supprimer l'intégralité des index

    DELETE <nom de l'index>

Va supprimer l'index spécifié

    POST <nom de l'index>/_close

Cloture l'index (on ne peut plus y accéder sauf après avoir fait).

    POST <nom de l'index>/_open

Pour supprimer un utilisateur par exemple

    DELETE <nom de l'index>/_doc/<id de l'élement>

### SELECT

GET permet de faire des requètes via l'URL. Le POST permet de renseigner des données dans le body.

Requète via URL :

    GET cave/_search



Requète via le body :

    POST cave/_search
    {
    "query":{
        "match_all":{}
    }
    }

*Afficher plus de resultats*

    POST cave/_search
    {
    "query":{
        "match_all":{}
    },
    "size":50
    }

*Faire de la pagination, a partir du 25 ème élément*

    POST cave/_search
    {
    "query":{
        "match_all":{}
    },
    "size":50,
    "from":25
    }

*Trier les éléments*

    POST cave/_search
    {
    "query":{
        "match_all":{}
    },
    "size":15,
    "sort":[
        {
        "nom.keyword":{
            "order":"desc"
        }
        }
    ]
    }

*Autoriser le tri sur le type test*

    POST cave/_mapping/
    {
    "properties" : {
        "nom":{
        "type":"text",
        "fielddata": true
        }
    }
    }

*Filtre et query*

    POST cave/_search
    {
    "query":{
        "bool":{
        "must": [
            {
            "match": {
                "adresse": "paris"
            }
            }
        ]
        , "filter": [
            {
            "term": {
                "zone": 75004
            }
            }
        ]
        }
    }
    }

*Les préfixes*

    GET cave/_search
    {
    "query": {
        "fuzzy": {
        "adresse": {
            "value": "par",
            "fuzziness": 2
        }
        }
    }
    }

### Les types 

Pour les entiers : bytes, short, integer, long
Pour les normbre a virgule : float, double
Pour les dates : 