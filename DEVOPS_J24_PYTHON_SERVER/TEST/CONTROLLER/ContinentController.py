from flask import render_template
from flask import redirect

class ContinentController():
    def fetch_continent(self, model):
        return render_template('bonjour.html', obj=model.fetch_all())

    def vue_add_continent(self):
        return render_template('formulaire.html')

    def suppr_continent(self, model, id):
        model.suppr_continent(id)
        #return redirect("http://localhost:5001")
        return render_template('bonjour.html', obj=model.fetch_all())
    
    def add(self, model, continent):
        return model.addContinent(continent)

    def vue_update_continent(self, data):
        return render_template('formulaire_update.html', data=data)
    
    def update(self, model, data):
        return model.update_continent(data)