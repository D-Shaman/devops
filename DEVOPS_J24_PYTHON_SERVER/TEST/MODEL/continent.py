from MODEL.db import Connect
from flask import redirect

class ContinentModel:
    def __init__(self):
        self.conn = Connect.log()
    
    def fetch_all(self):
        self.conn.execute(""" SELECT id_continent, nom_continent FROM continent """)
        rows = self.conn.fetchall()
        return rows

    def suppr_continent(self, id):
        self.conn.execute(f""" DELETE FROM continent WHERE id_continent = {id}""")
    
    def addContinent(self, continent):
        self.conn.execute(
            f"""INSERT INTO continent(id_continent, nom_continent)
            VALUES('{int(continent.get('id_continent'))}', '
            {continent.get('nom_continent')}')"""
            )
    
    def update_continent(self, data):
        self.conn.execute(
            f""" UPDATE continent SET nom_continent = '{data.get('nom')}' 
            WHERE id_continent = '{int(data.get('id'))}' """
        )