from CONTROLLER.ContinentController import ContinentController
from MODEL.continent import ContinentModel
from flask import Flask, render_template, request, redirect

app = Flask(__name__)
contModel = ContinentModel()
contController = ContinentController()

# Middleware applicatif, intercepte les route et renvoie les controller
@app.route('/')
def hello_world():
    return contController.fetch_continent(contModel)

@app.route('/delete/<int:id>')
def suppr(id):
    return contController.suppr_continent(contModel, id)

@app.route('/formulaire/')
def formulaire():
    return contController.vue_add_continent()

@app.route('/addContinent/', methods=['POST', 'GET'])
def add_continent():
    formData = request.form
    contController.add(contModel, formData)
    return redirect("http://localhost:5001")

@app.route('/update_formulaire/')
def formulaire_update():
    formData = request.args
    return contController.vue_update_continent(formData)

@app.route('/update_continent/', methods = ['POST', 'GET'])
def update_continent():
    formData = request.form
    contController.update(contModel, formData)
    return redirect("http://localhost:5001")
