# Retours sur le MVC

Flask est un microframework qui permet le développement d'app web.

Pour se connecter a MySQL on utilise mysql-connector-python. Grace a cela on est directement connecté a la db (en TCP). Grace a cet outil on peut directement utiliser des commandes SQL.

Le serveur est divisé en 3 : le modèle, la vue, et le controller (MVC). Le modèle est le repo, il va s'occuper de faire nos requètes. La vue (ou template) contient les éléments en HTML et en CSS. Le controller fait la liaison entre le modèle et la vue en MVC 2 on fait un controller par fonctionnalité.

L'utilisateur parle a l'app.py, l'app.py déterminer les routes et appelle le controller qui correspond. Le controller va alors appeller le modèle qui contient les requètes SQL, le model renvoie de la donnée. Le contrôler va alors envoyer les données a la vue (template) pour les mettre en forme. Pour le controller les renvoie a l'utilisateur.

Dans app.py , on commencer par instancier app, qui correspond a flask, avec la commande :

```py
app = Flask(__name__)
```

J'ai instancié Flask donc je peut me servir de ses méthode. L'une est le décorateur @app.route. Ce décorateur vient récupérer l'URL.

```py
@app.route
```

Ensuite, le décorateur sert a gérer les évènements. Donc en desous on a la code qui correspond a l'évènement. Donc appeller le controller etc...