from MODEL.db import Connect
from flask import redirect

class ContinentModel:
    def __init__(self):
        self.conn = Connect.log()
    
    def fetch_all(self):
        self.conn.execute(""" SELECT id_continent, nom_continent FROM continent """)
        rows = self.conn.fetchall()
        return rows

    def suppr_continent(self, id):
        self.conn.execute(f""" DELETE FROM continent WHERE id_continent = {id}""")
        return redirect("http://localhost:5001")