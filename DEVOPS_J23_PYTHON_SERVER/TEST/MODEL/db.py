from abc import ABC
import mysql.connector

class Connect (ABC):
    conn = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="beer",
        port="3306",
        auth_plugin="mysql_native_password"
    )

    @staticmethod
    def log():
        try:
            cursor = Connect.conn.cursor()
            return cursor
        except mysql.connector.Error as err:
            print(err)

