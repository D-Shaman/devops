from flask import render_template

class ContinentController():
    def fetch_continent(self, model):
        return render_template('bonjour.html', obj=model.fetch_all())

    def vue_add_continent(self):
        return render_template('formulaire.html')

    def suppr_continent(self, model, id):
        model.suppr_continent(id)
        return render_template('bonjour.html', obj=model.fetch_all())