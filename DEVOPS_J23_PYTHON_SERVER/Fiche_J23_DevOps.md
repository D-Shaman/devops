# Python server

## L'architecture

Dans l'architecture classique : La base de donnée (en MySQL) est en relation avec le back (donc avec python). Dans une architecture classique, il y a aussi une techno front (donc desktop) qui permet de discuter avec le back. Il existe aussi une partie mobile (react native). Ici on a deux projets différents, un front et un back qui communiquent en HTTP/Json.

En architecture monolithique : La base de donnée communique toujours avec le back python, mais python possède un système de templating (ici JINJA). La base de donnée communique via le protocole TCP avec des template qui font un rendu HTML/CSS. Le templace fait donc la partie front.  Le front et le back sont ensemble donc il n'y a pas de communication HTTP/Json. Au niveau des dossier on va avoir un modèle. Il vient communiquer avec la DB (le modèle fait les requètes) - M. Les template sont des vues, on met donc les fichiers HTML / CSS (c'est le template) - V. Les controller font la liaison entre les vues et le modèle - C. C'est donc un modèles MVC. Il y a aussi un fichier app.py qui fait office de main.

```
venv\Scripts\activate
```

