# Kafka 2

Plusieurs instances d'un même logiciel interconnecté est appellé un cluster.

Chaque instance de Kafka contient des topics, qui peuvent être divisés en partition. Chaque partition d'un même topic peut être traitée par un Kafka différent. Le producer envoie les information vers le kafka. Et le consumer va requèter kafka pour récupérer les informations.

    A noter qu'on envoie pas de "fichiers" dans Kafka, mais uniquement de la donnée.

Chaque message au niveau du broker a un identifiant appellé un offset