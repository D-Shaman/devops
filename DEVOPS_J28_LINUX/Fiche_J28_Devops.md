# LINUX

## Les distributions

On OS est un système d'exploitationj. Il fait le dialogue entre le programme et la machine. L'OS gerre la mémoire, l'utilisation du processeur entre les différentes applications, les accès aux réseaux, ainsi que les périphériques comme le clavier et la souris. Il existe une multitude d'OS. Le prenier est Colossus (1943-1945), puis UNIX apparu en 1973 et se poursuit jusqu'a présent (avec par exemple Android et Ubuntu, voire même les distrib basées sur Debian et RedHat).

Exemples : Kali Linux (orientée pentest), Ubuntu (très user friendly), Manjaro (basée sur ArchLinux), Debian(alternative a LINUX), CentOS (orienté server), AlpineLinux (très léger, qui sert pour faire des petits containers pour Docker)...

A la base, Windows est un OS basé sur DOS, opposé aux systèmes UNIX (MacOs -Darwin-, et Linux -UNIX-). Une distribution est une version d'un système d'exploitation. Le Noyau, ou Kernel est basés sur un système de syscall (appels système), les pilotes graphiques, les pilotes matériels, les gestionnaires de processus, et un gestionnaire de mémoire. Le Kernel comminique avec la partie applicative comme le shell, l'environnement de bureau et les librairies graphiques.

- 1969 : Ken Thompson et Denis Ritchie de la société ATT créent UNIX, un système d'exploitation et le langage C. Unix est le premier système basé sur une API écrite dans un langage évolué et non en assembleur. UNIX est d'abord Open Source puis deviendra commercial.
- 1984 : Richard Stallman développe un système d'exploitation libre compatible UNIX appelé GNU. Il crée la licence GPL (GNU Politic Licence) ainsi que la Free Software Foundation pour la promotion du logiciel libre et la défense des utilisateurs.
- 1985 Sortie du compilateur GCC (GNU C Compiler).
- 1991 : Linux Torvalds développe un noyau de système d'exploitation en Open Source sous licence GNU GPL appellé : GNU/Linux ou Linux.

GPL (GNU Politic Licence) spécifie qu'un logiciel y adhérent sera pour toujours en Open Source.

Open Source (logiciel libre) signifie que les utilisateurs sont libre :

- D'exécuter le programme, pour tous les usages.
- D'étudier le fonctionnement du programme, de l'adapter à ses besoins (accès au code source).
- De redistribuer des copies.
- D'améliorer le programme et de publier des améliorations

Ce sont 4 règles fondatrice du logiciel libre. Open Source obéit à une définition très précise établie par l'Open Source Initiative (organisation pour la promotion du logiciel Open Source).

Dans une distribution Linux on peut choisir son environnement graphique. Dans un environnement professionnel, les distrib sont en général headless (donc sans interface graphique).

## Les gestionnaires de paquets ()

Exemple: pip pour python
Permet d'installer des logiciels (qu'on appelle des paquets). Toutes les distributions Linux en ont un.
On a par exemple :

- APT : Advanced Packaging Tool que l'oin retrouve sur Debian et Ubuntu et de nombreux descendant, ne s'utilise qu'en ligne de commande.
- Synaptic : interface graphique pour APT.
- Aptitude autre interface graphique pour APT.
- RPM : Red Hat Package Manager, un gestionnaire de paquet pour Red Hat.
- Yum : présent sur RedHat, CentOs et Fedora.

## Le SHELL

C'est un terminal permettant de taper des commandes pour exécuter des tâches, tel que :

- Se déplacer dans le système de fichiers (répertoire)
- Modifier des fichiers
- Télécharger des fichiers
- Configurer le système
- Etc...

Le Shell communique donc avec l'OS. Sur Ubuntu le Shell est un Bash. La quasi-totalité des commandes se trouvent dans le usr/bin.
Dans Ubuntu : ouvrir un terminal : CTRL + ALT + T

```shell
# Pour avoir une liste : 
ls
```

```shell
# Pour avoir une liste avec les droits : lecture, écriture exécution pour l'user, puis pour le groupe, puis pour les autres
ls -l
```

```shell
#Pour savoir ou on se situe :
pwd
```

```shell
#Créer un fichier :
touch mon_fichier
```

```shell
#Pour écrire dans un fichier text :
echo "truc" > mon_fichier
```

```shell
#Déplacer un élément (et renommer en le déplaçant au même endroit)
mv chemin/mon_élément chemin_destination
mv chemin/mon_element1 chemin/monelement2 chemin_destination
#Pour copier 
cp mon_element chemin_destination
```

```shell
#Lire l'intérieur d'un fichier (par concaténation)
cat mon_élément
```

```shell
#Netoyer le shell:
clear
#ou CTRL+L
```

```shell
#Créer un dossier
mkdir mon_dossier
```

```shell
#Supprimer un élément
rm mon_element
#Supprimer un dossier
rmdir mon_dossier
#ou (-r pour récursif)
rm -r mon_dossier
```

```shell
#Afficher la consommation des ressources en cours
top
```

```shell
#Historique des commandes :
history
```

```shell
#refaire la dernière commande :
!!
```

```shell
#Eteindre la machine
shutdown now
```

### Les Couleurs

Couleur des élément :

- Bleu nuit = dossier
- Bleu turquoise : lien symbolique (donc un racourcis)
- Blanc : fichier
- Vert surligné : fichier système de var temporaires
- Le SwapFile : C'est un fichier qui va stocker les info en ram (les variables) pour libérer de l'espace.
- Le TMP est vidé au démarage de la machine. Il ne faut rien stocker dedans.

### APT

Permet d'installer des pacquets. Il faut utiliser la commande "sudo" (switch user and do). Il passe sur un status root le temps de la commande.

```shell
# Chercher une application avec un mot clé
sudo apt search nom_du_pacquet
```

```shell
#Installer une application (pacquet)
sudo apt install nom_du_pacquet
```

```shell
#retirer une application
sudo apt remove nom_du_pacquet
```

```shell
#Mettre a jour le système, le -y met yes a la prochaine question posée
sudo apt update && sudo apt upgrade -y && reboot
```

### Commandes réseau

Certaines commances sont utiles pour le réseau

```shell
#Ping envoie des pacquets a l'adresse ciblée. Ou ajoute -c5 pour ping 5 fois.
ping 8.8.8.8
```

### Les scripts

TOUTES LES COMMANDES VUES JUSQU'A MAINTENANT PEUVENT ETRE MISES DANS UN .sh ET EXECUTEES.

```shell
#Pour modifier les droits et exécuter un scipt (change mod). Ici via le +x on donne le droit d'exécution a l'urilisateur en cours 'u'
sudo chmod u+x mon_fichier.sh
#Pour retirer le droit d'execution :
sudo chmod u-x mon_fichier.sh
```

```shell
#Executer un scipt
bash fichier.sh
./ fichier
```

```shell
#Exemple de petit script
echo "Mise a jours des paquets en tant que $USER"

sudo apt update && sudo apt upgrade -y

echo "Fin de la mise a jour"
```

On se rappelle qu'on peut voir le contenu du script avec cat mon_fichier

## Debian

config de la vm :

- Acces par pont
- Depuis la VM je peut ping ma machine physique (vers l'adresse ipv4 (ipconfig))
- Depuis ma machine je peut ping ma VM (ip a)
- Puis depuis ma machine physique je peut me connecter en ssh a ma VM via la commande ssh user@ipmachine

Exemple:

```shell
ssh toto@192.168.0.25
```
