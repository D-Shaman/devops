# Git, GitHub, WorkFlowGit

## GitHub

Intervenant : Benoît

C'est une invention de Linus Thorvald, l'inventeur de Linux. C'est un vieux mot pour "get". C'est un outil indispensable (orienté DEV) pour le DevOps. GitHub appartient maintenant a Microsoft.

Il permet d'avoir des lignes temporelles sur lesquelle on peut poser des checkpoints sur le travail (et aussi de revenir en arrière s'il y a un problème). Il permet de ne plus perdre de versions du travail. On peut par exemple voir toutes les modifications avec git -log.

Le système de versionning est local, mais peut aussi être décentralisé, c'est a dire mettre les repos en ligne (GitHub, GitLab, GitBucket...).

GIT est un VCS : Version control system. Il enregistres les modifications apportées aux fichiers. C'est d'un côté un client (qui permet d'intéragir avec le répo, qui est stocké en local). Tous les fichiers et dossiers qui commencent par "." sont des fichiers cachés.

Les checkpoints sur les lignes temporels sont des commits, qui retiennent toutes les versions de l'application. Pour peut aller regarder chaque commit, et comparer l'état avant et après le commit. Les lignes temporelles parallèles sont appelées "branches" leur but est en général d'ajouter une feature. C'est en général fait sans modifier la branche principal.

Dans la partie "actions" de GitHub, il y a une partie CI/CD (Continuous integration/ Continuous Developpement). Il permet de build / test / deploy de manière dynamique.

Git permet aussi la gestion des conflits. C'est ce qui arrive quand on modifie un fichier en même temps qu'un collègue (il faut donc déterminer quel changement doit rester).
Git facilite la communication via un message a chaque push permettant d'expliquer quel est le push.

On configure git en utilisant la commande :

````shell
git config --list
````

et on peut modifier les éléments a l'intérieur.

Dans le shell :

````shell
cd -> Pour se déplacer
mkdir -> Pour créer un dossier
ls -> Avoir la liste des éléments
    ls -a pour avoir les fichiers / dossiers cachés
pwd -> Regarder ou on est
code . -> Ouvre vscode a l'emplacement "." donc ici

