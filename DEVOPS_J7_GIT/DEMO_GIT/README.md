# Markdown

````py
    print("Poulet")
````

## Les commandes

Avec les magic quotes (alt-gr + è)

Lancer un programme en Python : `py script.py`

```md
Lancer un programme en Python : `py script.py`
```

## Les notes

````md
> Ceci est une note, elle attire l'attention sur quelquechose a prendre en compte comme une option
````

## Les liens

Permet la navigation interne comme externe.

```md
Voir [le site de Python.](www.python.org)
```

Voir [le site de Python.](www.python.org)

Avec entre [ ] ce qui va être affiché et entre ( ) le lien vers le site.

On peut se déplacer en interne avec :

````md
Lien ver [le support Git](./git-base.pdf)
````

Lien ver [le support Git](./git-base.pdf)

## Les images

![blabla](https://picsum.photos/seed/picsum/200/300)

![ddd](art.png)

````md
![]([art.png](./art)
````

Comme pour les encres (lien) mais avec un point d'exclamation devant.

## Tableaux

| id | nom | prenom |
| ---- | ---- | ---- |
| 1 | Dupont | Jean |
| 2 | Connard | Man |

## Les éléments de texte

### Le gras

Pour mettre en **gras**

```md
Pour mettre en **gras**
```

### L'italique

Pour mettre en *italique*

````md
Pour mettre en *italique*
````

😊 (Win + .)

## Les listes

### Les listes non ordonées

Pratique pour l'énumération

- Lolilol
- mdr
- squalala
- nous sommes parti
  - Lol

````md
- Lolilol
- mdr
- squalala
- nous sommes parti
  - Lol
````

### Les listes ordonées

Pratique pour ordonner des éléments.

1) France
   1) Lille
2) Mars
3) Japon
4) Belgique

````md
1) France
   1) Lille
2) Mars
3) Japon
4) Belgique
````

### Retour sur Git

Pour initialiser le repo Git vide, il faut utiliser :

````shell
git init
````

Les fichiers marqués "U" dans visual code sont "untracked" donc ne sont pas encore pris en compte par Git.

La première étape pour push est de :

````shell
git add . 
# ou *

#Pour retirer un fichier du pré-push
git rm --cached .\README.md
````

Puis, pour commit:

````shell
git commit -m "First Commit"
# -m pour associer un message

#Git commit -am fait le add des fichiers deja existant
````

On utilise pour obtenir des info sur l'état du répo et des fichiers pas encore push :

````shell
git status
````

Pour afficher les logs :

````shell
git log
````

Il faut après lien le repository a une plateforme en ligne. Dans ce cas, GitLab. Et créer :

````shell
git remote add origin  https://gitlab.com/XXXXX
#Ici le origin n'est qu'une variable
````

Pour récupérer l'URL on peut utiliser :

````shell
git remote get-url
````

Puis pour push le projet :

````shell
git push origin main -u
# Le -u est le set upstream. Cela enregistre les push comme des push origin main
````

changement depuis la plateforme GitLab

Pour récupérer les changements que j'ai fait dans mon repo a distance :

````shell
git pull <nom var> <branche>
````

### Branches paralelles

Les branches permettent de travailler sur une partie du projet sans impacter le travail sur une autre branche. Les branches sont comme des lignes parallèles alternatives.
Pour voir toutes les branches du projet :

````shell
git branch
# affiche la liste des branches. Celle sur laquelle on est 
````

Pour créer une branche, on fait :

````shell
git branch <nouvellesbranche>

#Pour créer une branche et se place directement dessus

git checkout -b <nouvellebranche>

#Pour créer une branche locale :
git checkout -b feature/<branche>
````

puis pour s'y déplacer :

`````shell
git branch
````

Renommer une branche :

```shell
git branch -m nouveau nom
```

delete une branche :
````
git branch -d <branche a suppremier>
````

Merge des branche :

````shell
git merge <nom de la branche a merge>
````

On peut utiliser le stash () pour empiler des feature et les sortir a un autre endroit. Elles se comportent alors comme un merge.