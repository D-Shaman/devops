# Kafka

Si on prend l'exemple d'un envoie de formulaire depuis un client vers un serveur. Si le serveur tombe, il va renvoyer une erreur de type 500 (car ce n'est pas le faute de l'utilisateur mais du système). Ce problème peut aussi arriver en cas de surcharge du serveur.

    Rappel :
    - 100 : Information
    - 200 : Succès
    - 300 : Redirection
    - 400 : Erreur du client HTTP
    - 500 : Erreur du serveur

Pour éviter le plantage du serveur par un nombre trop important de requète, on met un "broker de messages" entre le cient et le serveur. On peut voir Kafka comme un genre de base de données très rapide et très robuste. On transmet le formulaire a Kafka qui va le garder en mémoire. C'est le serveur qui va se connecter au broker pour récupérer les formulaires. C'est donc un système **asynchrone** car il n'est pas traité au moment de la réception.

C'est bien le serveur qui va ensuite requèter le broker pour récupérer les formulaires en attente. 

Quand le client envoie un message au serveur, ce client est en mode push. Mais si ce dernier envoie trop de message, il risque de faire tomber le serveur. Pour diminuer la quantité de message sur un certain laps de temps, on peut choisir de scaler horizontalement les serveur et les mettre en lien avec un load balancer. On peut aussi choisir de faire de la scallabilité verticale (en augmentant le processeur et la ram du serveur), mais c'est quasiment toujours une mauvaise solution. La bonne solution peut être de mettre en place du broker de message.

La seconde méthode est celle du pull, dans ce cas c'est le serveur que va requèter un client pour tirer un message. La on peut configurer la vitesse a laquelle on récuère les message (en terme de Mo/s ou de messages par secondes).

Les broker fonctionnent différemment. Par exemple, quand un message est récupéré par un broker, il peut être détruit de ce dernier (Rabitnq), ou être conservé pour environ 7 jours (Kafka). De même le broker peut prendre la stack de message dans un sens ou dans un autre, ou même ne pas prendre la stack et prendre les prochians messages.

L'application qui va récupérer les informations de Kafka sont les consumers (subscribers, sur les autres techno). Les serveur ou appli qui vont fournir les informations a Kafka sont les producers (publisher sur les autres techno)

Kafka peut marcher avec Elastic et une autre base de donnée pour d'un coté -via elastic- aller chercher l'ID d'un élément dans une DB (car elastic est optimisé pour la recherche) et ensuite utiliser l'ID pour faire une requète via Mongo ou SQL.

Architecture CQRS : on utilise des microservices pour gerrer les inclusion et les lectures en base de données. 

Chaque information dans un broker est appelée un topic, c'est un genre de répertoire. Il peut donc y avoir plusieur topic dans un seul Kafka (Ils sont un peu comme des buckets).

Quand on fait un cluster de Kafka, on en démarre un Kafka par machine, et pas tous sur la même (Kafka est seul sur sa machine).

Il est a noter que l'on peut connecter deux consumers sur le même topic, mais dans ce cas la, les deux consumer vont recevoir les messages. Ils ne vont pas être triés.